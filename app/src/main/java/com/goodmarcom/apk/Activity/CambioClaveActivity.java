package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;


//import android.support.design.widget.FloatingActionButton;

//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;



import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;
//import android.widget.Toolbar;


import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.PerfilResponse;
//import com.puertomar.apk.Enty.Clientes;
import com.goodmarcom.apk.R;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CambioClaveActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private TextInputEditText mClaveActual;
    private TextInputEditText mClaveNva;
    private TextInputEditText mClaveConfirm;
    private Context context;
    private ProgressDialog progressDialog;
    private View cambioClaveContainer;
    private Button mBtnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio_clave);


        mClaveActual =  findViewById(R.id.cp_claveact);
        mClaveNva =  findViewById(R.id.cp_nvaclave);
        mClaveConfirm =  findViewById(R.id.cp_confirmaclave);
        cambioClaveContainer = findViewById(R.id.cambio_claveContainer);


        TextInputLayout inputContrasenaActual =  findViewById(R.id.textInputContrasenaActual);
        TextInputLayout inputContrasenaNva =  findViewById(R.id.textInputContrasenaNva);
        TextInputLayout inputContrasenaConf =  findViewById(R.id.textInputContrasenaConf);

        int colorInt = getResources().getColor(R.color.colorBlanco);
        ColorStateList csl = ColorStateList.valueOf(colorInt);

        inputContrasenaActual.setHintTextColor(csl);
        inputContrasenaNva.setHintTextColor(csl);
        inputContrasenaConf.setHintTextColor(csl);

        context = CambioClaveActivity.this;


        Toolbar toolbar =  findViewById(R.id.toolbar_clave);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Cambio de Contraseña");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mBtnGuardar = findViewById(R.id.cambio_clave_btn_guardar);


        mClaveConfirm.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                closeKeyboard();
                mBtnGuardar.performClick();
                handled = true;
            }
            return handled;
        });


        mBtnGuardar.setOnClickListener(v -> {

            SharedPreferences v_preferences = getSharedPreferences("session", Context.MODE_PRIVATE);

            String clave = v_preferences.getString("clave", "");

            if(mClaveActual.getText().toString().trim().isEmpty()){

                mClaveActual.setError("Debe Introducir la contraseña actual");

                mClaveActual.requestFocus();

            }else if (mClaveNva.getText().toString().trim().isEmpty() || (mClaveNva.length()<6)) {

                mClaveNva.setError("La contraseña debe ser mayor o igual a seis (6) caracteres");

                mClaveNva.requestFocus();

            }else if (mClaveConfirm.getText().toString().trim().isEmpty()) {

                mClaveConfirm.setError("Debe Repetir la contraseña nueva");

                mClaveConfirm.requestFocus();

            }else {

                if(!mClaveActual.getText().toString().equals(clave)){

                    mClaveActual.setError("La contraseña actual es incorrecta");

                    mClaveActual.requestFocus();

                }else  if (mClaveNva.getText().toString().equals(clave)){

                    mClaveNva.setError("La contraseña no puede ser igual a la anterior");

                    mClaveNva.requestFocus();

                }else if (!mClaveNva.getText().toString().equals(mClaveConfirm.getText().toString())) {

                    mClaveConfirm.setError("La contraseña no son iguales");

                    mClaveConfirm.requestFocus();

                } else {

                    actualizar();

                }
            }

         });


        mClaveActual.requestFocus();
    }


    private void actualizar() {

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);;
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        cambioClaveContainer.setVisibility(View.GONE);

        SharedPreferences v_preferences = getSharedPreferences("session", Context.MODE_PRIVATE);

        String token = v_preferences.getString("token", "");


        Call<PerfilResponse> call = PuertomarApiAdapter.getApiService().actualizarPerfil(getJsonEncode(),
                "Bearer " + token);

        call.enqueue(new ActualizarCallback());
    }


    class ActualizarCallback implements Callback<PerfilResponse> {

        @Override
        public void onResponse(Call<PerfilResponse> call, Response<PerfilResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                PerfilResponse actualizarResponse = response.body();

                if (actualizarResponse.isPerfil()) {

                    setSession();

                    Toast.makeText(context, "Cambio de clave exitoso", Toast.LENGTH_LONG).show();

                    Intent intent=new Intent();
                    intent.putExtra("clave",mClaveNva.getText().toString());
                    setResult(2,intent);
                    finish();


                }

            } else {
                cambioClaveContainer.setVisibility(View.VISIBLE);
                try {

                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();

                    Log.e(TAG, " Error Update: " + jObjError.toString().toString());

                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(Call<PerfilResponse> call, Throwable t) {

            progressDialog.dismiss();

            Log.e(TAG, " Error:: " + t.getLocalizedMessage());

            Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    public void setSession(){


        //obtiene la referencia a la preferencia de la aplicacion
        SharedPreferences v_preferences = getSharedPreferences("session",Context.MODE_PRIVATE);
        //obtiene el editor de las preferencias
        SharedPreferences.Editor v_editor = v_preferences.edit();

        v_editor.putString("clave", mClaveNva.getText().toString());

        //confirma los cambios realizados
        v_editor.commit();


    }


    public RequestBody getJsonEncode() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("clave", ""+mClaveNva.getText().toString());

        } catch (JSONException e) {

        }

        Log.e(TAG, obj.toString());

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), obj.toString());

        return body;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                //finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.actions_registro, menu);


        //MenuItem menuItem = menu.findItem(R.menu.actions_search2);
        //menuItem.setIcon(buildCounterDrawable(appBadgeCount, R.drawable.ic_action_mi_compra));
        return true;

    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }


}
