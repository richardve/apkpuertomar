package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goodmarcom.apk.Adapter.DireccionesAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.DireccionesResponse;
import com.goodmarcom.apk.Enty.Ciudad;
import com.goodmarcom.apk.Enty.Direcciones;
import com.goodmarcom.apk.R;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DireccionActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices direccioneService;

    //private Button fabAgregar;

    private RecyclerView recycler;

    private DireccionesAdapter adapter;

    private ArrayList<Direcciones> mDirecciones = new ArrayList<>();

    private String token;

    private String mCociudad;

    private ProgressDialog progressDialog;

    private Context context;

    private Direcciones  mClienteDireccion = new Direcciones();

    private boolean update=false;

    private Integer mIdDireccion;

    private Integer mPosition;

    private Bundle parametros;

    private String direccion;

    private String ciudad;

    private String co_postal;

    private boolean in_empresa;

    private LinearLayoutManager lManager;

    private Double latitud;

    private Double longitud;

    private String sector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direccion);

        context = DireccionActivity.this;

        Toolbar toolbar_seg =  findViewById(R.id.toolbar_dir);

        setSupportActionBar(toolbar_seg);


        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Direcciones de Entrega");

        }

        SharedPreferences v_preferences = getSharedPreferences("session",Context.MODE_PRIVATE);


        token = v_preferences.getString("token","" );

        in_empresa = v_preferences.getBoolean("in_empresa",false);

        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        FloatingActionButton fabAgregar =  findViewById(R.id.dir_fab_agregar);

        fabAgregar.setBackgroundTintList(csl);
        fabAgregar.setColorFilter(Color.WHITE);


        fabAgregar.setOnClickListener(view -> {

            if(in_empresa) {

                Intent Principal = new Intent(context, AgregarDireccionActivity.class);

                parametros = new Bundle();

                update = false;

                parametros.putBoolean("update", update);

                Principal.putExtras(parametros);

                startActivityForResult(Principal, 2);
            }else{
                Toast.makeText(context, "No puede agregar dirección sin haber registrado una empresa", Toast.LENGTH_LONG).show();
            }

        });

        recycler = findViewById(R.id.recycler_view_dir);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        adapter = new DireccionesAdapter(mDirecciones);


        adapter.setClickListener(v -> {
            mPosition = adapter.getmPosition();

            Log.e(TAG, "Posición Activity:"+mPosition);

            Direcciones direcciones = adapter.getItem(mPosition);

            mIdDireccion = direcciones.getCo_direccion();

            mCociudad = direcciones.getCo_ciudad();

            direccion = direcciones.getTx_direccion();

            ciudad = direcciones.getTxCiudad();

            co_postal = direcciones.getTx_codigo_postal();

            latitud = direcciones.getLatitud();

            longitud = direcciones.getLongitud();

            sector = direcciones.getTx_sector();

            update = true;

            Intent Principal = new Intent(context, AgregarDireccionActivity.class);

            parametros = new Bundle();

            parametros.putBoolean("update", update);

            parametros.putInt("mIdDireccion", mIdDireccion);

            parametros.putString("mCociudad", mCociudad);

            parametros.putString("direccion", direccion);

            parametros.putString("ciudad", ciudad);

            parametros.putString("co_postal", co_postal);

            parametros.putDouble("latitud", latitud);

            parametros.putDouble("longitud", longitud);

            parametros.putString("sector", sector);


            Log.e(TAG, "ID Dirección Enviada:"+mIdDireccion);

            Principal.putExtras(parametros);

            startActivityForResult(Principal, 2);

        });


        adapter.setmClickListenerEliminar(v -> {

            mPosition = adapter.getmPosition();

            Direcciones direcciones = adapter.getItem(mPosition);

            eliminarDireccion(direcciones.getCo_direccion(),mPosition);

            Log.e(TAG, "Eliminar Posición:"+mPosition);

       }


        );

        recycler.setAdapter(adapter);

        direccioneService = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        //progressDialog.show();

        obtenerDatos();

    }

    private void obtenerDatos() {
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        callDireccionesApi().enqueue(new Callback<DireccionesResponse>() {
            @Override
            public void onResponse(Call<DireccionesResponse> call, Response<DireccionesResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<Direcciones> direcciones = fetchResults(response);

                    mDirecciones.addAll(direcciones);
                    adapter.notifyDataSetChanged();


                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<DireccionesResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Call<DireccionesResponse> callDireccionesApi() {

        return  direccioneService.getDirecciones("Bearer "+token);
    }

    private List<Direcciones> fetchResults(Response<DireccionesResponse> response) {
        DireccionesResponse cuentaEntidadResponse = response.body();
        return cuentaEntidadResponse.getClienteDireccion();
    }


    public void eliminarDireccion(Integer id_direccion, Integer position) {

        //progressDialog.show();

        Call<DireccionesResponse> call = direccioneService.deleteDireccion("Bearer " + token, id_direccion);

        call.enqueue(new EliminarCallback(position));

    }

    private class EliminarCallback implements Callback<DireccionesResponse>
    {
        private int position;

        public EliminarCallback(int position) {

            this.position = position;
        }


        @Override
        public void onResponse(Call<DireccionesResponse> call, Response<DireccionesResponse> response) {

            if(response.isSuccessful()){

                Toast.makeText(context, "Dirección eliminada con éxito!", Toast.LENGTH_SHORT).show();

                mDirecciones.removeAll(mDirecciones);
                adapter.notifyDataSetChanged();
                recycler.removeAllViewsInLayout();
                obtenerDatos();


            }else{

                try {

                    assert response.errorBody() != null;
                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    JSONArray arrJson = jObjError.getJSONArray("error");

                    Toast.makeText(context, arrJson.get(0).toString(), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<DireccionesResponse> call, Throwable t) {
           // progressDialog.dismiss();

            Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();

            Log.e("ERROR: ", t.getMessage());
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( data != null)
        {
            Bundle parametros;

            switch (requestCode) {

                case 2:

                    parametros = data.getExtras();

                    mClienteDireccion.setCo_direccion(mIdDireccion);
                    mClienteDireccion.setTx_direccion(parametros.getString("mTxdireccion"));
                    mClienteDireccion.setCo_ciudad(parametros.getString("mCociudad"));
                    mClienteDireccion.setTx_codigo_postal(parametros.getString("co_postal"));
                    mClienteDireccion.setLatitud(parametros.getDouble("latitud"));
                    mClienteDireccion.setLongitud(parametros.getDouble("longitud"));

                    Ciudad c = new Ciudad();
                    c.setTx_ciudad(parametros.getString("ciudad"));

                    mClienteDireccion.setCiudad(c);

                    Log.e(TAG, "Ciudad:"+c.getTx_ciudad());

                    Log.e(TAG, "Update:"+update);

                    Log.e(TAG, "Posición Result:"+mPosition);

                    Log.e(TAG, "mIdDireccion Recibida:"+parametros.getInt("mIdDireccion"));


                    mDirecciones.removeAll(mDirecciones);
                    adapter.notifyDataSetChanged();
                    recycler.removeAllViewsInLayout();
                    obtenerDatos();


                  /*  if(update) {
                        mDirecciones.set(mPosition, mClienteDireccion);
                        adapter.notifyItemChanged(mPosition);
                        recycler.invalidate();
                    }else
                    {
                        mClienteDireccion.setCo_direccion(parametros.getInt("mIdDireccion"));
                        mDirecciones.add(mClienteDireccion);
                        adapter.notifyItemInserted(adapter.getItemCount());
                        recycler.invalidate();
                    }*/

                    break;


                default:

            }

        }


    }


    public void resetAdapterState() {
        adapter = (DireccionesAdapter) recycler.getAdapter();
        recycler.setAdapter(adapter);
    }

}
