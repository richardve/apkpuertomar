package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.Marker;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.DireccionesGuardarResponse;
import com.goodmarcom.apk.Api.response.DireccionesResponse;
import com.goodmarcom.apk.Enty.Direcciones;
import com.goodmarcom.apk.R;


import org.json.JSONException;
import org.json.JSONObject;

//import java.util.ArrayList;

//import androidx.appcompat.app.AppCompatActivity;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgregarDireccionActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private static final int MAP_REQUEST_CODE = 1;

    private PuertomarApiServices apiService;

    private String token;

    private String mCociudad;

    private ProgressDialog progressDialog;

    private Context context;

    //private TextView mCiudad;

    private TextInputEditText mTxCiudad;

    private String ciudad;

    private String direccion;

    private TextInputEditText mTxdireccion;

    //private TextView mLbdireccion;

    private TextInputEditText mTxsector;

    private TextInputEditText mCoPostal;

    private String co_postal;

    private boolean update;

    private Integer mIdDireccion;

    private Direcciones  mClienteDireccion = new Direcciones();

    //private GoogleMap mMap;
    //private Marker mCurrLocationMarker;
    private SharedPreferences v_preferences;
    //private String codiso;

    private UiSettings mUiSettings;

    private String sector;
    private String pais;
    //private String ubicacion;
    private Double latitud=0.00;
    private Double longitud=0.00;
    private View direccionContainer;
    private  FloatingActionButton mBtnConfirmar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_direccion);

        context = AgregarDireccionActivity.this;


        v_preferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);

        //codiso = v_preferences.getString("codIsoPais","" );
        pais = v_preferences.getString("pais","" );

        mTxCiudad =  findViewById(R.id.dir_tx_ciudad);

        direccionContainer = findViewById(R.id.direccionContainer);

        mTxsector = findViewById(R.id.dir_tx_sector);

        mCoPostal = findViewById(R.id.dir_codigo_postal);

        mTxdireccion = findViewById(R.id.dir_tx_direccion);

        mTxdireccion.setOnClickListener(v -> {

            mTxdireccion.setEnabled(false);

            Intent Principal = new Intent(context, MapsActivity.class);
            Principal.putExtra("ciudad",ciudad); //ojo
            Principal.putExtra("sector", mTxsector.getText().toString());
            Principal.putExtra("latitud",latitud);
            Principal.putExtra("longitud", longitud);
            Principal.putExtra("update", update);

            startActivityForResult(Principal, MAP_REQUEST_CODE);


        });

        mTxsector.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                closeKeyboard();
                mBtnConfirmar.performClick();
                handled = true;
            }
            return handled;
        });

        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        mBtnConfirmar =  findViewById(R.id.dir_btn_confirmar);

        mBtnConfirmar.setBackgroundTintList(csl);
        mBtnConfirmar.setColorFilter(Color.WHITE);

        mBtnConfirmar.setOnClickListener(v -> {

            if(mTxCiudad.getText().toString().trim().isEmpty())
            {
                mTxCiudad.setError("Debe seleccionar una ciudad");

            }else if(mCoPostal.getText().toString().trim().isEmpty()){

                mCoPostal.setError("Campo requerido");
                mCoPostal.requestFocus();

            }else if(mTxsector.getText().toString().trim().isEmpty()){

                mTxsector.setError("Campo requerido");
                mTxsector.requestFocus();

            } else if (mTxdireccion.getText().toString().trim().isEmpty()) {

                    mTxdireccion.performClick();

            }else {

                progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
                progressDialog.setCancelable(false);

                mClienteDireccion.setTx_direccion(mTxdireccion.getText().toString());

                mClienteDireccion.setCo_ciudad(mCociudad);

                mClienteDireccion.setTx_codigo_postal(mCoPostal.getText().toString());

                mClienteDireccion.setLatitud(latitud);

                mClienteDireccion.setLongitud(longitud);

                mClienteDireccion.setTx_sector(mTxsector.getText().toString());

                progressDialog.show();
                progressDialog.setContentView(R.layout.progress_dialog);
                direccionContainer.setVisibility(View.GONE);

                if (update) {
                    updateDireccion(mIdDireccion, mClienteDireccion);
                } else {
                    guardarDireccion(mClienteDireccion);
                }

                progressDialog.dismiss();
            }


        });


        mTxCiudad.setOnClickListener(view -> {

            Intent Principal = new Intent(context, ListCiudadesActivity.class);

            startActivityForResult(Principal, 2);

        });


        Toolbar toolbar_add_dir =  findViewById(R.id.toolbar_add_dir);

        setSupportActionBar(toolbar_add_dir);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Direcciones de Entrega");

        }

        SharedPreferences v_preferences = getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        ciudad = v_preferences.getString("ciudad", "");

        mCociudad  = v_preferences.getString("cociudad", "");

        mTxCiudad.setText(ciudad);

        //ubicacion = pais+","+ciudad+","+sector;



        Bundle parametros = this.getIntent().getExtras();

        if (parametros!=null) {
            update = parametros.getBoolean("update");
            if (update)
            {
                mIdDireccion = parametros.getInt("mIdDireccion");
                mCociudad = parametros.getString("mCociudad");
                direccion = parametros.getString("direccion");
                ciudad = parametros.getString("ciudad");
                co_postal = parametros.getString("co_postal");
                latitud = parametros.getDouble("latitud");
                longitud = parametros.getDouble("longitud");
                sector = parametros.getString("sector");
                mTxCiudad.setText(ciudad);
                mTxdireccion.setText(direccion);
                mTxsector.setText(sector);
                mCoPostal.setText(co_postal);

            }

            Log.e(TAG, "update:"+update);
            Log.e(TAG, "mIdDireccion Recibida:"+mIdDireccion);
            Log.e(TAG, "mCociudad:"+mCociudad);
            Log.e(TAG, "ciudad:"+ciudad);
            Log.e(TAG, "Sector:"+ciudad);



        }



        apiService = PuertomarApiAdapter.getApiService();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( data != null)
        {
            Bundle parametros, coordenadas;

            switch (requestCode) {

                case 2:

                    parametros = data.getExtras();

                    mCociudad = parametros.getString("mIdCiudad");

                    mTxCiudad.setText(parametros.getString("tx_ciudad"));

                    Log.e(TAG, "mCociudad:"+mCociudad);

                    break;

                case MAP_REQUEST_CODE:

                    coordenadas = data.getExtras();

                    latitud = coordenadas.getDouble("latitud");
                    longitud = coordenadas.getDouble("longitud");

                    Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitud, longitud, 1);
                        if (addresses != null) {
                            Address returnedAddress = addresses.get(0);
                            mTxCiudad.setText(addresses.get(0).getLocality());
                            if(mCoPostal.getText().toString().trim().isEmpty()) mCoPostal.setText(addresses.get(0).getPostalCode());
                            mTxdireccion.setText(addresses.get(0).getAddressLine(0));

                            Log.e(TAG, "Dirección Completa:"+returnedAddress);
                        } else {
                            Log.e(TAG, "NoAddress returned!");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "Canont get Address!");
                    }

                    break;

                default:

            }

        }

    }


    public void guardarDireccion(Direcciones d){

        Call<DireccionesGuardarResponse> call = apiService.guardarDireccion("Bearer "+token, getJsonEncode(d));
        call.enqueue(new Callback<DireccionesGuardarResponse>() {
            @Override
            public void onResponse(Call<DireccionesGuardarResponse> call, Response<DireccionesGuardarResponse> response) {
                if(response.isSuccessful()){

                        Toast.makeText(context, "Dirección agregada con éxito!!", Toast.LENGTH_SHORT).show();

                        mIdDireccion = response.body().getCo_direccion();

                        Log.e(TAG,"CoDirección Guardar:"+mIdDireccion);

                        setParametros();

                    try {
                        JSONObject codigo = new JSONObject(response.body().toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    progressDialog.dismiss();
                    direccionContainer.setVisibility(View.VISIBLE);

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<DireccionesGuardarResponse> call, Throwable t) {

                progressDialog.dismiss();

                Log.e("ERROR: ", t.getMessage());
            }
        });
    }


    public void updateDireccion(Integer id_direccion, Direcciones d){

        Log.e(TAG, "Token:"+token);
        Log.e(TAG, " Update " + update);
        Log.e(TAG, "ID Dirección Para Guardar:"+mIdDireccion);

        Call<DireccionesResponse> call = apiService.updateDireccion(id_direccion,"Bearer "+token,  getJsonEncode(d));
        call.enqueue(new Callback<DireccionesResponse>() {
            @Override
            public void onResponse(Call<DireccionesResponse> call, Response<DireccionesResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(context, "Dirección actualizada con éxito", Toast.LENGTH_SHORT).show();
                    setParametros();

                }else{
                    progressDialog.dismiss();
                    direccionContainer.setVisibility(View.VISIBLE);

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<DireccionesResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }


    public RequestBody getJsonEncode(Direcciones direccion) {

        Log.e(TAG, "CoCiudad:"+direccion.getCo_ciudad());

        JSONObject obj = new JSONObject();
        try {
            obj.put("direccion", ""+ direccion.getTx_direccion());
            obj.put("ciudad", ""+direccion.getCo_ciudad());
            obj.put("codigo_postal", ""+direccion.getTx_codigo_postal());
            obj.put("latitud", ""+direccion.getLatitud());
            obj.put("longitud", ""+direccion.getLongitud());
            obj.put("sector", ""+direccion.getTx_sector());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e(TAG,"Obj Dir:"+obj.toString());

        MediaType JSON = MediaType.get("application/json; charset=utf-8");
        RequestBody body = RequestBody.Companion.create(obj.toString(),JSON);

        //RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), obj.toString());

        return  body;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.actions_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }


    public void setParametros(){

        Intent intent=new Intent();

        Bundle parametros = new Bundle();

        if(mIdDireccion!=null) parametros.putInt("mIdDireccion", mIdDireccion);
        parametros.putString("mTxdireccion", mTxdireccion.getText().toString());
        parametros.putString("mCociudad", mCociudad);
        parametros.putString("ciudad", mTxCiudad.getText().toString());
        parametros.putString("co_postal", mCoPostal.getText().toString());
        parametros.putDouble("latitud", latitud);
        parametros.putDouble("longitud", longitud);


        intent.putExtras(parametros);
        setResult(2,intent);
        finish();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mTxdireccion.setEnabled(true);

    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

}
