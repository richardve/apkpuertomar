package com.goodmarcom.apk.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.goodmarcom.apk.R;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,  GoogleMap.OnMarkerDragListener {

    private static final String TAG = "PUERTOMAR";

    private  static final int PETICION_PERMISO_LOCALIZACION = 10;

    private static final int AUTOCOMPLETE_REQUEST_CODE = 1;

    private GoogleMap mMap;
    private Marker mCurrLocationMarker;
    private SharedPreferences v_preferences;
    private String codiso;

    private UiSettings mUiSettings;

    private String ciudad;
    private String sector;
    private String pais;
    private Context context;

    private Double latitud;
    private Double longitud;
    private boolean update;
    //private FusedLocationProviderClient fusedLocationClient;
    private GoogleApiClient mGoogleApiClient;
    private boolean isPermiso;


    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mFusedLocationClient  = LocationServices.getFusedLocationProviderClient(this);

        Toolbar toolbar =  findViewById(R.id.toolbar_place);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Buscar");

        context = MapsActivity.this;

        v_preferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);

        codiso = v_preferences.getString("codIsoPais","" );

        Log.e(TAG, "Codido iso:"+codiso);

        pais = v_preferences.getString("pais","" );

        Bundle parametros=this.getIntent().getExtras();

        ciudad = parametros.getString("ciudad");
        sector = parametros.getString("sector");
        latitud = parametros.getDouble("latitud");
        longitud = parametros.getDouble("longitud");
        update  =  parametros.getBoolean("update");


        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        }


        Button mBtnConfirmar =  findViewById(R.id.maps_dir_btn_confirmar);

        mBtnConfirmar.setOnClickListener(v -> {

            Intent intent=new Intent();

            Bundle coordenadas = new Bundle();

            coordenadas.putDouble("latitud", latitud);
            coordenadas.putDouble("longitud",longitud);

            intent.putExtras(coordenadas);
            setResult(1,intent);
            finish();

        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mUiSettings = mMap.getUiSettings();
        // Keep the UI Settings state in sync with the checkboxes.
        mUiSettings.setZoomControlsEnabled(true);
        //mUiSettings.setCompassEnabled(isChecked(R.id.compass_toggle));
        mUiSettings.setMyLocationButtonEnabled(true);
       // mMap.setMyLocationEnabled(true);
        mUiSettings.setScrollGesturesEnabled(true);
        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setTiltGesturesEnabled(true);
        //mUiSettings.setRotateGesturesEnabled(isChecked(R.id.rotate_toggle));*/

        Log.e(TAG,"Mapa, Coordenadas mi ubicacion: Latitud"+latitud + "Longitud"+ longitud);

      if(update) {

          final LatLng user = new LatLng(latitud, longitud);

          mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(user).title("Mi Dirección"));
          mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
          mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(user, 16.0f));
          mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

      }else{
          //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
          mLocationRequest = new LocationRequest();
          mLocationRequest.setInterval(120000); // two minute interval
          mLocationRequest.setFastestInterval(120000);
          mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

          if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
              if (ContextCompat.checkSelfPermission(this,
                      Manifest.permission.ACCESS_FINE_LOCATION)
                      == PackageManager.PERMISSION_GRANTED) {
                  //Location Permission already granted
                  mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                  mMap.setMyLocationEnabled(true);
              } else {
                  //Request Location Permission
                  checkLocationPermission();
              }
          }
          else {
              mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
              mMap.setMyLocationEnabled(true);
          }
      }



        //String ubicacion = pais+","+ciudad+","+sector;

        //Log.e(TAG,"Ubicacion:"+ubicacion);

     /*  Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        try
        {
            List<Address> addresses = geoCoder.getFromLocationName(ubicacion, 5);
            if (addresses.size() > 0)
            {
              if(!update) {
                  latitud = (double) (addresses.get(0).getLatitude());
                  longitud = (double) (addresses.get(0).getLongitude());
              }

                final LatLng user = new LatLng(latitud, longitud);*/

             /*   mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(user).title(ubicacion));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(user, 16.0f));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);*/
       /*     }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
*/

            mMap.setOnMarkerDragListener(this);

            mMap.setOnMapClickListener(latlng -> {
            // TODO Auto-generated method stub
                latitud = latlng.latitude;
                longitud = latlng.longitude;

            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }
            mCurrLocationMarker = mMap.addMarker(new MarkerOptions()
                    .position(latlng)
                    .title("Mi Dirección")
                    .icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
            Log.e(TAG, "Posición: " + latlng);

            System.out.println(latlng);

        });
}

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

        MarkerOptions markerOptions = new MarkerOptions();
        // New Marker Position
        LatLng position = marker.getPosition();

        Log.e(TAG, "Posición Drag: " + position);
// Remove Old Marker
        mCurrLocationMarker.remove();
// Add New marker position
        markerOptions.position(position);
// Add Title on Marker
        markerOptions.title(getCompleteAddressString(position.latitude, position.longitude));
// Set Draggable
        markerOptions.draggable(true);
// Set Icon of Marker
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
// Add marker on map
        mCurrLocationMarker = mMap.addMarker(markerOptions);
// Show Info window on marker
        marker.showInfoWindow();

    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.i(TAG, "" + strReturnedAddress.toString());
            } else {
                Log.i(TAG, "NoAddress returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "Canont get Address!");
        }
        return strAdd;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actions_autocomplete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.search_place:
                onSearchCalled();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());
                //Toast.makeText(context, "ID: " + place.getId() + "address:" + place.getAddress() + "Name:" + place.getName() + " latlong: " + place.getLatLng(), Toast.LENGTH_LONG).show();
                String address = place.getAddress();

                latitud = place.getLatLng().latitude;
                longitud = place.getLatLng().longitude;

                final LatLng user = new LatLng(latitud, longitud);

                mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(user).title(address));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(user, 17.0f));
                // Zoom in, animating the camera.
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    public void onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .setCountry(codiso)
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PETICION_PERMISO_LOCALIZACION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permiso Denegado", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }


    LocationCallback mLocationCallback = new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }
                latitud = location.getLatitude();
                longitud = location.getLongitude();
                //Place current location marker
                LatLng latLng = new LatLng(latitud, longitud);
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Mi Ubicación");
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                mCurrLocationMarker = mMap.addMarker(markerOptions);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

                //move map camera
                //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
            }
        };

    };

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                MaterialAlertDialogBuilder confirmDialog = new MaterialAlertDialogBuilder(this);

                confirmDialog.setTitle("Permiso de Localizción requerido")
                        .setMessage("Esta aplicación necesita permiso de localización, acepta otorgar este permiso?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        PETICION_PERMISO_LOCALIZACION );
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PETICION_PERMISO_LOCALIZACION );
            }
        }
    }




}
