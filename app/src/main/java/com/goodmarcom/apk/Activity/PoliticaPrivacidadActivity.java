package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.PoliticasPrivacidadResponse;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Response;

public class PoliticaPrivacidadActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private String token;
    private Context context;
    private ProgressDialog progressDialog;
    WebView myWebView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_politica_privacidad);

        context = PoliticaPrivacidadActivity.this;


        Toolbar toolbar_pol =  findViewById(R.id.toolbar_pol);

        setSupportActionBar(toolbar_pol);


        if (getSupportActionBar()!= null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Política de Privacidad");

        }


        myWebView = findViewById(R.id.webview_pol);

        //obtiene la referencia a la preferencia de la aplicacion
        SharedPreferences v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","");

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);


        obtenerDatos();

    }


    private void obtenerDatos() {

        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        Call<PoliticasPrivacidadResponse> call = PuertomarApiAdapter.getApiService().getPoliticasPrivacidad("Bearer " + token);
        call.enqueue(new PoliticasPrivacidadCallback());

    }

    private class PoliticasPrivacidadCallback implements retrofit2.Callback<PoliticasPrivacidadResponse> {
        @Override
        public void onResponse(Call<PoliticasPrivacidadResponse> call, Response<PoliticasPrivacidadResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                PoliticasPrivacidadResponse politicasPrivacidadResponse = response.body();

                if(politicasPrivacidadResponse.isPoliticas()){

                    Log.e(TAG,  "Politicas:" + politicasPrivacidadResponse.getSuccess());

                    String data = politicasPrivacidadResponse.getSuccess();

                    myWebView.loadData(data, "text/html; charset=utf-8", "UTF-8");

                }


            } else {

                try {

                    progressDialog.dismiss();
                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(Call<PoliticasPrivacidadResponse> call, Throwable t) {

            progressDialog.dismiss();

            Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
