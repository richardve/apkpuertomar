package com.goodmarcom.apk.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.FcmResponse;
import com.goodmarcom.apk.Db.DBHelper;
import com.goodmarcom.apk.Fragments.CarlistFragment;
import com.goodmarcom.apk.Fragments.CatalogoGrupoFragment;
import com.goodmarcom.apk.Fragments.ConfiguracionFragment;
import com.goodmarcom.apk.Fragments.OpcionPediosFragment;
import com.goodmarcom.apk.Fragments.RecetasFragment;
import com.goodmarcom.apk.Notification.NotificationCountSetClass;
import com.goodmarcom.apk.R;

import org.json.JSONObject;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    public static String currentTab = "";
    private String fragment_origen;
    private SharedPreferences v_preferences;
    //private FragmentManager fm;
    private DBHelper v_db_helper;
    private  BottomNavigationView navigation;
    public static int notificationCountCart = 0;
    public static MenuItem mItem;
    public static Context mContext;
    public static Menu mMenu;
    private Context context;

    private String token;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            //navigation.setSelectedItemId(item.getItemId());

            switch (item.getItemId()) {

              /*  case R.id.navigation_home:

                    if(!currentTab.equals("home")) {

                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_content, new MainFragment(),"home")
                                .commit();
                        currentTab = "home";
                    }
                    return true;*/

                case R.id.navigation_catalogo:

                    if(!currentTab.equals("catalogo") ) {

                        Log.e("PUERTOMAR", "En el main: in_empresa" +  v_preferences.getBoolean("in_empresa",false));

                        if(v_preferences.getBoolean("in_empresa",false)){
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_content, new CatalogoGrupoFragment(),"catalogo")
                                    .commit();
                            currentTab = "catalogo";

                        }else{
                            Intent RegistroEmp = new Intent(getApplicationContext(), RegistroEmpresaActivity.class);
                            startActivity(RegistroEmp);
                        }

                    }
                    return true;
                case R.id.navigation_pedidos:
                    if(!currentTab.equals("pedidos") ) {

                        if(v_preferences.getBoolean("in_empresa",false)) {

                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_content, new OpcionPediosFragment(),"pedidos")
                                    .commit();

                            //obtiene el editor de las preferencias
                            SharedPreferences.Editor v_editor = v_preferences.edit();
                            //crea el atributo que indica que se inicio la session
                            v_editor.putBoolean("pedido-change", false);
                            v_editor.putBoolean("pedido-init", false);
                            //confirma los cambios realizados
                            //v_editor.commit();
                            v_editor.apply();
                            currentTab = "pedidos";
                        }else{
                            Intent RegistroEmp = new Intent(getApplicationContext(), RegistroEmpresaActivity.class);
                            startActivity(RegistroEmp);
                        }
                    }
                    return true;

                case R.id.navigation_recetas:

                    if(!currentTab.equals("recetas")) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_content, new RecetasFragment(),"recetas")
                                .commit();
                        currentTab = "recetas";
                    }
                    return true;

                case R.id.navigation_ajustes:
                    if(!currentTab.equals("ajustes")) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_content, new ConfiguracionFragment(),"ajustes")
                                .addToBackStack(null)
                                .commit();
                        currentTab = "ajustes";
                    }
                    return true;
            }
            return false;
        }
    };

    private void iconCarShow(MenuItem item){
        String total = "0";

        //para evitar que se cree la base de datos sin estar el usuario conectado y asignado los valores al
        //preferences
        if(null != v_db_helper){
            total = v_db_helper.getTotalProductos();
        }
        //String total = v_db_helper.getTotalProductos();
        if(null == total  || total.equals("0")){
            //mMenu.findItem(R.id.action_cart).setVisible(false);
            item.setVisible(false);
        }
        else{
           // mMenu.findItem(R.id.action_cart).setVisible(true);
            item.setVisible(true);
        }
    }


    private int totalCar(){

        String total = v_db_helper.getTotalProductos();
        if(null == total){

           return 0;
        }
        else{
           return Integer.valueOf(total);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent v_intent = null;
       // fm = getSupportFragmentManager();

        context = MainActivity.this;

        v_preferences = this.getSharedPreferences("session",MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        mContext =  this;


        /*mBarra = findViewById(R.id.toolbar);
        setSupportActionBar(mBarra);*/

        //el usuario ya tiene la session iniciada
        if(v_preferences.getBoolean("logged-in",false)) {

            if(!v_preferences.getBoolean("update_fcm",false)) {
                guardarTokenFcm(v_preferences.getString("token_fcm", ""));
            }

            v_db_helper = new DBHelper(getApplicationContext());


            String total = v_db_helper.getTotalProductos();
            if(null == total)
                total="0";
            Log.e("main-activity",total);

            notificationCountCart = Integer.valueOf(total);

            if(v_preferences.getBoolean("in_empresa",false)){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_content, new CatalogoGrupoFragment(),"catalogo")
                        .commit();
                currentTab = "catalogo";

            }else{
                Intent RegistroEmp = new Intent(getApplicationContext(), RegistroEmpresaActivity.class);
                startActivity(RegistroEmp);
            }


            navigation =  findViewById(R.id.navigation);

            //removePaddingFromNavigationItem();
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        }


    }



    public void guardarTokenFcm(String tokenFcm){


        Call<FcmResponse> call = PuertomarApiAdapter.getApiService().postFcm("Bearer "+token,tokenFcm);

        call.enqueue(new FcmCallback());

    }


    private class FcmCallback implements retrofit2.Callback<FcmResponse> {
        @Override
        public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

            if(response.isSuccessful()){

                FcmResponse fcmResponse = response.body();

                if(fcmResponse.isFcm())
                {
                    //Toast.makeText(getApplicationContext(),fcmResponse.getSuccess(), Toast.LENGTH_LONG).show();

                    SharedPreferences.Editor v_editor = v_preferences.edit();

                    v_editor.putBoolean("update_fcm", true);

                    //confirma los cambios realizados
                    v_editor.commit();

                }

            }else{

                try {

                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());


                }

            }


        }

        @Override
        public void onFailure(Call<FcmResponse> call, Throwable t) {
            Toast.makeText(getApplicationContext(),"Error de conexión",Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        mMenu = menu;

        mItem = menu.findItem(R.id.action_cart);

        //mItem = item;

        iconCarShow(mItem);

        setBadge(notificationCountCart);

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_cart:

                if(totalCar()>0){

                    //mBarra.setVisibility(View.GONE);

                    if(!currentTab.equals("carrito")) fragment_origen = currentTab;

                    Bundle bundle = new Bundle();
                    bundle.putString("fragment_origen", fragment_origen);

                    CarlistFragment frg = new CarlistFragment();

                    frg.setArguments(bundle);

                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_content, frg,"carrito")
                            .addToBackStack("carrito")
                            .commit();
                   // mBarra.setTitle("Carrito");
                    currentTab = "carrito";

                }else{
                    Toast.makeText(getApplicationContext(),"No hay productos seleccionados", Toast.LENGTH_LONG).show();
                }

                    return true;

            case R.id.action_salir:

                MaterialAlertDialogBuilder exitDialog = new MaterialAlertDialogBuilder(this);

                exitDialog.setTitle("Alerta");
                exitDialog.setMessage("Desea Salir de la APP?");
                exitDialog.setCancelable(false);
                exitDialog.setPositiveButton("SI", (arg0, arg1) -> {

                    setSession(false);

                    Intent Principal = new Intent(getApplicationContext(), SlideActivity.class);

                    startActivity(Principal);

                    finish();
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();


                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public static void setBadge(int count){

        NotificationCountSetClass.setAddToCart(mContext, mItem, count);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void setSession( boolean login){

        //obtiene la referencia a la preferencia de la aplicacion
        // SharedPreferences v_preferences = this.getSharedPreferences("session",MODE_PRIVATE);

        //obtiene el editor de las preferencias
        SharedPreferences.Editor v_editor = v_preferences.edit();


        //crea el atributo que indica que se inicio la session
        v_editor.putBoolean("logged-in",login);
        v_editor.putString("empresa", "");
        v_editor.putString("replegal", "");
        v_editor.putString("dni", "");
        v_editor.putString("correo_emp", "");
        v_editor.putString("telefono", "");
        v_editor.putString("cociudad", "");
        v_editor.putString("ciudad", "");
        v_editor.putString("co_pais", "");
        v_editor.putString("pais", "");
        v_editor.putString("codIsoPais", "");
        v_editor.putString("direccion", "");


        v_editor.putString("usuario", "");
        v_editor.putString("correo_usuario", "");
        v_editor.putString("clave", "");
        v_editor.putString("token", token);
        v_editor.putString("imagen_perfil", "");
        v_editor.putBoolean("update_fcm", false);


        v_editor.putBoolean("in_activo",false);
        v_editor.putString("codmoneda", "");
        v_editor.putString("moneda", "");
        v_editor.putString("simbmoneda", "");
        v_editor.putString("formapago", "");
        v_editor.putBoolean("in_pedidos", false);
        v_editor.putBoolean("in_empresa", false);
        v_editor.putString("tipodoc", "");
        v_editor.putFloat("deuda_total", 0);


        //confirma los cambios realizados
        //v_editor.commit();

        v_editor.apply();


    }


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

}
