package com.goodmarcom.apk.Activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.EmpresaResponse;
import com.goodmarcom.apk.Enty.Clientes;
import com.goodmarcom.apk.Enty.TipoDocumento;
import com.goodmarcom.apk.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegistroEmpresaActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";


    private TextInputEditText mEmpresa;
    //private TextInputEditText mReplegal;
    private TextInputEditText mDocumento;
    private String mCorreo;
    private TextInputEditText mTelefono;
    private TextView mTviewTipoDocumento;


    private String codigoCiudad = null;
    private String codigoPais = "";
    private String codDocumento= "";
    private ProgressDialog progressDialog;
    private Context context;
    private TextInputLayout mEtDocumento;
    private TextInputEditText mTxPais;
    private TextInputEditText mTxCiudad;

    private SharedPreferences v_preferences;
    private String mTipodoc;
    private Boolean modificar;
    private Boolean inEmpresa;
    private Boolean perfil;
    private Boolean inPedido;
    private String ciudad;
    private String pais;
    private TipoDocumento[] td_class;
    private String tx_tipo_documento;
    private View registroempresaContainer;
    private Button mBtnConfirmar;
    private boolean isRegister;

    private PuertomarApiServices apiServices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_empresa);


        context = RegistroEmpresaActivity.this;

        isRegister = false;

        mEmpresa =  findViewById(R.id.rg_tx_empresa);
        //mReplegal =  findViewById(R.id.rg_tx_rep_legal);
        mDocumento = findViewById(R.id.rg_tx_documento);
        mTelefono =  findViewById(R.id.rg_tx_tlf);
        mEtDocumento = findViewById(R.id.rg_ft_documento);
        mTviewTipoDocumento = findViewById(R.id.rg_tipo_doc);
        registroempresaContainer = findViewById(R.id.registroempresaContainer);


        mTviewTipoDocumento.setEnabled(false);

        TextInputLayout inputNombreEmpresa =  findViewById(R.id.rg_input_nombEmpresa);
        //TextInputLayout inputRepLegal =  findViewById(R.id.rg_input_repbEmpresa);
        TextInputLayout inputPais =  findViewById(R.id.rg_pais);
        TextInputLayout inputCiudad =  findViewById(R.id.rg_ciudad);
        TextInputLayout inputTelefono =  findViewById(R.id.rg_telefono);


        int colorInt = getResources().getColor(R.color.colorBlanco);
        ColorStateList csl = ColorStateList.valueOf(colorInt);

        mEtDocumento.setHintTextColor(csl);
        inputNombreEmpresa.setHintTextColor(csl);
        //inputRepLegal.setHintTextColor(csl);
        inputPais.setHintTextColor(csl);
        inputCiudad.setHintTextColor(csl);
        inputTelefono.setHintTextColor(csl);


        mBtnConfirmar =  findViewById(R.id.reg_empresa_btn_confirmar);

        mBtnConfirmar.setOnClickListener(v -> {

            if (validarDatosEmpresa()) {

                Clientes mCliente = new Clientes();

                if (validarDatosEmpresa()) {

                    mCliente.setTx_empresa(mEmpresa.getText().toString());
                    // mCliente.setTx_representante_legal(mReplegal.getText().toString());
                    mCliente.setTx_documento(mDocumento.getText().toString());
                    mCliente.setTx_correo(mCorreo);
                    mCliente.setTx_tlf(mTelefono.getText().toString());
                    mCliente.setCo_ciudad(codigoCiudad);
                    mCliente.setCo_documento(codDocumento);

                    mCliente.setTx_ciudad(ciudad);
                    mCliente.setTx_pais(pais);

                    registrar(mCliente);

                }

            }


        });

        mTelefono.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                closeKeyboard();
                mBtnConfirmar.performClick();
                handled = true;
            }
            return handled;
        });



        mTxPais =  findViewById(R.id.reg_tx_pais);
        mTxCiudad =  findViewById(R.id.reg_tx_ciudad);


        mTxPais.setOnClickListener(view -> {

            Intent Principal = new Intent(context, ListPaisesActivity.class);

            startActivityForResult(Principal, 1);

        });


        mTxCiudad.setOnClickListener(view -> {


            if(mTxPais.getText().toString().isEmpty())
            {

                Toast.makeText(context, "Debe Seleccionar un País Válido", Toast.LENGTH_LONG).show();

            }else {

                Intent Principal = new Intent(context, ListCiudadesActivity.class);

                if(!codigoPais.isEmpty()){

                    Bundle parametros = new Bundle();

                    parametros.putString("co_pais", codigoPais);

                    Principal.putExtras(parametros);

                }

                startActivityForResult(Principal, 2);

            }

        });


        mTviewTipoDocumento.setOnClickListener(view -> {


            if(mTxPais.getText().toString().isEmpty())
            {

                Toast.makeText(context, "Debe Seleccionar un País Válido", Toast.LENGTH_LONG).show();

            }else {

                Intent Principal = new Intent(context, ListTipoDocumentoActivity.class);

                Bundle parametros = new Bundle();

                parametros.putString("mTipodoc", mTipodoc);

                Principal.putExtras(parametros);

                startActivityForResult(Principal, 3);
            }

        });


        apiServices = PuertomarApiAdapter.getApiService();

        v_preferences = getSharedPreferences("session", Context.MODE_PRIVATE);

        mCorreo = v_preferences.getString("correo_usuario", "");


        Toolbar toolbar = findViewById(R.id.toolbar_emp);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Registro del Cliente");

        toolbar.setNavigationOnClickListener(v -> {

           if(isRegister) {
               onBackPressed();
           }else {
               cerrarSession();
           }

        });

        //perfil = false;
        modificar = false;
        inPedido = false;

       // Bundle parametros = this.getIntent().getExtras();


        inEmpresa = v_preferences.getBoolean("in_empresa",false);

     /*  if (parametros!=null) {
           perfil = parametros.getBoolean("perfil");
       }
*/

     /*  if(perfil) {

           inPedido = v_preferences.getBoolean("in_pedidos", false);
           getSupportActionBar().setTitle("Datos del Cliente");
           mEmpresa.setText(v_preferences.getString("empresa", ""));
           mReplegal.setText(v_preferences.getString("replegal", ""));
           mDocumento.setText(v_preferences.getString("dni", ""));
           mEtDocumento.setHint(v_preferences.getString("tipodoc", "RUC"));
           mTelefono.setText(v_preferences.getString("telefono", ""));
           //mDireccion.setText(v_preferences.getString("direccion", ""));


           ciudad = v_preferences.getString("ciudad", "");
           pais = v_preferences.getString("pais", "");
           codigoCiudad = v_preferences.getString("cociudad", "");
           codDocumento = v_preferences.getString("codocumento", "");
           codigoPais = v_preferences.getString("co_pais", "");


           mTxPais.setText(pais);
           mTxCiudad.setText(ciudad);


           if(inPedido){

               mEmpresa.setFocusable(false);
               mReplegal.setFocusable(false);
               mDocumento.setFocusable(false);
               mTelefono.setFocusable(false);

               mEmpresa.setEnabled(false);
               mReplegal.setEnabled(false);
               mDocumento.setEnabled(false);
               mTelefono.setEnabled(false);
               mPais.setEnabled(false);
               mCiudad.setEnabled(false);
               mTviewTipoDocumento.setEnabled(false);



           } else {

               if (inEmpresa) {

                   //obtenerTipoDocumento();

                   modificar = true;

               }
           }

       }*/

    }

    private Boolean validarDatosEmpresa() {

        Boolean valido = false;

        if (mEmpresa.getText().toString().trim().isEmpty()) {

            mEmpresa.setError("Debe Introducir un nombre de Empresa");

            mEmpresa.requestFocus();

        } /*else if (mReplegal.getText().toString().trim().isEmpty()) {

            mReplegal.setError("Debe Introducir un Representante Legal");

            mReplegal.requestFocus();

        }*/ else if (mTxPais.getText().toString().isEmpty()) {

            Toast.makeText(context, "Debe Seleccionar un País Válido", Toast.LENGTH_LONG).show();

        }else if(mTxCiudad.getText().toString().isEmpty()){

            Toast.makeText(context, "Debe Seleccionar una Ciudad Válida", Toast.LENGTH_LONG).show();

        }else if (mDocumento.getText().toString().trim().isEmpty()) {

            mDocumento.setError("Debe Introducir una Documento");

            mDocumento.requestFocus();

        }else if(mTelefono.getText().toString().trim().isEmpty()){

            mTelefono.setError("Debe Introducir un Teléfono");

            mTelefono.requestFocus();

      }else {

            valido = true;
        }

        return valido;

    }


    private void registrar(Clientes perfil) {

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        registroempresaContainer.setVisibility(View.GONE);


        String token = v_preferences.getString("token", "");


        Log.e(TAG,"Token:"+token);


        Call<EmpresaResponse> call = apiServices.postRegisterEmpresa(getJsonEncode(perfil), "Bearer " + token);

        call.enqueue(new RegistroEmpresaActivity.RegisterCallback());
    }

    class RegisterCallback implements Callback<EmpresaResponse> {

        @Override
        public void onResponse(@NonNull Call<EmpresaResponse> call, Response<EmpresaResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                EmpresaResponse registerResponse = response.body();

                assert registerResponse != null;
                if(registerResponse.isEmpresa()){

                    setDatosSession(response.body(),true);

                    Toast.makeText(context,registerResponse.getSuccess(), Toast.LENGTH_LONG).show();

                    isRegister = true;

                   if(!modificar){
                       Intent Principal = new Intent(getApplicationContext(), MainActivity.class);
                       startActivity(Principal);
                   }
                   finish();

                }


            } else {
                registroempresaContainer.setVisibility(View.VISIBLE);
                try {

                    assert response.errorBody() != null;
                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();


                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(@NonNull Call<EmpresaResponse> call, Throwable t) {
            progressDialog.dismiss();
            Toast.makeText(context,getString(R.string.error_conexion),Toast.LENGTH_SHORT).show();
        }

    }


    public void setDatosSession(EmpresaResponse registerResponse, boolean registrado){

        ArrayList<Clientes> perfil = registerResponse.getPerfil();

        //obtiene la referencia a la preferencia de la aplicacion
        //SharedPreferences v_preferences = this.getSharedPreferences("datos",MODE_PRIVATE);


        //obtiene el editor de las preferencias
        SharedPreferences.Editor v_editor = v_preferences.edit();


        //crea el atributo que indica que se inicio la session
        v_editor.putBoolean("registrado-in",registrado);
        v_editor.putBoolean("logged-in",true);
       // v_editor.putString("token", token);

        v_editor.putBoolean("in_empresa", perfil.get(0).getInEmpresa());
        v_editor.putString("empresa", perfil.get(0).getTx_empresa());
        //v_editor.putString("replegal", perfil.get(0).getTx_representante_legal());
        v_editor.putString("replegal", perfil.get(0).getTx_empresa());
        v_editor.putString("dni", perfil.get(0).getTx_documento());
        v_editor.putString("correo_emp", perfil.get(0).getTx_correo());
        v_editor.putString("telefono", perfil.get(0).getTx_tlf());
        v_editor.putString("cociudad", perfil.get(0).getCo_ciudad());
        v_editor.putString("ciudad", perfil.get(0).getTx_ciudad());
        v_editor.putString("co_pais", perfil.get(0).getCo_pais());
        v_editor.putString("pais", perfil.get(0).getTx_pais());
        //v_editor.putString("direccion", perfil.get(0).getTx_direccion());

        v_editor.putString("codmoneda", perfil.get(0).getTx_codmoneda());
        v_editor.putString("moneda", perfil.get(0).getTx_moneda());
        v_editor.putString("simbmoneda", perfil.get(0).getTx_simbolo_moneda());
        v_editor.putString("formapago", perfil.get(0).getJsonFormaPago());

        v_editor.putString("codmoneda", perfil.get(0).getTx_codmoneda());
        v_editor.putString("moneda", perfil.get(0).getTx_moneda());
        v_editor.putString("simbmoneda", perfil.get(0).getTx_simbolo_moneda());
        v_editor.putString("tipodoc", perfil.get(0).getTx_documento_legal());
        v_editor.putString("codocumento",  perfil.get(0).getCo_documento());


        //confirma los cambios realizados
        //v_editor.commit();

        v_editor.apply();


    }


   /* private void obtenerTipoDocumento() {

        callServiceApi().enqueue(new Callback<PaisesResponse>() {
            @Override
            public void onResponse(Call<PaisesResponse> call, Response<PaisesResponse> response) {

                if(response.isSuccessful()){

                    List<Pais> paises = fetchResults(response);

                    for(Pais p:paises)
                    {
                        if(p.getCo_pais().equals(Integer.valueOf(codigoPais)))
                        {
                            mTipodoc = p.getJsonTipoDocumento();

                            Log.e(TAG, "mTipodoc GetPais:"+mTipodoc);

                            Gson gson = new Gson();
                            td_class = gson.fromJson(mTipodoc, TipoDocumento[].class);

                            if(td_class.length>1) {
                                mTviewTipoDocumento.setEnabled(true);
                            }else
                            {
                                mTviewTipoDocumento.setEnabled(false);
                            }

                            break;
                        }
                    }


                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                       // Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        //Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<PaisesResponse> call, Throwable t) {
                t.printStackTrace();
                //Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Call<PaisesResponse> callServiceApi() {

        return apiServices.getPaises("");

    }


    private List<Pais> fetchResults(Response<PaisesResponse> response) {
        PaisesResponse paisesResponse = response.body();
        return paisesResponse.getPaises();
    }*/



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( data != null)
        {
            Bundle parametros;

            switch (requestCode) {

                case 1:

                    parametros = data.getExtras();

                    pais = parametros.getString("tx_pais");

                    codigoPais = parametros.getString("mIdPais");

                    //codDocumento = parametros.getString("codDocumento");

                    mTipodoc = parametros.getString("mTipodoc");


                    Log.e(TAG, "mTipodoc Act:"+mTipodoc);


                    Gson gson = new Gson();
                    td_class = gson.fromJson(mTipodoc, TipoDocumento[].class);

                    mEtDocumento.setHint(td_class[0].getTx_documento());

                    codDocumento = String.valueOf(td_class[0].getCo_documento());

                    if(td_class.length>1) {
                        mTviewTipoDocumento.setEnabled(true);
                    }else
                    {
                        mTviewTipoDocumento.setEnabled(false);
                    }

                    mTxPais.setText(pais);

                    mTxCiudad.setText("");

                    break;


                case 2:

                    parametros = data.getExtras();

                    ciudad = parametros.getString("tx_ciudad");

                    codigoCiudad = parametros.getString("mIdCiudad");

                    mTxCiudad.setText(ciudad);

                    break;

                case 3:

                    parametros = data.getExtras();

                    //mIdTipoDocumento = parametros.getString("mIdTipoDocumento");

                    String tx_tipo_documento = parametros.getString("tx_tipo_documento");


                    mEtDocumento.setHint(tx_tipo_documento);

                    codDocumento = String.valueOf(parametros.getString("mIdTipoDocumento"));

                    break;

                default:

            }

        }

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }


    public RequestBody getJsonEncode(Clientes perfil) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("empresa", ""+perfil.getTx_empresa());
            obj.put("representante", ""+perfil.getTx_empresa());
            //obj.put("representante", ""+perfil.getTx_representante_legal());
            obj.put("email", ""+  perfil.getTx_correo());
            obj.put("telefono", ""+ perfil.getTx_tlf());
            obj.put("documento", ""+ perfil.getTx_documento());
            obj.put("ciudad", ""+ perfil.getCo_ciudad());
            obj.put("co_documento", ""+perfil.getCo_documento());


        } catch (JSONException e) {

        }

        Log.e(TAG, obj.toString());

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), obj.toString());

        return body;
    }


    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            //do actions like show message
            if(isRegister) {
                onBackPressed();
            }else {
                cerrarSession();
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void cerrarSession (){

        SharedPreferences.Editor v_editor = v_preferences.edit();

        v_editor.putBoolean("registrado-in",false);
        v_editor.putBoolean("logged-in",false);
        v_editor.apply();

        Intent Principal = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(Principal);
        finish();


    }

}
