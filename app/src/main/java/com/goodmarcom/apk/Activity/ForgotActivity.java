package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.ForgotResponse;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Response;

public class ForgotActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";
    private ProgressDialog progressDialog;
    private Button mBtnEnviar;
    private Context context;
    private TextInputEditText mUsuario;
    private TextView mClaveTemporal;
    private View forgotContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        context = ForgotActivity.this;

        mBtnEnviar =  findViewById(R.id.fg_btn_enviar);
        mClaveTemporal =  findViewById(R.id.fg_clave_temp);
        mUsuario = findViewById(R.id.forgot_usuario);
        forgotContainer = findViewById(R.id.forgotContainer);

        TextInputLayout inputUsuario =  findViewById(R.id.input_layout_usuario_forgot);


        int colorInt = getResources().getColor(R.color.colorBlanco);
        ColorStateList csl = ColorStateList.valueOf(colorInt);

        inputUsuario.setHintTextColor(csl);


        Toolbar toolbar = findViewById(R.id.toolbar_user);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Resetear Contraseña");

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        /*Inicializar el dialogo*/
        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);


        /*Bundle parametros = this.getIntent().getExtras();

        if (parametros!=null) mUsuario = parametros.getString("usuario");*/

        mUsuario.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                closeKeyboard();
                mBtnEnviar.performClick();
                handled = true;
            }
            return handled;
        });


        mBtnEnviar.setOnClickListener(v -> {

                if(mUsuario.getText().toString().trim().isEmpty())
                {
                    mUsuario.setError("Este campo es obligatorio");
                    mUsuario.requestFocus();


                }else {
                    enviarCorreo();
                }
        });

        mClaveTemporal.setOnClickListener(v -> {

            if(mUsuario.getText().toString().trim().isEmpty())
            {
                mUsuario.setError("Este campo es obligatorio");
                mUsuario.requestFocus();

            }else {
                mClaveTemporal.setEnabled(false);
                ActivityPin();
            }
        });

    }


    public void enviarCorreo(){

        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        forgotContainer.setVisibility(View.GONE);


        Log.e(TAG, " usuario: " + mUsuario.getText().toString());


        Call<ForgotResponse> call = PuertomarApiAdapter.getApiService().postForgot(mUsuario.getText().toString());

        call.enqueue(new ForgotCallback());

    }


    private class ForgotCallback implements retrofit2.Callback<ForgotResponse> {
        @Override
        public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {

            progressDialog.dismiss();

            if(response.isSuccessful()){

                ForgotResponse forgotResponse = response.body();

                if(forgotResponse.isForgot())
                {
                    Toast.makeText(context,forgotResponse.getSuccess(), Toast.LENGTH_LONG).show();

                    ActivityPin();

                }

            }else{
                forgotContainer.setVisibility(View.VISIBLE);
                try {

                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }


        }

        @Override
        public void onFailure(Call<ForgotResponse> call, Throwable t) {
            progressDialog.dismiss();
            Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
        }
    }

    public void  ActivityPin(){
        Intent Principal = new Intent(context,ForgotPinActivity.class);

        Bundle parametros = new Bundle();

        parametros.putString("usuario", mUsuario.getText().toString());

        Principal.putExtras(parametros);

        startActivity(Principal);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mClaveTemporal.setEnabled(true);

    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
