package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.ForgotResetResponse;
import com.goodmarcom.apk.Enty.Clientes;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Response;

public class ForgotResetActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";
    private ProgressDialog progressDialog;
    private Button mBtnGuardar;
    private Context context;
    private TextInputEditText mClaveNva;
    private TextInputEditText mClaveConfirm;
    private String mUsuario;
    private View forgotresetContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_reset);

        mClaveNva = findViewById(R.id.fpr_clave_nva);
        mClaveConfirm = findViewById(R.id.fpr_clave_nva_rp);
        mBtnGuardar = findViewById(R.id.fpr_btn_guardar);
        forgotresetContainer = findViewById(R.id.forgotresetContainer);


        /*para mantener el mismo tipo de letra en el campo contraseña*/
        mClaveNva.setTypeface(Typeface.DEFAULT);
        mClaveNva.setTransformationMethod(new PasswordTransformationMethod());
        mClaveConfirm.setTypeface(Typeface.DEFAULT);
        mClaveConfirm.setTransformationMethod(new PasswordTransformationMethod());

        context = ForgotResetActivity.this;


        /*Inicializar el dialogo*/
        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);


        TextInputLayout inputContrasenaNva1 =  findViewById(R.id.input_layout_clave_nva1);
        TextInputLayout inputContrasenaNva2 =  findViewById(R.id.input_layout_clave_nva2);

        int colorInt = getResources().getColor(R.color.colorBlanco);
        ColorStateList csl = ColorStateList.valueOf(colorInt);

        inputContrasenaNva1.setHintTextColor(csl);
        inputContrasenaNva2.setHintTextColor(csl);


        Bundle parametros = this.getIntent().getExtras();

        if (parametros!=null) mUsuario = parametros.getString("usuario");


        mClaveConfirm.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                closeKeyboard();
                mBtnGuardar.performClick();
                handled = true;
            }
            return handled;
        });



        mBtnGuardar.setOnClickListener(v -> {

            if (mClaveNva.getText().toString().trim().isEmpty() || (mClaveNva.length()<6)) {

                mClaveNva.setError("La contraseña debe ser mayor o igual a seis (6) caracteres");

                mClaveNva.requestFocus();

            }else if (mClaveConfirm.getText().toString().trim().isEmpty()) {

                mClaveConfirm.setError("Debe Repetir la contraseña nueva");

                mClaveConfirm.requestFocus();

            }else if (!mClaveNva.getText().toString().equals(mClaveConfirm.getText().toString())) {

                mClaveConfirm.setError("La contraseña no son iguales");

                mClaveConfirm.requestFocus();

            } else {

                guardarClave();

            }


        });

    }

    public void guardarClave(){

        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        forgotresetContainer.setVisibility(View.GONE);

        Log.e(TAG, " usuario: " + mUsuario);


        String password = mClaveNva.getText().toString();

        Call<ForgotResetResponse> call = PuertomarApiAdapter.getApiService().postForgotReset(mUsuario,password);

        call.enqueue(new ForgotResetCallback());

    }

    private class ForgotResetCallback implements retrofit2.Callback<ForgotResetResponse> {
        @Override
        public void onResponse(Call<ForgotResetResponse> call, Response<ForgotResetResponse> response) {

            progressDialog.dismiss();

            if(response.isSuccessful()){

                ForgotResetResponse forgotResetResponse = response.body();

                if(forgotResetResponse.isReset())
                {
                    Toast.makeText(context,"Clave restablecida con éxito", Toast.LENGTH_LONG).show();

                    setSession(response.body(),true);

                    Intent Principal = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(Principal);
                    finish();
                }

            }else{
                forgotresetContainer.setVisibility(View.VISIBLE);
                try {

                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(Call<ForgotResetResponse> call, Throwable t) {

            progressDialog.dismiss();
            Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
        }
    }


    public void setSession(ForgotResetResponse forgotResetResponse, boolean login){


        String token = forgotResetResponse.getToken();

        ArrayList<Clientes> perfil = forgotResetResponse.getPerfil();


        //obtiene la referencia a la preferencia de la aplicacion
        SharedPreferences v_preferences = this.getSharedPreferences("session",MODE_PRIVATE);

        //obtiene el editor de las preferencias
        SharedPreferences.Editor v_editor = v_preferences.edit();


        //crea el atributo que indica que se inicio la session
        v_editor.putBoolean("logged-in",login);
        v_editor.putString("empresa", perfil.get(0).getTx_empresa());
        v_editor.putString("replegal", perfil.get(0).getTx_representante_legal());
        v_editor.putString("dni", perfil.get(0).getTx_documento());
        v_editor.putString("correo_emp", perfil.get(0).getTx_correo());
        v_editor.putString("telefono", perfil.get(0).getTx_tlf());
        v_editor.putString("cociudad", perfil.get(0).getCo_ciudad());
        v_editor.putString("ciudad", perfil.get(0).getTx_ciudad());
        v_editor.putString("co_pais", perfil.get(0).getCo_pais());
        v_editor.putString("pais", perfil.get(0).getTx_pais());
        v_editor.putString("direccion", perfil.get(0).getTx_direccion());


        v_editor.putString("usuario", perfil.get(0).getTx_usuario());
        v_editor.putString("correo_usuario", perfil.get(0).getTx_correo_usuario());
        v_editor.putString("clave", mClaveNva.getText().toString());
        v_editor.putString("token", token);
        v_editor.putString("imagen_perfil", perfil.get(0).getFoto());


        v_editor.putBoolean("in_activo", perfil.get(0).getIn_activo());
        v_editor.putString("codmoneda", perfil.get(0).getTx_codmoneda());
        v_editor.putString("moneda", perfil.get(0).getTx_moneda());
        v_editor.putString("simbmoneda", perfil.get(0).getTx_simbolo_moneda());
        v_editor.putString("formapago", perfil.get(0).getJsonFormaPago());
        v_editor.putBoolean("in_pedidos", perfil.get(0).getIn_pedidos());
        v_editor.putBoolean("in_empresa", perfil.get(0).getInEmpresa());
        v_editor.putString("tipodoc", perfil.get(0).getTx_documento_legal());
        v_editor.putFloat("deuda_total", perfil.get(0).getDeudaTotal());


        //confirma los cambios realizados
        //v_editor.commit();

        v_editor.apply();
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }
}
