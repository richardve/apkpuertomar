package com.goodmarcom.apk.Activity;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.goodmarcom.apk.Adapter.ImageSlideAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.SliderPortadaResponse;
import com.goodmarcom.apk.Enty.Imagen;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.SliderItem;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class SlideActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";
    Context context;
    SliderView sliderView;
    private ImageSlideAdapter adapter;
    private PuertomarApiServices imagenService;
    private String token;
    TextView mRegistrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide);


        context = SlideActivity.this;

        imagenService = PuertomarApiAdapter.getApiService();

        SharedPreferences v_preferences = getSharedPreferences("session",Context.MODE_PRIVATE);
        token = v_preferences.getString("token","" );

        mRegistrate =  findViewById(R.id.lg_registrate);

        sliderView = findViewById(R.id.imageSlider);

        adapter = new ImageSlideAdapter(this);
        adapter.setGrupo(false);
        sliderView.setSliderAdapter(adapter);


        sliderView.setIndicatorAnimation(IndicatorAnimations.THIN_WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();

        //addNewItem(R.drawable.slide01);
        //addNewItem(R.drawable.slide02);
        //addNewItem(R.drawable.slide03);


       //SharedPreferences v_preferences = this.getSharedPreferences("session",MODE_PRIVATE);


        //el usuario ya tiene la session iniciada
        if(v_preferences.getBoolean("logged-in",false)) {
            Intent v_intent = new Intent(this, MainActivity.class);

            this.startActivity(v_intent);

            this.finish(); //para terminar la actividad principal

        }


        sliderView.setOnIndicatorClickListener(position -> sliderView.setCurrentPagePosition(position));


        /*Agregar evento a boton registrar*/
        mRegistrate.setOnClickListener(v -> {
            //llamar al layout registrar

            mRegistrate.setEnabled(false);
            startActivity(new Intent(SlideActivity.this,RegistroUsuarioActivity.class));
            //finish();
        });

        Button btnSession = findViewById(R.id.sl_btn_session);

        btnSession.setOnClickListener(v -> {

            Intent v_intent = new Intent(this, LoginActivity.class);

            this.startActivity(v_intent);

            this.finish();

        });

        obtenerImagenes();

    }


    private List<Imagen> fetchResultsImagenes(Response<SliderPortadaResponse> response) {
        SliderPortadaResponse imagenResponse = response.body();
        return imagenResponse.getImagenes();
    }


    private void obtenerImagenes() {

        callImagenesApi().enqueue(new Callback<SliderPortadaResponse>() {
            @Override
            public void onResponse(Call<SliderPortadaResponse> call, Response<SliderPortadaResponse> response) {


                if (response.isSuccessful()) {

                    List<Imagen> imagenes = fetchResultsImagenes(response);

                    SliderItem sliderItem = new SliderItem();

                    List<SliderItem> sliderItemList = new ArrayList<>();
                    int i=0;
                    for(Imagen img:imagenes) {
                        /*sliderItem.setDescription("");
                        sliderItem.setImageUrl(img.getUrlImagen());*/
                        addNewItem(img.getUrlImagen());

                        //sliderItemList.add(sliderItem);
                        Log.e(TAG, " Error:" + img.getUrlImagen());
                    }
                   // adapter.renewItems(sliderItemList);


                } else {

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());

                        Log.e(TAG, jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<SliderPortadaResponse> call, Throwable t) {
                t.printStackTrace();
                //progressDialog.dismiss();
                Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private Call<SliderPortadaResponse> callImagenesApi() {

        return imagenService.getImgSliderPortada();
    }

   /* public void renewItems(View view) {
        List<SliderItem> sliderItemList = new ArrayList<>();
        //dummy data
        for (int i = 0; i < 5; i++) {
            SliderItem sliderItem = new SliderItem();
            sliderItem.setDescription("Slider Item " + i);
            if (i % 2 == 0) {
                sliderItem.setImageUrl(R.drawable.slide01);
            } else {
                sliderItem.setImageUrl(R.drawable.slide02);
            }
            sliderItemList.add(sliderItem);
        }
        adapter.renewItems(sliderItemList);
    }

    public void removeLastItem(View view) {
        if (adapter.getCount() - 1 >= 0)
            adapter.deleteItem(adapter.getCount() - 1);
    }*/

    public void addNewItem(String imagen) {
        SliderItem sliderItem = new SliderItem();
        sliderItem.setDescription("");
        sliderItem.setImageUrl(imagen);
        adapter.addItem(sliderItem);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mRegistrate.setEnabled(true);

    }
}
