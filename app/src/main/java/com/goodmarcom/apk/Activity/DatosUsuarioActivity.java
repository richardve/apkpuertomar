package com.goodmarcom.apk.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.PerfilResponse;
import com.goodmarcom.apk.BuildConfig;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.ImageUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DatosUsuarioActivity extends AppCompatActivity {


    private static final String TAG = "PUERTOMAR";

    private String token;
    private Context context;
    private ProgressDialog progressDialog;
    private TextInputEditText mNombre;
    private TextInputEditText mCorreo;
    private TextInputEditText mContrasena;

    Boolean modificar;
    private CircleImageView mAvatar;
    private CircleImageView userProfilePhoto;
    private ImageView mEditImagen;

    public static final int REQUEST_TAKE_PHOTO = 101;
    public static final int REQUEST_GALLERY_PHOTO = 102;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 103;

    private String currentPhotoPath;
    private String imageBase64 = "";
    private String imagen_perfil = "";
    private SharedPreferences v_preferences;
    private Uri photoURI;
    private Boolean inPedido;

    private View datosUsuarioContainer;

    static String[] permissions = new String[]{
            Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_usuario);


        context = DatosUsuarioActivity.this;

        mNombre = findViewById(R.id.datos_usuario_nombre);
        mCorreo = findViewById(R.id.datos_usuario_correo);
        mContrasena = findViewById(R.id.datos_usuario_contrasena);

        userProfilePhoto = findViewById(R.id.datos_usuario_avatar);

        mEditImagen = findViewById(R.id.datos_usuario_edit_foto);
        datosUsuarioContainer = findViewById(R.id.datos_usuarioContainer);


        /*para mantener el mismo tipo de letra en el campo contraseña*/
        mContrasena.setTypeface(Typeface.DEFAULT);
        mContrasena.setTransformationMethod(new PasswordTransformationMethod());

        v_preferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);

        token = v_preferences.getString("token", "");

        modificar = false;



        Toolbar toolbar_user = findViewById(R.id.toolbar_datos_usuario);

        setSupportActionBar(toolbar_user);


        if (getSupportActionBar() != null) {

            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
            Objects.requireNonNull(getSupportActionBar()).setTitle("Datos de Usuario");

        }


        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        FloatingActionButton mBtnGuardar = findViewById(R.id.datos_usuario_btn_guardar);

        mBtnGuardar.setBackgroundTintList(csl);
        mBtnGuardar.setColorFilter(Color.WHITE);



        mBtnGuardar.setOnClickListener(v -> {

            if (validarDatosUsuario()) {

                Log.e(TAG, " imageBase64:" + imageBase64);

                if (modificar) {
                    if (imageBase64.isEmpty() && inPedido ) {
                        Toast.makeText(context, "Debe seleccionar una imagen", Toast.LENGTH_LONG).show();
                    } else {
                        actualizar();
                    }
                }
            }
        });


        inPedido = v_preferences.getBoolean("in_pedidos", false);
        imagen_perfil = v_preferences.getString("imagen_perfil", "");
        mNombre.setText(v_preferences.getString("usuario", ""));
        mCorreo.setText(v_preferences.getString("correo_usuario", ""));
        mContrasena.setText(v_preferences.getString("clave", ""));

        if (imagen_perfil.isEmpty()) imagen_perfil = "default";


        setImageAvatar(imagen_perfil);
        mContrasena.setFocusable(false);

        if (inPedido) {
            mNombre.setFocusable(false);
            mCorreo.setFocusable(false);
            mNombre.setEnabled(false);
            mCorreo.setEnabled(false);
            //mEditImagen.setEnabled(false);
        }

        if(imagen_perfil.equals("default")) {
               imageBase64 = "";
        }else{
               imageBase64 = imagen_perfil;
        }


        modificar = true;


        //evento para editar el avatar
        mEditImagen.setOnClickListener(view -> selectImage());

        mContrasena.setOnClickListener(v -> {

            Intent Principal = new Intent(context.getApplicationContext(), CambioClaveActivity.class);

            startActivityForResult(Principal, 2);

        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                Log.e(TAG, "Ruta:" + currentPhotoPath);
                Log.e(TAG, "URI:" + photoURI);
                displayImagePreview(photoURI);
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri imageUri = data.getData();
                displayImagePreview(imageUri);
            }
        }
        if (data != null) {
            if ((requestCode == 2)) {
                String clave = data.getStringExtra("clave");
                mContrasena.setText(clave);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            // we have heard back from our request for camera and write external storage.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                invokeCamera();
            } else {
                Toast.makeText(context, R.string.cannotopencamera, Toast.LENGTH_LONG).show();
            }
        }
    }


    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_gallery),getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Cámara")) {
                onTakePhotoClicked();
            } else if (items[item].equals("Galería")) {
                onImageGalleryClicked();
            } else if (items[item].equals("Cancelar")) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    public void onTakePhotoClicked() {

            if(ContextCompat.checkSelfPermission(DatosUsuarioActivity.this,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                invokeCamera();
            } else {
                String[] permissionRequest = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

                ActivityCompat.requestPermissions(DatosUsuarioActivity.this,permissionRequest, CAMERA_PERMISSION_REQUEST_CODE);

                //ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);

            }

    }

    private void invokeCamera() {

        photoURI = FileProvider.getUriForFile(context,BuildConfig.APPLICATION_ID + ".provider", newFile());

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

        intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        startActivityForResult(intent, REQUEST_TAKE_PHOTO);

    }

    public void onImageGalleryClicked() {
        // invoke the image gallery using an implict intent.
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);

        // where do we want to find the data?
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        // finally, get a URI representation
        Uri data = Uri.parse(pictureDirectoryPath);

        // set the data and type.  Get all image types.
        photoPickerIntent.setDataAndType(data, "image/*");

        // we will invoke this activity, and get something back from it.
        startActivityForResult(photoPickerIntent, REQUEST_GALLERY_PHOTO);
    }

    public File getFilePath() {
        return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public File newFile() {
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTimeInMillis();
        String mFileName = String.valueOf(timeInMillis) + ".jpeg";
        File mFilePath = getFilePath();
        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            newFile.createNewFile();

            currentPhotoPath = newFile.getAbsolutePath();

            return newFile;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void displayImagePreview(Uri mFileUri) {
        //Picasso.with(context).load(mFileUri).placeholder(R.drawable.avatar_default).into(userProfilePhoto);

        Picasso.get().load(mFileUri).placeholder(R.drawable.avatar_default).into(userProfilePhoto);

        InputStream inputStream;

        // we are getting an input stream, based on the URI of the image.
        try {
            inputStream = getContentResolver().openInputStream(mFileUri);

            // get a bitmap from the stream.
            Bitmap image = BitmapFactory.decodeStream(inputStream);

            // show the image to the user
            //userProfilePhoto.setImageBitmap(image);

            imageBase64 = ImageUtils.encodeBase64(image);

            Log.e(TAG,"imageBase64:"+imageBase64);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(context, "No se puede abrir imagen", Toast.LENGTH_LONG).show();
        }
    }


    private void actualizar() {

        progressDialog = new ProgressDialog(context, R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        datosUsuarioContainer.setVisibility(View.GONE);

        Call<PerfilResponse> call = PuertomarApiAdapter.getApiService().actualizarPerfil(getJsonEncode(),
                "Bearer " + token);
        call.enqueue(new ActualizarCallback());

    }

    class ActualizarCallback implements Callback<PerfilResponse> {

        @Override
        public void onResponse(@NonNull Call<PerfilResponse> call, Response<PerfilResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                PerfilResponse actualizarResponse = response.body();

                assert actualizarResponse != null;
                if (actualizarResponse.isPerfil()) {

                    setSession();

                    Toast.makeText(context, actualizarResponse.getSuccess(), Toast.LENGTH_LONG).show();

                    finish();
                }

            } else {
                datosUsuarioContainer.setVisibility(View.VISIBLE);
                try {

                    assert response.errorBody() != null;
                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();

                    Log.e(TAG, " Error Update: " + jObjError.toString());

                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(@NonNull Call<PerfilResponse> call, Throwable t) {

            progressDialog.dismiss();

            Log.e(TAG, " Error:: " + t.getLocalizedMessage());

            Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        }
    }


    public void setSession() {

        //obtiene la referencia a la preferencia de la aplicacion
        SharedPreferences v_preferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);


        //obtiene el editor de las preferencias
        SharedPreferences.Editor v_editor = v_preferences.edit();

        v_editor.putBoolean("logged-in", true);
        v_editor.putString("usuario", mNombre.getText().toString());
        v_editor.putString("correo_usuario", mCorreo.getText().toString());
        v_editor.putString("imagen_perfil", imageBase64);
        v_editor.putBoolean("update_fcm", true);

        //confirma los cambios realizados
        v_editor.commit();


    }


    public RequestBody getJsonEncode() {


        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("usuario", mNombre.getText().toString());
        jsonParams.put("correo", mCorreo.getText().toString());
        jsonParams.put("foto", imageBase64);


        Log.e(TAG, "Params:"+jsonParams.toString());

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());

        return body;
    }


    private void setImageAvatar (String imgBase64){
        try {
            Resources res = getResources();
            Bitmap src;
            if (imgBase64.equals("default")) {
                src = BitmapFactory.decodeResource(res, R.drawable.avatar_default);
            } else {
                byte[] decodedString = Base64.decode(imgBase64, Base64.DEFAULT);
                src = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            userProfilePhoto.setImageBitmap(src);
        } catch (Exception ignored) {
        }
    }


    private Boolean validarDatosUsuario () {

        Boolean valido = false;


        if (mNombre.getText().toString().trim().isEmpty()) {

            mNombre.setError("Debe Introducir un nombre");
            mNombre.requestFocus();

        } else if (mCorreo.getText().toString().trim().isEmpty() || !validarEmail(mCorreo.getText().toString())) {

            mCorreo.setError("Debe Introducir un correo válido");
            mCorreo.requestFocus();

        } else if (mContrasena.getText().toString().trim().isEmpty() || (mContrasena.length() < 6)) {

            mContrasena.setError("La contraseña debe ser mayor o igual a seis (6) caracteres");
            mContrasena.requestFocus();

        } else {
            valido = true;
        }

        return valido;

    }

    private boolean validarEmail (String email){

        Pattern pattern = Patterns.EMAIL_ADDRESS;

        return pattern.matcher(email).matches();
    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }


}
