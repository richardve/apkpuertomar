package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goodmarcom.apk.Adapter.CiudadAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.CiudadesResponse;
import com.goodmarcom.apk.Enty.Ciudad;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListCiudadesActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices apiServices;

    private RecyclerView recycler;

    private CiudadAdapter adapter;

    private ArrayList<Ciudad> mCiudad = new ArrayList<>();

    private String token;

    private ProgressDialog progressDialog;

    private Context context;

    private String search="";

    private String mCopais;

    private String mIdCiudad="";

    private String mTxciudad;

    private Integer mPosition=0;

    SharedPreferences v_preferences;

    private Timer timer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_ciudades);


        context = ListCiudadesActivity.this;

        Toolbar toolbar =  findViewById(R.id.toolbar_list_ciudad);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Seleccione una Ciudad");

        }

        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        FloatingActionButton mBtnConfirmar =  findViewById(R.id.list_ciudad_btn_confirmar);

        mBtnConfirmar.setBackgroundTintList(csl);
        mBtnConfirmar.setColorFilter(Color.WHITE);


        mBtnConfirmar.setOnClickListener(v -> {

            if (!adapter.isEmpty())
            {

                Ciudad ciudad = adapter.getItem(mPosition);

                mIdCiudad = String.valueOf(ciudad.getCo_ciudad());

                mTxciudad  = ciudad.getTx_ciudad();

                Log.e(TAG, "Co_Ciudad:"+mIdCiudad);

                Log.e(TAG, "Ciudad:"+mTxciudad);

            }else{
                mIdCiudad = "";
            }

            if(mIdCiudad.isEmpty()){
                Toast.makeText(context,"Debe seleccionar una Ciudad",Toast.LENGTH_SHORT).show();
            }else
            {
                Intent intent=new Intent();

                Bundle parametros = new Bundle();

                parametros.putString("tx_ciudad", mTxciudad);
                parametros.putString("mIdCiudad",mIdCiudad);

                intent.putExtras(parametros);
                setResult(2,intent);
                finish();
            }

         });


        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        Bundle parametros = this.getIntent().getExtras();

        if (parametros!=null) {
            mCopais =  parametros.getString("co_pais");
        }else {
            mCopais = v_preferences.getString("co_pais","" );
        }

        Log.e(TAG,"CoPais:"+mCopais);


        recycler =  findViewById(R.id.recycler_view_ciudad);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        adapter = new CiudadAdapter(mCiudad);

        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPosition = adapter.getmPosition();

              /*  Ciudad ciudad = adapter.getItem(mPosition);

                mIdCiudad = String.valueOf(ciudad.getCo_ciudad());

                mTxciudad  = ciudad.getTx_ciudad();

                Log.e(TAG, "Co_Ciudad:"+mIdCiudad);*/

            }
        });

        recycler.setAdapter(adapter);

        apiServices = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        obtenerDatos();

    }


    private void obtenerDatos() {

        if(!adapter.isEmpty()) adapter.clear();

        callServiceApi().enqueue(new Callback<CiudadesResponse>() {
            @Override
            public void onResponse(Call<CiudadesResponse> call, Response<CiudadesResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<Ciudad> ciudades = fetchResults(response);

                    mCiudad.addAll(ciudades);
                    adapter.notifyDataSetChanged();

                   /* mIdCiudad = String.valueOf(mCiudad.get(0).getCo_ciudad());

                    mTxciudad  = mCiudad.get(0).getTx_ciudad();*/


                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<CiudadesResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });

    }


    private Call<CiudadesResponse> callServiceApi() {

        return apiServices.getCiudades(mCopais,search/*,"Bearer " + token*/);
    }


    private List<Ciudad> fetchResults(Response<CiudadesResponse> response) {
        CiudadesResponse ciudadesResponse = response.body();
        return ciudadesResponse.getCiudades();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.actions_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        SearchView sv = (SearchView) item.getActionView();
        if (sv != null){
            sv.setSubmitButtonEnabled(false);
            sv.setOnQueryTextListener(this);
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {

        search = s;

        adapter.getFilter().filter(s);

        Log.e(TAG, "search:"+s);

        return false;
    }
}
