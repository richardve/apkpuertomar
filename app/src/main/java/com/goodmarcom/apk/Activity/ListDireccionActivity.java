package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
//import android.support.design.widget.FloatingActionButton;
//import android.support.v7.app.AppCompatActivity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
/*import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;*/
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goodmarcom.apk.Adapter.DireccionesPagoAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.DireccionesResponse;
import com.goodmarcom.apk.Enty.Direcciones;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListDireccionActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices apiServices;


    private RecyclerView recycler;

    private DireccionesPagoAdapter adapter;

    private ArrayList<Direcciones> mDirecciones = new ArrayList<>();

    private String token;

    private ProgressDialog progressDialog;

    private Context context;

    private Integer mIdDireccion=-1;

    private String mTxdireccion;

    private Integer mPosition;

    SharedPreferences v_preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_direccion);

        context = ListDireccionActivity.this;

        Toolbar toolbar_pp = findViewById(R.id.toolbar_list_pp);

        setSupportActionBar(toolbar_pp);


        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Seleccione una Dirección");

        }


        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        FloatingActionButton mBtnConfirmar =  findViewById(R.id.list_direccion_btn_confirmar);

        mBtnConfirmar.setBackgroundTintList(csl);
        mBtnConfirmar.setColorFilter(Color.WHITE);

        mBtnConfirmar.setOnClickListener(v -> {

            if(mIdDireccion.equals(-1)){
                Toast.makeText(context,"Debe seleccionar una dirección",Toast.LENGTH_SHORT).show();
            }else
            {
                Intent intent=new Intent();

                Bundle parametros = new Bundle();

                parametros.putString("tx_direccion", mTxdireccion);
                parametros.putInt("mIdDireccion",mIdDireccion);

                intent.putExtras(parametros);
                setResult(4,intent);
                finish();
            }

         });

        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        recycler =  findViewById(R.id.recycler_view_pago);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        adapter = new DireccionesPagoAdapter(mDirecciones);


        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPosition = adapter.getmPosition();

                Direcciones direcciones = adapter.getItem(mPosition);

                mIdDireccion = direcciones.getCo_direccion();

                mTxdireccion  = direcciones.getTx_direccion();

                Log.e(TAG, "Co_Dirección:"+mIdDireccion);

            }
        });


        recycler.setAdapter(adapter);

        apiServices = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        obtenerDatos();

    }


    private void obtenerDatos() {

        callServiceApi().enqueue(new Callback<DireccionesResponse>() {
            @Override
            public void onResponse(Call<DireccionesResponse> call, Response<DireccionesResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<Direcciones> direcciones = fetchResults(response);

                    mDirecciones.addAll(direcciones);
                    adapter.notifyDataSetChanged();

                    mIdDireccion = mDirecciones.get(0).getCo_direccion();
                    mTxdireccion  = mDirecciones.get(0).getTx_direccion();


                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<DireccionesResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Call<DireccionesResponse> callServiceApi() {

        return apiServices.getDirecciones("Bearer " + token);
    }

    private List<Direcciones> fetchResults(Response<DireccionesResponse> response) {
        DireccionesResponse direccionesResponse = response.body();
        return direccionesResponse.getClienteDireccion();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.actions_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
           /* case R.id.registro:

                if(mIdDireccion.equals(0)){
                    Toast.makeText(context,"Debe seleccionar una dirección",Toast.LENGTH_SHORT).show();
                }else
                {
                    Intent intent=new Intent();

                    Bundle parametros = new Bundle();

                    parametros.putString("tx_direccion", mTxdireccion);
                    parametros.putInt("mIdDireccion",mIdDireccion);

                    intent.putExtras(parametros);
                    setResult(4,intent);
                    finish();
                }
                return true;*/

            default:
                return super.onOptionsItemSelected(item);

        }
    }


}
