package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goodmarcom.apk.Adapter.PaisAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.PaisesResponse;
import com.goodmarcom.apk.Enty.Pais;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPaisesActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices apiServices;

    private RecyclerView recycler;

    private PaisAdapter adapter;

    private ArrayList<Pais> mPais = new ArrayList<>();

    private ProgressDialog progressDialog;

    private Context context;

    private String search="";

    private String mIdPais="";

    private String mTxpais;

    private Integer mPosition=0;


    private String codDocumento= "";

    private String mTipodoc="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_paises);


        context = ListPaisesActivity.this;

        Toolbar toolbar =  findViewById(R.id.toolbar_list_pais);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Seleccione un Pais");

        }

        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        FloatingActionButton mBtnConfirmar =  findViewById(R.id.list_paises_btn_confirmar);

        mBtnConfirmar.setBackgroundTintList(csl);
        mBtnConfirmar.setColorFilter(Color.WHITE);

        mBtnConfirmar.setOnClickListener(v -> {


            if (!adapter.isEmpty())
            {

                Pais pais = adapter.getItem(mPosition);

                mIdPais = String.valueOf(pais.getCo_pais());

                mTxpais  = pais.getTx_pais();

                //codDocumento = pais.getCo_documento();

                mTipodoc = pais.getJsonTipoDocumento();

                Log.e(TAG, "Pais:"+mTxpais);

            }else{
                mIdPais = "";
            }

           if(mIdPais.isEmpty()){
                Toast.makeText(context,"Debe seleccionar un País",Toast.LENGTH_SHORT).show();
            }else
            {

                Intent intent=new Intent();

                Bundle parametros = new Bundle();

                parametros.putString("tx_pais", mTxpais);
                parametros.putString("mIdPais",mIdPais);
                //parametros.putString("codDocumento", codDocumento);
                parametros.putString("mTipodoc", mTipodoc);


                intent.putExtras(parametros);
                setResult(1,intent);
                finish();


            }

        });

        recycler =  findViewById(R.id.recycler_view_pais);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        adapter = new PaisAdapter(mPais);

        adapter.setClickListener(v -> {

            mPosition = adapter.getmPosition();

            Log.e(TAG, "Selección "+mPosition);

        });

        recycler.setAdapter(adapter);

        apiServices = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        obtenerDatos();

    }


    private void obtenerDatos() {

        if(!adapter.isEmpty()) adapter.clear();

        callServiceApi().enqueue(new Callback<PaisesResponse>() {
            @Override
            public void onResponse(Call<PaisesResponse> call, Response<PaisesResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<Pais> paises = fetchResults(response);

                    mPais.addAll(paises);
                    adapter.notifyDataSetChanged();


                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<PaisesResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Call<PaisesResponse> callServiceApi() {

        return apiServices.getPaises(search);
    }


    private List<Pais> fetchResults(Response<PaisesResponse> response) {
        PaisesResponse paisesResponse = response.body();
        return paisesResponse.getPaises();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.actions_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        SearchView sv = (SearchView) item.getActionView();
        if (sv != null){
            sv.setSubmitButtonEnabled(false);
            sv.setOnQueryTextListener(this);
        }

        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {

        search = s;
        adapter.getFilter().filter(s);

        Log.e(TAG, "search:"+s);

        return false;

    }
}
