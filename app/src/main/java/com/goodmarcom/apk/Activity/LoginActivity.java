package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.FcmResponse;
import com.goodmarcom.apk.Api.response.LoginResponse;
import com.goodmarcom.apk.Enty.Clientes;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private TextView mForgot;
    private TextInputEditText mUsuario;
    private TextInputEditText mContrasena;
    private Button  mBtnEntrar;
    private ProgressDialog progressDialog;
    private Context context;
    private View loginContainer;
    private TextView mRegistrate;



    private String token;

    private String password;

    private SharedPreferences v_preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        context = LoginActivity.this;

        /*Obtener los componentes de la vista*/
        mUsuario = findViewById(R.id.lg_usuario);
        mContrasena =  findViewById(R.id.lg_contrasena);
        mBtnEntrar =  findViewById(R.id.lg_btn_entrar);
        mForgot =  findViewById(R.id.lg_olvidoclave);
        loginContainer = findViewById(R.id.loginContainer);



        TextInputLayout inputUsuario =  findViewById(R.id.input_layout_usuario);
        TextInputLayout inputContrasena =  findViewById(R.id.input_layout_contrasena);

        int colorInt = getResources().getColor(R.color.colorBlanco);
        ColorStateList csl = ColorStateList.valueOf(colorInt);

        inputUsuario.setHintTextColor(csl);
        inputContrasena.setHintTextColor(csl);
                /*para mantener el mismo tipo de letra en el campo contraseña*/
        mContrasena.setTypeface(Typeface.DEFAULT);
        mContrasena.setTransformationMethod(new PasswordTransformationMethod());

        /*Inicializar el dialogo*/
        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        //progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
       /* progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));*/

        v_preferences = this.getSharedPreferences("session",MODE_PRIVATE);


        token = v_preferences.getString("token","" );



        mContrasena.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                closeKeyboard();
                mBtnEntrar.performClick();
                handled = true;
            }
            return handled;
        });



        mRegistrate =  findViewById(R.id.login_registrate);

        /*Agregar evento a boton registrar*/
        mRegistrate.setOnClickListener(v -> {
            //llamar al layout registrar

            mRegistrate.setEnabled(false);
            startActivity(new Intent(LoginActivity.this,RegistroUsuarioActivity.class));
            //finish();
        });



        /*Agregar eventos al boton entrar*/
        mBtnEntrar.setOnClickListener(v -> {

            if(mUsuario.getText().toString().trim().isEmpty()){

                mUsuario.requestFocus();
                mUsuario.setError("Este campo es obligatorio");

            }else if (mContrasena.getText().toString().trim().isEmpty()) {

                mContrasena.requestFocus();
                mContrasena.setError("Este campo es obligatorio");

            }else {
                    login();
                }

            });


        /*Agregar evento a boton registrar*/
        mForgot.setOnClickListener(v -> {
                mForgot.setEnabled(false);
                Intent Principal = new Intent(context,ForgotActivity.class);
                startActivity(Principal);

                //finish();
        });
    }

    public void login(){

        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        loginContainer.setVisibility(View.GONE);


        String usuario;


        usuario = mUsuario.getText().toString();
        password = mContrasena.getText().toString();

            Log.e(TAG, " usuario: " + usuario);
            Log.e(TAG, " password: " + password);


        Call<LoginResponse> call = PuertomarApiAdapter.getApiService().postLogin(usuario, password);

        call.enqueue(new LoginCallback());

    }

    class LoginCallback implements Callback<LoginResponse> {

        @Override
        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

            progressDialog.dismiss();

                if(response.isSuccessful()){

                    LoginResponse loginResponse = response.body();

                    if(loginResponse.isLogin())
                    {

                        if(!v_preferences.getBoolean("update_fcm",false)) {
                            guardarTokenFcm(v_preferences.getString("token_fcm", ""));

                        }

                        setSession(response.body(),true);
                        Intent Principal = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(Principal);
                        finish();

                    }

                }else{
                    loginContainer.setVisibility(View.VISIBLE);

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }

                }

        }

        @Override
        public void onFailure(Call<LoginResponse> call, Throwable t) {
            progressDialog.dismiss();
            Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            
        }
    }



    public void guardarTokenFcm(String tokenFcm){

        Call<FcmResponse> call = PuertomarApiAdapter.getApiService().postFcm("Bearer "+token,tokenFcm);

        call.enqueue(new FcmCallback());

    }


    private class FcmCallback implements retrofit2.Callback<FcmResponse> {
        @Override
        public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

            if(response.isSuccessful()){

                FcmResponse fcmResponse = response.body();

                if(fcmResponse.isFcm())
                {
                   SharedPreferences.Editor v_editor = v_preferences.edit();

                    v_editor.putBoolean("update_fcm", true);

                    v_editor.commit();

                }

            }else{

                try {

                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    //Toast.makeText(getApplicationContext(), jObjError.getString("error"), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());

                    //Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }


        }

        @Override
        public void onFailure(Call<FcmResponse> call, Throwable t) {
            //Toast.makeText(getApplicationContext(),"Error de conexión",Toast.LENGTH_SHORT).show();
        }
    }



    public void setSession(LoginResponse loginResponse, boolean login){


        String token = loginResponse.getToken();

        ArrayList<Clientes> perfil = loginResponse.getPerfil();


        //obtiene la referencia a la preferencia de la aplicacion
       // SharedPreferences v_preferences = this.getSharedPreferences("session",MODE_PRIVATE);

        //obtiene el editor de las preferencias
        SharedPreferences.Editor v_editor = v_preferences.edit();


        //crea el atributo que indica que se inicio la session
        v_editor.putBoolean("logged-in",login);
        v_editor.putString("empresa", perfil.get(0).getTx_empresa());
        v_editor.putString("replegal", perfil.get(0).getTx_representante_legal());
        v_editor.putString("dni", perfil.get(0).getTx_documento());
        v_editor.putString("correo_emp", perfil.get(0).getTx_correo());
        v_editor.putString("telefono", perfil.get(0).getTx_tlf());
        v_editor.putString("cociudad", perfil.get(0).getCo_ciudad());
        v_editor.putString("ciudad", perfil.get(0).getTx_ciudad());
        v_editor.putString("co_pais", perfil.get(0).getCo_pais());
        v_editor.putString("pais", perfil.get(0).getTx_pais());
        v_editor.putString("codIsoPais", perfil.get(0).getTx_codiso());
        v_editor.putString("direccion", perfil.get(0).getTx_direccion());


        v_editor.putString("usuario", perfil.get(0).getTx_usuario());
        v_editor.putString("correo_usuario", perfil.get(0).getTx_correo_usuario());
        v_editor.putString("clave", password);
        v_editor.putString("token", token);
        v_editor.putString("imagen_perfil", perfil.get(0).getFoto());


        v_editor.putBoolean("in_activo", perfil.get(0).getIn_activo());
        v_editor.putString("codmoneda", perfil.get(0).getTx_codmoneda());
        v_editor.putString("moneda", perfil.get(0).getTx_moneda());
        v_editor.putString("simbmoneda", perfil.get(0).getTx_simbolo_moneda());
        v_editor.putString("formapago", perfil.get(0).getJsonFormaPago());
        v_editor.putBoolean("in_pedidos", perfil.get(0).getIn_pedidos());
        v_editor.putBoolean("in_empresa", perfil.get(0).getInEmpresa());
        v_editor.putString("tipodoc", perfil.get(0).getTx_documento_legal());
        v_editor.putFloat("deuda_total", perfil.get(0).getDeudaTotal());


        Log.e(TAG, "Set Session: in_empresa" +  perfil.get(0).getInEmpresa());


        //confirma los cambios realizados
        //v_editor.commit();
        v_editor.apply();

    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

@Override
public void onDestroy()
{
    super.onDestroy();
}

    @Override
    protected void onResume() {
        super.onResume();
        mRegistrate.setEnabled(true);
        mForgot.setEnabled(true);

    }

}
