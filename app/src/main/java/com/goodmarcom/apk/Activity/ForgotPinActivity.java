package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.ForgotPinResponse;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Response;

public class ForgotPinActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";
    private ProgressDialog progressDialog;
    private Button mBtnEnviarPin;
    private Context context;
    private String mUsuario;
    private TextInputEditText mPin;
    private View forgotpinContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pin);


        context = ForgotPinActivity.this;

        mBtnEnviarPin = findViewById(R.id.fg_btn_enviar_pin);
        mPin = findViewById(R.id.fg_pin);
        forgotpinContainer = findViewById(R.id.forgotpinContainer);


        /*para mantener el mismo tipo de letra en el campo contraseña*/
        mPin.setTypeface(Typeface.DEFAULT);
        mPin.setTransformationMethod(new PasswordTransformationMethod());


        TextInputLayout inputPin =  findViewById(R.id.input_layout_pin);

        int colorInt = getResources().getColor(R.color.colorBlanco);
        ColorStateList csl = ColorStateList.valueOf(colorInt);

        inputPin.setHintTextColor(csl);

        /*Evento al toolbar y titulo*/
        Toolbar toolbar = findViewById(R.id.toolbar_forgot_resetpin);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Contraseña Temporal");

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        /*Inicializar el dialogo*/
        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);


        Bundle parametros = this.getIntent().getExtras();

        if (parametros!=null) mUsuario = parametros.getString("usuario");

        mBtnEnviarPin.setOnClickListener(this::onClick);


        mPin.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                closeKeyboard();
                mBtnEnviarPin.performClick();
                handled = true;
            }
            return handled;
        });



    }


    public void enviarPin(){

        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        forgotpinContainer.setVisibility(View.GONE);

        Log.e(TAG, " usuario: " + mUsuario);


        String pin = Objects.requireNonNull(mPin.getText()).toString();

        Call<ForgotPinResponse> call = PuertomarApiAdapter.getApiService().postForgotPin(mUsuario,pin);

        call.enqueue(new ForgotPinCallback());

    }

    private void onClick(View v) {
        if (Objects.requireNonNull(mPin.getText()).toString().trim().isEmpty()) {

            mPin.requestFocus();
            mPin.setError("Este campo es obligatorio");

        } else {
            enviarPin();
        }
    }


    private class ForgotPinCallback implements retrofit2.Callback<ForgotPinResponse> {
        @Override
        public void onResponse(Call<ForgotPinResponse> call, Response<ForgotPinResponse> response) {

            progressDialog.dismiss();

            if(response.isSuccessful()){

                ForgotPinResponse forgotPinResponse = response.body();

                if(forgotPinResponse.isPin())
                {
                    Toast.makeText(context,forgotPinResponse.getSuccess(), Toast.LENGTH_LONG).show();

                    Intent Principal = new Intent(context,ForgotResetActivity.class);

                    Bundle parametros = new Bundle();

                    parametros.putString("usuario", mUsuario);

                    Principal.putExtras(parametros);

                    startActivity(Principal);
                }

            }else{
                forgotpinContainer.setVisibility(View.VISIBLE);
                try {

                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Log.e(TAG, " Error:: " + e.getMessage());

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(Call<ForgotPinResponse> call, Throwable t) {

            progressDialog.dismiss();
            Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();

        }
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }
}
