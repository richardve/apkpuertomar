package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goodmarcom.apk.Adapter.TipoPagoAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.TipoPagoResponse;
import com.goodmarcom.apk.Enty.TipoPago;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListTipoPagoActivity extends AppCompatActivity {


    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices apiServices;


    private RecyclerView recycler;

    private TipoPagoAdapter adapter;

    private ArrayList<TipoPago> mTipoPago = new ArrayList<>();

    private String token;

    private ProgressDialog progressDialog;

    private Context context;

    private String mIdFormaPago;

    private String mIdTipoPago="";

    private String mTxTipopago;

    private Integer mPosition;

    SharedPreferences v_preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tipo_pago);


        context = ListTipoPagoActivity.this;

        Toolbar toolbar =  findViewById(R.id.toolbar_list_tpago);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Seleccione un Tipo de Pago");

        }

        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        FloatingActionButton mBtnConfirmar =  findViewById(R.id.list_tpago_btn_confirmar);

        mBtnConfirmar.setBackgroundTintList(csl);

        mBtnConfirmar.setColorFilter(Color.WHITE);

        mBtnConfirmar.setOnClickListener(v -> {

            if(mIdTipoPago.isEmpty()){
                Toast.makeText(context,"Debe seleccionar un Tipo de Pago",Toast.LENGTH_SHORT).show();
            }else
            {
                Intent intent=new Intent();

                Bundle parametros = new Bundle();

                parametros.putString("mIdTipoPago", mIdTipoPago);
                parametros.putString("tx_tipo_pago",mTxTipopago);

                intent.putExtras(parametros);
                setResult(3,intent);
                finish();
            }

        });

        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        Bundle parametros = this.getIntent().getExtras();

        if (parametros!=null) mIdFormaPago = parametros.getString("mIdFormaPago");

        Log.e(TAG, "mIdFormaPago:"+mIdFormaPago);

        recycler =  findViewById(R.id.recycler_view_tpago);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        adapter = new TipoPagoAdapter(mTipoPago);

        adapter.setClickListener(v -> {

            mPosition = adapter.getmPosition();

            TipoPago tipoPago = adapter.getItem(mPosition);

            mIdTipoPago = String.valueOf(tipoPago.getCo_tipo_pago());

            mTxTipopago  = tipoPago.getTx_tipo_pago();

            Log.e(TAG, "Co_FormaPago:"+mIdFormaPago);

        });


        recycler.setAdapter(adapter);

        apiServices = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        obtenerDatos();

    }

    private void obtenerDatos() {

        callServiceApi().enqueue(new Callback<TipoPagoResponse>() {
            @Override
            public void onResponse(Call<TipoPagoResponse> call, Response<TipoPagoResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<TipoPago> tipoPagos = fetchResults(response);

                    mTipoPago.addAll(tipoPagos);
                    adapter.notifyDataSetChanged();

                    mIdTipoPago = String.valueOf(mTipoPago.get(0).getCo_tipo_pago());

                    mTxTipopago  = mTipoPago.get(0).getTx_tipo_pago();

                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<TipoPagoResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });

    }


    private Call<TipoPagoResponse> callServiceApi() {
        return apiServices.getTipoPagos("Bearer "+token, mIdFormaPago);
    }


    private List<TipoPago> fetchResults(Response<TipoPagoResponse> response) {
        TipoPagoResponse tipoPagoResponse = response.body();
        return tipoPagoResponse.getTipoPagos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.actions_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
           /* case R.id.registro:

                if(mIdTipoPago.isEmpty()){
                    Toast.makeText(context,"Debe seleccionar un Tipo de Pago",Toast.LENGTH_SHORT).show();
                }else
                {
                    Intent intent=new Intent();

                    Bundle parametros = new Bundle();

                    parametros.putString("mIdTipoPago", mIdTipoPago);
                    parametros.putString("tx_tipo_pago",mTxTipopago);

                    intent.putExtras(parametros);
                    setResult(3,intent);
                    finish();
                }
                return true;*/

            default:
                return super.onOptionsItemSelected(item);

        }
    }


}
