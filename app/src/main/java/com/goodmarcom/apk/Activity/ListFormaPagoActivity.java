package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
/*import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;*/
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goodmarcom.apk.Adapter.FormaPagoAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.PerfilResponse;
import com.goodmarcom.apk.Enty.FormaPagoPerfil;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFormaPagoActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices apiServices;


    private RecyclerView recycler;

    private FormaPagoAdapter adapter;

    private ArrayList<FormaPagoPerfil> mFormaPago = new ArrayList<>();

    private String token;

    private ProgressDialog progressDialog;

    private Context context;

    private String mIdFormaPago="";

    private String mTxformapago;

    private boolean in_contado;

    private Integer mPosition;

    SharedPreferences v_preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_forma_pago);


        context = ListFormaPagoActivity.this;

        Toolbar toolbar =  findViewById(R.id.toolbar_list_fpago);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Seleccione una Forma de Pago");

        }

        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        FloatingActionButton mBtnConfirmar =  findViewById(R.id.list_fpago_btn_confirmar);

        mBtnConfirmar.setBackgroundTintList(csl);
        mBtnConfirmar.setColorFilter(Color.WHITE);


        mBtnConfirmar.setOnClickListener(v -> {

            if(mIdFormaPago.isEmpty()){
                Toast.makeText(context,"Debe seleccionar una Forma de Pago",Toast.LENGTH_SHORT).show();
            }else
            {
                Intent intent=new Intent();

                Bundle parametros = new Bundle();

                parametros.putString("tx_forma_pago", mTxformapago);
                parametros.putString("mIdFormaPago",mIdFormaPago);
                parametros.putBoolean("in_contado", in_contado);

                intent.putExtras(parametros);
                setResult(2,intent);
                finish();
            }

         });

        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        recycler =  findViewById(R.id.recycler_view_fpago);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);


        recycler.setLayoutManager(lManager);

        adapter = new FormaPagoAdapter(mFormaPago);

        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPosition = adapter.getmPosition();

                FormaPagoPerfil formaPago = adapter.getItem(mPosition);

                mIdFormaPago = formaPago.getCo_forma_pago();

                mTxformapago  = formaPago.getMetodo().getTx_forma_pago();

                in_contado = formaPago.getMetodo().getIn_contado();

                Log.e(TAG, "Co_FormaPago:"+mIdFormaPago);

            }
        });

        recycler.setAdapter(adapter);

        apiServices = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        obtenerDatos();


    }

    private void obtenerDatos() {

        callServiceApi().enqueue(new Callback<PerfilResponse>() {
            @Override
            public void onResponse(Call<PerfilResponse> call, Response<PerfilResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<FormaPagoPerfil> formaPagos = fetchResults(response);

                    mFormaPago.addAll(formaPagos);
                    adapter.notifyDataSetChanged();

                    mIdFormaPago = mFormaPago.get(0).getCo_forma_pago();

                    mTxformapago  = mFormaPago.get(0).getMetodo().getTx_forma_pago();

                    in_contado = mFormaPago.get(0).getMetodo().getIn_contado();

                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<PerfilResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Call<PerfilResponse> callServiceApi() {

        return apiServices.getPerfil("Bearer " + token);
    }


    private List<FormaPagoPerfil> fetchResults(Response<PerfilResponse> response) {
        PerfilResponse perfilResponse = response.body();
        return perfilResponse.getMyperfil().get(0).getFormapago();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.actions_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
          /*  case R.id.registro:

                if(mIdFormaPago.isEmpty()){
                    Toast.makeText(context,"Debe seleccionar una Forma de Pago",Toast.LENGTH_SHORT).show();
                }else
                {
                    Intent intent=new Intent();

                    Bundle parametros = new Bundle();

                    parametros.putString("tx_forma_pago", mTxformapago);
                    parametros.putString("mIdFormaPago",mIdFormaPago);
                    parametros.putBoolean("in_contado", in_contado);

                    intent.putExtras(parametros);
                    setResult(2,intent);
                    finish();
                }
                return true;*/

            default:
                return super.onOptionsItemSelected(item);

        }
    }


}
