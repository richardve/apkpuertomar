package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
//import android.support.design.widget.FloatingActionButton;
//import android.support.v7.app.AppCompatActivity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
/*import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;*/
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goodmarcom.apk.Adapter.CuentaPagoAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.CuentaEntidadResponse;
import com.goodmarcom.apk.Enty.CuentaEntidad;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListCuentaActivity extends AppCompatActivity {


    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices apiServices;


    private Context context;
    CuentaPagoAdapter adapter;
    private RecyclerView recycler;

    private String token;

    private ProgressDialog progressDialog;

    ArrayList<CuentaEntidad> mCuentaEntidad = new ArrayList<>();

    private Integer mPosition;

    private Integer mIdCuentaEntidad=-1;

    private String mTxCuenta;

    private String mTxEntidad;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cuenta);


        context = ListCuentaActivity.this;

        Toolbar toolbar_list_cuenta =  findViewById(R.id.toolbar_list_cuenta);

        setSupportActionBar(toolbar_list_cuenta);


        if (getSupportActionBar()!= null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Seleccione una Cuenta");

        }

        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);

        FloatingActionButton mBtnConfirmar =  findViewById(R.id.list_cuenta_btn_confirmar);

        mBtnConfirmar.setBackgroundTintList(csl);
        mBtnConfirmar.setColorFilter(Color.WHITE);

        mBtnConfirmar.setOnClickListener(v -> {
            if(mIdCuentaEntidad.equals(-1)){
                Toast.makeText(context,"Debe seleccionar una Cuenta",Toast.LENGTH_SHORT).show();
            }else
            {
                Intent intent=new Intent();

                Bundle parametros = new Bundle();

                parametros.putInt("mIdCuentaEntidad", mIdCuentaEntidad);
                parametros.putString("mTxCuenta",mTxCuenta);
                parametros.putString("mTxEntidad", mTxEntidad);

                intent.putExtras(parametros);
                setResult(2,intent);
                finish();
            }
        });


        SharedPreferences v_preferences = getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        recycler = findViewById(R.id.recycler_view_cuenta);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        recycler.setItemAnimator(new DefaultItemAnimator());

        adapter = new CuentaPagoAdapter(mCuentaEntidad);


        adapter.setClickListener(v -> {

            mPosition = adapter.getmPosition();

            CuentaEntidad cuentaEntidad = adapter.getItem(mPosition);

            mIdCuentaEntidad = cuentaEntidad.getCo_cuenta();

            mTxCuenta  = cuentaEntidad.getTx_nro_cuenta();

            mTxEntidad = cuentaEntidad.getTx_entidad();

            Log.e(TAG, "Co_CuentaEntidad:"+mIdCuentaEntidad);

            Log.e(TAG, "Tx Cuenta:"+mTxCuenta);

        });

        recycler.setAdapter(adapter);

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        apiServices = PuertomarApiAdapter.getApiService();

        obtenerDatos();

    }


    private void obtenerDatos() {

        //if(!adapter.isEmpty()) adapter.clear();

        callCuentaEntidadApi().enqueue(new Callback<CuentaEntidadResponse>() {
            @Override
            public void onResponse(Call<CuentaEntidadResponse> call, Response<CuentaEntidadResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<CuentaEntidad> cuentaEntidad = fetchResults(response);

                    mCuentaEntidad.addAll(cuentaEntidad);
                    adapter.notifyDataSetChanged();

                    mIdCuentaEntidad = mCuentaEntidad.get(0).getCo_cuenta();

                    mTxCuenta  = mCuentaEntidad.get(0).getTx_nro_cuenta();

                    mTxEntidad = mCuentaEntidad.get(0).getTx_entidad();

                    Log.e(TAG, "Tx Cuenta Result:"+mTxCuenta);

                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<CuentaEntidadResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Call<CuentaEntidadResponse> callCuentaEntidadApi() {

        return  apiServices.getCuentaEntidad("Bearer "+token);
    }

    private List<CuentaEntidad> fetchResults(Response<CuentaEntidadResponse> response) {
        CuentaEntidadResponse cuentaEntidadResponse = response.body();
        return cuentaEntidadResponse.getCuentaEntidad();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }



}
