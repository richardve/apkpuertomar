package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
/*import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;*/
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.goodmarcom.apk.Adapter.CuentaEntidadAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.CuentaEntidadResponse;
import com.goodmarcom.apk.Enty.CuentaEntidad;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoEmpresarialActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices cuentaEntidadService;


    private Context context;
    CuentaEntidadAdapter adapter;
    private RecyclerView recycler;

    private String token;

    private ProgressDialog progressDialog;

    ArrayList<CuentaEntidad> mCuentaEntidad = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_empresarial);

        context = InfoEmpresarialActivity.this;

        Toolbar toolbar_ie =  findViewById(R.id.toolbar_ie);

        setSupportActionBar(toolbar_ie);


        if (getSupportActionBar()!= null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Información Bancaria");

        }


        SharedPreferences v_preferences = getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        recycler = findViewById(R.id.rv_cuenta_entidad);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        recycler.setItemAnimator(new DefaultItemAnimator());

        adapter = new CuentaEntidadAdapter(mCuentaEntidad);

        recycler.setAdapter(adapter);


        cuentaEntidadService = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        obtenerDatos();

    }


    private void obtenerDatos() {

        //if(!adapter.isEmpty()) adapter.clear();

        callCuentaEntidadApi().enqueue(new Callback<CuentaEntidadResponse>() {
            @Override
            public void onResponse(Call<CuentaEntidadResponse> call, Response<CuentaEntidadResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<CuentaEntidad> cuentaEntidad = fetchResults(response);

                    mCuentaEntidad.addAll(cuentaEntidad);
                    adapter.notifyDataSetChanged();


                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<CuentaEntidadResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Call<CuentaEntidadResponse> callCuentaEntidadApi() {

        return  cuentaEntidadService.getCuentaEntidad("Bearer "+token);
    }

    private List<CuentaEntidad> fetchResults(Response<CuentaEntidadResponse> response) {
        CuentaEntidadResponse cuentaEntidadResponse = response.body();
        return cuentaEntidadResponse.getCuentaEntidad();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
