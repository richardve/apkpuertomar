package com.goodmarcom.apk.Activity;


import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.goodmarcom.apk.Adapter.TipoDocumentoAdapter;
import com.goodmarcom.apk.Enty.TipoDocumento;
import com.goodmarcom.apk.R;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListTipoDocumentoActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";

    private RecyclerView recycler;

    private TipoDocumentoAdapter adapter;

    private ArrayList<TipoDocumento> mTipoDocumento = new ArrayList<>();

    //private ProgressDialog progressDialog;

    private Context context;

    private String mIdTipoDocumento="";

    private String mTxTipoDocumento;

    private String mTipodoc;

    private Integer mPosition;

    private TipoDocumento[] td_class;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tipo_documento);


        context = ListTipoDocumentoActivity.this;

        Toolbar toolbar =  findViewById(R.id.toolbar_list_tdocumento);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Seleccione un Tipo de Documento");

        }

        int colorInt = getResources().getColor(R.color.background);
        ColorStateList csl = ColorStateList.valueOf(colorInt);


        FloatingActionButton mBtnConfirmar =  findViewById(R.id.list_tdocumento_btn_confirmar);

        mBtnConfirmar.setBackgroundTintList(csl);
        mBtnConfirmar.setColorFilter(Color.WHITE);

        mBtnConfirmar.setOnClickListener(v -> {

            if(mIdTipoDocumento.isEmpty()){
                Toast.makeText(context,"Debe seleccionar un Tipo de Pago",Toast.LENGTH_SHORT).show();
            }else
            {
                Intent intent=new Intent();

                Bundle parametros = new Bundle();

                parametros.putString("mIdTipoDocumento", mIdTipoDocumento);
                parametros.putString("tx_tipo_documento",mTxTipoDocumento);

                intent.putExtras(parametros);
                setResult(3,intent);
                finish();
            }

        });

       // SharedPreferences v_preferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);

        //String token = v_preferences.getString("token", "");

        Bundle parametros = this.getIntent().getExtras();

        if (parametros!=null)
        {
            mTipodoc = parametros.getString("mTipodoc");

            Gson gson = new Gson();

            td_class = gson.fromJson(mTipodoc, TipoDocumento[].class);


            for(int i=0; i<td_class.length;i++)
            {
                mTipoDocumento.add(td_class[i]);
            }

            mIdTipoDocumento = String.valueOf(td_class[0].getCo_documento());

            mTxTipoDocumento  = td_class[0].getTx_documento();

            Log.e(TAG, "mIdFormaPago:"+mTipodoc);
        }

        recycler =  findViewById(R.id.recycler_view_tdocumento);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        adapter = new TipoDocumentoAdapter(mTipoDocumento);

        adapter.setClickListener(v -> {

            mPosition = adapter.getmPosition();

            TipoDocumento tipoDocumento = adapter.getItem(mPosition);

            mIdTipoDocumento = String.valueOf(tipoDocumento.getCo_documento());

            mTxTipoDocumento  = tipoDocumento.getTx_documento();

            Log.e(TAG, "Co_FormaPago:"+mIdTipoDocumento);

        });

        recycler.setAdapter(adapter);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
