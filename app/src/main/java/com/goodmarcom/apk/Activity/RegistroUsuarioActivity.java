package com.goodmarcom.apk.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.RegisterResponse;
import com.goodmarcom.apk.Enty.Clientes;
import com.goodmarcom.apk.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroUsuarioActivity extends AppCompatActivity {

    private static final String TAG = "PUERTOMAR";


    private TextInputEditText mNombre;
    private TextInputEditText mCorreo;
    private TextInputEditText mContrasena;
    private TextInputEditText mRepContrasena;
    private ProgressDialog progressDialog;
    private Context context;
    private Clientes  mCliente;
    private SharedPreferences v_preferences;
    private String token_fcm;
    private View registrousuarioContainer;
    private Button btnRegistrar;

    // METODOS DEL ACTIVITY

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        context = RegistroUsuarioActivity.this;

        /*Obtener los componentes de la vista*/
        mNombre = findViewById(R.id.rg_nombre);
        mCorreo =  findViewById(R.id.rg_correo);
        mContrasena =  findViewById(R.id.rg_contrasena);
        mRepContrasena =  findViewById(R.id.rg_rep_contrasena);
        registrousuarioContainer = findViewById(R.id.registrousuarioContainer);

        /*para mantener el mismo tipo de letra en el campo contraseña*/
        mContrasena.setTypeface(Typeface.DEFAULT);
        mContrasena.setTransformationMethod(new PasswordTransformationMethod());
        mRepContrasena.setTypeface(Typeface.DEFAULT);
        mRepContrasena.setTransformationMethod(new PasswordTransformationMethod());

        v_preferences = getSharedPreferences("session", Context.MODE_PRIVATE);


        TextInputLayout inputNombre =  findViewById(R.id.rg_layout_nombre);
        TextInputLayout inputCorreo =  findViewById(R.id.rg_layout_correo);
        TextInputLayout inputContrasena =  findViewById(R.id.rg_layout_contrasena);
        TextInputLayout inputContrasenaRep =  findViewById(R.id.rg_layout_rep_contrasena);

        int colorInt = getResources().getColor(R.color.colorBlanco);
        ColorStateList csl = ColorStateList.valueOf(colorInt);

        inputNombre.setHintTextColor(csl);
        inputCorreo.setHintTextColor(csl);
        inputContrasena.setHintTextColor(csl);
        inputContrasenaRep.setHintTextColor(csl);


        btnRegistrar =  findViewById(R.id.lg_btn_registrar);

        Toolbar toolbar = findViewById(R.id.toolbar_user);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Registro de Usuario");

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        token_fcm="";

        btnRegistrar.setOnClickListener(v -> {
            if (validarDatosUsuario()) {
                mCliente = new Clientes();

                Log.e(TAG, "Usuario/Antes"+mNombre.getText().toString());

                mCliente.setTx_usuario(mNombre.getText().toString());
                mCliente.setTx_correo_usuario(mCorreo.getText().toString());
                mCliente.setTx_clave(mContrasena.getText().toString());

                if(!v_preferences.getBoolean("update_fcm",false)) {
                    token_fcm =  v_preferences.getString("token_fcm", "");
                }
                mCliente.setTx_token_fcm(token_fcm);

                registrar(mCliente);
            }
        });

        mRepContrasena.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                closeKeyboard();
                btnRegistrar.performClick();
                handled = true;
            }
            return handled;
        });


        mNombre.requestFocus();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.actions_registro_user, menu);

        return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }


    // METODOS DE LA APLICACIÓN


    private Boolean validarDatosUsuario() {

        Boolean valido = false;


        if (mNombre.getText().toString().trim().isEmpty()) {

            mNombre.setError("Debe Introducir un nombre");
            mNombre.requestFocus();

        } else if (mCorreo.getText().toString().trim().isEmpty() || !validarEmail(mCorreo.getText().toString())) {

            mCorreo.setError("Debe Introducir un correo válido");
            mCorreo.requestFocus();

        } else if (mContrasena.getText().toString().trim().isEmpty() || (mContrasena.length()<6)){

            mContrasena.setError("La contraseña debe ser mayor o igual a seis (6) caracteres");
            mContrasena.requestFocus();

        }
        else if(!mRepContrasena.getText().toString().trim().equals(mContrasena.getText().toString().trim())){
            mRepContrasena.setError("Las contraseñas no coinciden");
            mRepContrasena.requestFocus();
        }
        else {
            valido = true;
        }

        return valido;

    }


    public void setDatosSession(RegisterResponse registerResponse, boolean registrado){

        String token = registerResponse.getToken();

        Log.e(TAG, "Token:"+token);

        //ArrayList<Clientes> perfil = registerResponse.getPerfil();

        Clientes perfil = mCliente;


        //obtiene la referencia a la preferencia de la aplicacion
        SharedPreferences v_preferences = this.getSharedPreferences("session",MODE_PRIVATE);


        //obtiene el editor de las preferencias
        SharedPreferences.Editor v_editor = v_preferences.edit();


        //crea el atributo que indica que se inicio la session
        v_editor.putBoolean("registrado-in",registrado);
        v_editor.putBoolean("logged-in",true);
        v_editor.putString("token", token);

        Log.e(TAG, "Usuario/Despues"+perfil.getTx_usuario());

        v_editor.putString("usuario", perfil.getTx_usuario());
        v_editor.putString("correo_usuario", perfil.getTx_correo_usuario());
        v_editor.putString("clave", perfil.getTx_clave());
        v_editor.putBoolean("in_empresa",perfil.getInEmpresa());
        //v_editor.putString("imagen_perfil",imageBase64);

        v_editor.putBoolean("in_activo", true);

        //confirma los cambios realizados
        //v_editor.commit();

        v_editor.apply();


    }

    private boolean validarEmail(String email) {

        Pattern pattern = Patterns.EMAIL_ADDRESS;

        return pattern.matcher(email).matches();
    }


    // METODOS DE ACCESO A DATOS - LLAMADAS ASINCRONAS


    private void registrar(Clientes perfil) {

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        registrousuarioContainer.setVisibility(View.GONE);



        Call<RegisterResponse> call = PuertomarApiAdapter.getApiService().postRegister(perfil.getTx_usuario(),
                perfil.getTx_correo_usuario(),
                perfil.getTx_clave());

        call.enqueue(new RegisterCallback());
    }


    class RegisterCallback implements Callback<RegisterResponse> {

        @Override
        public void onResponse(@NonNull Call<RegisterResponse> call, Response<RegisterResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                RegisterResponse registerResponse = response.body();

                assert registerResponse != null;
                if(registerResponse.isRegister()){

                    setDatosSession(response.body(),true);


                    Toast.makeText(context,registerResponse.getSuccess(), Toast.LENGTH_LONG).show();

                    Intent Principal = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(Principal);
                    finish();

                }

            } else {
                registrousuarioContainer.setVisibility(View.VISIBLE);
                try {

                    assert response.errorBody() != null;
                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    JSONArray arrJson = jObjError.getJSONArray("error");

                    Toast.makeText(context, arrJson.get(0).toString(), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    Log.e(TAG, " Error:: " + e.getMessage());

                }

            }

        }

        @Override
        public void onFailure(@NonNull Call<RegisterResponse> call, Throwable t) {
            progressDialog.dismiss();
            Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
        }
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }
}

