package com.goodmarcom.apk.Adapter;

import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.CuentaEntidad;
import com.goodmarcom.apk.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class CuentaEntidadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private List<CuentaEntidad> items;

    static Context context;
    private CuentaEntidad mCuentaEntidad;


    public CuentaEntidadAdapter(List<CuentaEntidad> items) {  this.items = items; }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_cuenta_entidad, viewGroup, false);



        return new CuentaEntidadAdapter.CuentaEntidadViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        final CuentaEntidadViewHolder holder = (CuentaEntidadViewHolder) viewHolder;


        holder.entidad.setText(items.get(i).getTx_entidad());
        holder.nro_cuenta.setText(items.get(i).getTx_nro_cuenta());
        holder.propietario.setText(items.get(i).getTx_propietario());
        holder.documento.setText(items.get(i).getTx_documento());
        holder.tipo_cuenta.setText(items.get(i).getTx_tipo_cuenta());

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }


    protected class CuentaEntidadViewHolder extends RecyclerView.ViewHolder{

        TextView entidad;
        TextView nro_cuenta;
        TextView propietario;
        TextView documento;
        TextView tipo_cuenta;


        public CuentaEntidadViewHolder(@NonNull View itemView) {
            super(itemView);

            entidad = itemView.findViewById(R.id.ce_entidad);
            nro_cuenta = itemView.findViewById(R.id.ce_cuenta);
            propietario = itemView.findViewById(R.id.ce_propietario);
            documento = itemView.findViewById(R.id.ce_documento);
            tipo_cuenta = itemView.findViewById(R.id.ce_tipo_cuenta);


        }
    }


}
