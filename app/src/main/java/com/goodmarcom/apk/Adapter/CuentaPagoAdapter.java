package com.goodmarcom.apk.Adapter;

import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.CuentaEntidad;
import com.goodmarcom.apk.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CuentaPagoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = "PUERTOMAR";

    private List<CuentaEntidad> items;

    static Context context;


    private int mPosition;

    private View.OnClickListener mClickListener;

    private int lastSelectedPosition = 0;

    public CuentaPagoAdapter(List<CuentaEntidad> items) {  this.items = items; }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_cuenta_pago, viewGroup, false);


        return new CuentaPagoAdapter.CuentaPagoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        final CuentaPagoViewHolder holder = (CuentaPagoViewHolder) viewHolder;


        holder.entidad.setText(items.get(i).getTx_entidad());
        holder.nro_cuenta.setText(items.get(i).getTx_nro_cuenta());
        holder.tipo_cuenta.setText(items.get(i).getTx_tipo_cuenta());

        holder.opcion.setChecked(lastSelectedPosition == i);

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public CuentaEntidad getItem(int position) {
        return items.get(position);
    }

    public int getmPosition() {
        return mPosition;
    }


    protected class CuentaPagoViewHolder extends RecyclerView.ViewHolder{

        TextView entidad;
        TextView nro_cuenta;
        TextView tipo_cuenta;
        RadioButton opcion;


        public CuentaPagoViewHolder(@NonNull View itemView) {
            super(itemView);

            entidad = itemView.findViewById(R.id.item_entidad);
            nro_cuenta = itemView.findViewById(R.id.item_cuenta);
            tipo_cuenta = itemView.findViewById(R.id.item_tipo_cuenta);

            opcion = itemView.findViewById(R.id.radio_button_cpago);

            opcion.setOnClickListener(v -> {
                mPosition = getAdapterPosition();;
                mClickListener.onClick(v);
                lastSelectedPosition = mPosition;
                notifyDataSetChanged();
            });
        }
    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }
}



