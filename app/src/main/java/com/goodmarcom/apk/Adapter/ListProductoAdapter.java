package com.goodmarcom.apk.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.goodmarcom.apk.Enty.Combo;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.Globals;
import com.squareup.picasso.Picasso;


import java.text.DecimalFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



public class ListProductoAdapter extends RecyclerView.Adapter<ListProductoAdapter.ListProdctoViewHolder>{
    private static final String TAG = "PUERTOMAR";

    private List<Combo> items;
    private Context context;
    private SharedPreferences v_preferences;

    public ListProductoAdapter(List<Combo> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ListProdctoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_productos_combo,parent,false);
        context = parent.getContext();

        //obtiene la referencia a la preferencia de la aplicacion
        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        return new ListProdctoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ListProdctoViewHolder holder, int i) {

        final Globals globalVariable = new Globals();

        String UrlImagen = globalVariable.URL_IMAGEN();

        DecimalFormat numberFormat = new DecimalFormat(" #,##0");

        Picasso.get().load(UrlImagen+items.get(i).getCo_producto()+items.get(i).getTx_ext_imagen())
                .placeholder(R.drawable.ic_progress_animation)
                .into(holder.imagen);

        Log.e(TAG,UrlImagen+items.get(i).getCo_producto());

        holder.nombre.setText(items.get(i).getTx_producto());

       // String moneda = v_preferences.getString("simbmoneda", "$");

        Log.e(TAG,"Cantidad BD;"+items.get(i).getNu_cantidad());


        String cantidad = items.get(i).getNu_cantidad();

        int punto = cantidad.indexOf(".");

        String entero = cantidad.substring(0, punto);

        //cantidad = entero;

        if(!items.get(i).getNu_unidades().equals("0")) {
            cantidad = entero +"/"+items.get(i).getNu_unidades() + " Unid.";
       }else {
            cantidad = entero;
        }

        Log.e(TAG,"Cantidad APP;"+cantidad);

       holder.cantidad.setText(cantidad);


    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public static class ListProdctoViewHolder extends RecyclerView.ViewHolder{

        public TextView nombre;
        public TextView cantidad;
        public TextView precio;
        public TextView subtotal;
        public ImageView imagen;


        public ListProdctoViewHolder(View itemView) {
            super(itemView);

            nombre =  itemView.findViewById(R.id.prod_nombre);
            cantidad = itemView.findViewById(R.id.prod_cantidad);
            // precio = itemView.findViewById(R.id.prod_precio);
            //subtotal =   itemView.findViewById(R.id.prod_subtotal);
            imagen = itemView.findViewById(R.id.prod_img);


        }
    }
}
