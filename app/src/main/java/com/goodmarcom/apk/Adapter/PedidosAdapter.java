package com.goodmarcom.apk.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.goodmarcom.apk.Enty.Pedidos;
import com.goodmarcom.apk.Fragments.DetallePedidoFragment;
import com.goodmarcom.apk.Fragments.DetallePedidosOpcionFragment;
import com.goodmarcom.apk.R;


import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;


public class PedidosAdapter extends RecyclerView.Adapter<PedidosAdapter.PedidosViewHolder>{

    private List<Pedidos> items;
    private Context context;
    private SharedPreferences v_preferences;

    public PedidosAdapter(List<Pedidos> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public PedidosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_resumen_pedidos,parent,false);
        context = parent.getContext();

        //obtiene la referencia a la preferencia de la aplicacion
        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        return new PedidosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PedidosViewHolder holder, int i) {

        DecimalFormat numberFormat = new DecimalFormat(" #,##0.00");

        holder.estatus.setText(items.get(i).getEstatus());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatOut = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = format.parse(items.get(i).getFecha());
            holder.fecha.setText(formatOut.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String moneda = v_preferences.getString("simbmoneda", "$");

        Double total_pedido,
                recargo=0.00,
                subtotal=items.get(i).getTotal(),
                mNuPorc = Double.valueOf(items.get(i).getPago_pedido().getNu_porc());


        recargo = mNuPorc*subtotal;

        total_pedido = subtotal + recargo;

        holder.mTxTotal.setText(moneda+numberFormat.format(total_pedido));

        //holder.total.setText(numberFormat.format(items.get(i).getTotal()));
        holder.codigo.setText("Pedido N°: "+items.get(i).getCodigo());
        holder.cantidad.setText(String.format("%.0f",items.get(i).getCantidad()));
       // holder.moneda.setText(v_preferences.getString("simbmoneda","$"));
        holder.metodo.setText(items.get(i).getTx_forma_pago());

        if(!items.get(i).getCo_estatus().equals("4") && items.get(i).getDeuda().getNu_saldo()>0 /*&& items.get(i).getPago_pedido().getForma_pago().isIn_credito()*/){
            holder.borderDeuda.setVisibility(View.VISIBLE);
            holder.deuda.setVisibility(View.VISIBLE);
            holder.deuda_total.setText(numberFormat.format(items.get(i).getDeuda().getNu_saldo()));
            holder.deuda_moneda.setText(v_preferences.getString("simbmoneda","$"));

            try {
                Date date = format.parse(items.get(i).getDeuda().getFe_vencimiento());
                holder.deuda_fecha.setText(formatOut.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //final String codigo = items.get(i).getCodigo();
        final int position = i;

        //programar el evento para los botones
        holder.btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Bundle parametros = new Bundle();

                parametros.putString("opcion", "");
                parametros.putString("fragment_origen", "historial");

                DetallePedidoFragment frg = new DetallePedidoFragment(items.get(position));

                frg.setArguments(parametros);

                ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_content, frg,"detpedidos")
                        .addToBackStack("historial")
                        .commit();

            }
        });

        //verificar si el pedido esta en estatus 2 planificada
        if(items.get(position).getCo_estatus().equals("2")){
            holder.btnAnular.setVisibility(View.VISIBLE);
        }
        else{
            holder.btnAnular.setVisibility(View.GONE);
        }

        holder.btnAnular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //verificar si el usuario esta activo
                boolean activo = v_preferences.getBoolean("in_activo",false);

                if(activo) {

                    Bundle parametros = new Bundle();

                    parametros.putString("opcion", "Anular");
                    parametros.putString("fragment_origen", "historial");

                    DetallePedidosOpcionFragment frag = new DetallePedidosOpcionFragment(items.get(position));

                    frag.setArguments(parametros);

                    ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_content, frag , "detpedidosopcion")
                            .addToBackStack("historial")
                            .commit();
                }
                else{
                    Toast.makeText(context,"Tu cuenta se encuentra desactivada" ,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public static class PedidosViewHolder extends RecyclerView.ViewHolder{

        public TextView estatus;
        public TextView codigo;
        public TextView fecha;
        public TextView cantidad;
        public TextView metodo;
        public View deuda;
        public TextView deuda_total;
        public TextView deuda_moneda;
        public TextView deuda_fecha;
        public View borderDeuda;

        public ImageView btnVer;
        public ImageView btnAnular;

        private TextView mTxTotal;


        public PedidosViewHolder(View itemView) {
            super(itemView);

            estatus =  itemView.findViewById(R.id.pd_estatus);
            fecha =    itemView.findViewById(R.id.pd_fecha);
            codigo =   itemView.findViewById(R.id.pd_codigo);
            cantidad = itemView.findViewById(R.id.pd_cantidad);
            metodo =  itemView.findViewById(R.id.pd_metodo);
            deuda =   itemView.findViewById(R.id.view_deuda);
            deuda_total =  itemView.findViewById(R.id.du_total);
            deuda_fecha =  itemView.findViewById(R.id.du_fecha);
            deuda_moneda =  itemView.findViewById(R.id.du_modena);
            borderDeuda =  itemView.findViewById(R.id.boder_deuda);
            btnVer =  itemView.findViewById(R.id.pd_btn_ver);
            btnAnular =  itemView.findViewById(R.id.pd_btn_anular);
            mTxTotal = itemView.findViewById(R.id.res_tx_total);




        }
    }
}
