package com.goodmarcom.apk.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.Direcciones;
import com.goodmarcom.apk.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DireccionesPagoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = "PUERTOMAR";

    static Context context;
    private List<Direcciones> items;

    private ProgressDialog progressDialog;

    private int mPosition;

    private View.OnClickListener mClickListener;

    private int lastSelectedPosition = 0;


    public DireccionesPagoAdapter(List<Direcciones> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_direccion_pago, viewGroup, false);

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);


        return new DireccionesPagoAdapter.DireccionesPagoViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        final DireccionesPagoViewHolder holder = (DireccionesPagoViewHolder) viewHolder;

        holder.direccion.setText(items.get(i).getTx_direccion());

        holder.opcion.setChecked(lastSelectedPosition == i);


    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public Direcciones getItem(int position) {
        return items.get(position);
    }

    public int getmPosition() {
        return mPosition;
    }

    protected class DireccionesPagoViewHolder extends RecyclerView.ViewHolder {

        TextView direccion;
        RadioButton opcion;


        public DireccionesPagoViewHolder(@NonNull View itemView) {
            super(itemView);

            direccion = itemView.findViewById(R.id.dir_item_direccion_pago);
            opcion = itemView.findViewById(R.id.dir_radio_button);

            opcion.setOnClickListener(v -> {
                mPosition = getAdapterPosition();;
                mClickListener.onClick(v);
                lastSelectedPosition = mPosition;
                notifyDataSetChanged();
            });
        }

    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

}
