package com.goodmarcom.apk.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.Pedidos;
import com.goodmarcom.apk.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class DetallePedidoAdapter extends RecyclerView.Adapter<DetallePedidoAdapter.DetallePedidoViewHolder>{

    private static final String TAG = "PUERTOMAR";

    private List<Pedidos.DetallePedidos> items;
    private Context context;
    private SharedPreferences v_preferences;

    public DetallePedidoAdapter(List<Pedidos.DetallePedidos> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public DetallePedidoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_pedidos,parent,false);
        context = parent.getContext();

        //obtiene la referencia a la preferencia de la aplicacion
        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        return new DetallePedidoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DetallePedidoViewHolder holder, int i) {

        DecimalFormat numberFormat = new DecimalFormat(" #,###.00");

        //Picasso.with(context).load(items.get(i).getImagen()).placeholder(R.drawable.ic_progress_animation).into(holder.imagen);

        Picasso.get().load(items.get(i).getImagen()).placeholder(R.drawable.ic_progress_animation ).into(holder.imagen);

        Log.e(TAG, "Imagen:"+items.get(i).getImagen());

        String moneda = v_preferences.getString("simbmoneda","$");

        holder.cantidad.setText(String.format("%.0f",items.get(i).getNu_cantidad()));

        String nombre = "";
        nombre = items.get(i).getNombre();
        //holder.nombre.setText(nombre);
        if(!items.get(i).getValor().isEmpty()) {
            //holder.unidad.setText(items.get(i).getUnidad()+"/"+items.get(i).getValor());

            nombre = nombre + "\n" + items.get(i).getUnidad()+"/"+items.get(i).getValor();
        }

        holder.nombre.setText(nombre);

        holder.precio.setText(moneda+numberFormat.format(items.get(i).getNu_precio()));
        holder.subtotal.setText(moneda+numberFormat.format(items.get(i).getNu_cantidad()*items.get(i).getNu_precio()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class DetallePedidoViewHolder extends RecyclerView.ViewHolder{

        public TextView cantidad;
        public TextView nombre;
        //public TextView unidad;
        public TextView precio;
        public TextView subtotal;
        public ImageView imagen;

        public DetallePedidoViewHolder(View itemView) {
            super(itemView);

            cantidad =  itemView.findViewById(R.id.item_cantidad);
            nombre =  itemView.findViewById(R.id.item_nombre);
            //unidad =  itemView.findViewById(R.id.item_unidad);
            precio =  itemView.findViewById(R.id.item_precio);
            subtotal =  itemView.findViewById(R.id.item_subtotal);
            imagen =  itemView.findViewById(R.id.item_img);
        }
    }
}
