package com.goodmarcom.apk.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.goodmarcom.apk.Enty.Grupos;
import com.goodmarcom.apk.Enty.Productos;
import com.goodmarcom.apk.Fragments.CatalogoFragment;
import com.goodmarcom.apk.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;

public class GruposAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = "PUERTOMAR";

    private List<Grupos> items;
    private List<Grupos> GruposList;

    ArrayList<Integer> counter = new ArrayList<Integer>();

    private String search="";

    static Context context;
    private SharedPreferences v_preferences;

    private boolean busqueda;


    public GruposAdapter(List<Grupos> items) {
        this.items = items;
        this.GruposList = items;
    }

    public List<Grupos> getProductos() {
        return items;
    }

    public void setProductos(List<Grupos> grupos) {
        this.items = grupos;
    }

    public void setBusqueda(boolean busqueda)
    {
        this.busqueda = busqueda;
    }



    public void setCounter(){

        for (int i = 0; i < getItemCount(); i++) {
            counter.add(0);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void remove(Grupos g) {
        int position = items.indexOf(g);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }


    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public Grupos getItem(int position) {
        return items.get(position);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_grupos, viewGroup, false);

        v_preferences = context.getSharedPreferences("session",MODE_PRIVATE);

        return new GruposAdapter.GruposViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        final GruposViewHolder holder = (GruposViewHolder) viewHolder;


        if(items.get(i).getProductos().size()>0){
            holder.lyGrupo.setVisibility(View.VISIBLE);
            holder.nombre.setText(items.get(i).getTx_grupo());

            float radius = context.getResources().getDimension(R.dimen._12sdp);
            holder.imagen.setShapeAppearanceModel(holder.imagen.getShapeAppearanceModel()
                    .toBuilder()
                    .setTopRightCorner(CornerFamily.ROUNDED,radius)
                    .setTopLeftCorner(CornerFamily.ROUNDED,radius)
                    .build());

            Picasso.get().load(items.get(i).getImagen())
                    .placeholder(R.drawable.ic_progress_animation).into(holder.imagen);

        }else
        {
            holder.lyGrupo.setVisibility(View.GONE);
        }


        holder.imagen.setOnClickListener(view -> {

            Bundle parametros = new Bundle();

            //parametros.putSerializable("productos", items.get(i).getProductos());
            parametros.putInt("co_grupo", items.get(i).getCo_grupo());
            parametros.putString("tx_grupo", items.get(i).getTx_grupo());

            CatalogoFragment catalogoFragment = new CatalogoFragment();

            catalogoFragment.setArguments(parametros);

            ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_content, catalogoFragment,"catalogo_productos")
                    .commit();
           // currentTab = "catalogo";


        });


    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String searchString = charSequence.toString();

                search = searchString;

                Log.e(TAG,"Search Filter:"+search);

                List<Grupos> filteredList = new ArrayList<>();

                if (searchString.isEmpty()) {

                    filteredList = GruposList;

                } else {

                    for (Grupos g : GruposList) {

                        List<Productos> ProductosList =  buscarProductos(g.getProductos(),searchString);

                        int countFilter = ProductosList == null ? 0 : ProductosList.size();

                        if(countFilter>0){
                            Log.e(TAG,"Cant. Prod:"+countFilter);
                            Log.e(TAG,"Grupo:"+g.getTx_grupo());
                            filteredList.add(g);
                        }

                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                items = (ArrayList<Grupos>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public List<Productos> buscarProductos(List<Productos> productos, CharSequence charSequence ){

        String searchString = charSequence.toString();

        List<Productos> filteredList = new ArrayList<>();

      if(searchString.isEmpty())
      {
          filteredList = productos;

      }else {
          for (Productos p : productos) {

              if (p.getNombre().toLowerCase().contains(searchString.toLowerCase())) {

                  filteredList.add(p);
                  Log.e(TAG, "Productos:" + p.getNombre());

              }
          }
      }

        return  filteredList;
    }

    protected class GruposViewHolder extends RecyclerView.ViewHolder{

        private TextView nombre;
        public ShapeableImageView imagen;
        private RelativeLayout lyGrupo;

        public GruposViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre = itemView.findViewById(R.id.grp_nombre);
            lyGrupo = itemView.findViewById(R.id.ly_grupo);
            imagen =  itemView.findViewById(R.id.gr_imagen);


        }
    }
}
