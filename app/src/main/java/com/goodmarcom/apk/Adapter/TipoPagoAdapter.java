package com.goodmarcom.apk.Adapter;

import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.TipoPago;
import com.goodmarcom.apk.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TipoPagoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PUERTOMAR";

    static Context context;
    private List<TipoPago> items;

    private int mPosition;

    private View.OnClickListener mClickListener;

    private int lastSelectedPosition = 0;

    public TipoPagoAdapter(List<TipoPago> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_tipo_pago, viewGroup, false);


        return new TipoPagoAdapter.TipoPagoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {


        final TipoPagoViewHolder holder = (TipoPagoViewHolder) viewHolder;

        holder.tipopago.setText(items.get(i).getTx_tipo_pago());

        holder.opcion.setChecked(lastSelectedPosition == i);

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public TipoPago getItem(int position) {
        return items.get(position);
    }

    public int getmPosition() {
        return mPosition;
    }


    protected class TipoPagoViewHolder extends RecyclerView.ViewHolder {

        TextView tipopago;
        RadioButton opcion;


        public TipoPagoViewHolder(@NonNull View itemView) {
            super(itemView);

            tipopago = itemView.findViewById(R.id.item_tipo_pago);
            opcion = itemView.findViewById(R.id.radio_button_tpago);

            opcion.setOnClickListener(v -> {
                mPosition = getAdapterPosition();
                mClickListener.onClick(v);
                lastSelectedPosition = mPosition;
                notifyDataSetChanged();
            });
        }

    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

}
