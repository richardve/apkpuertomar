package com.goodmarcom.apk.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;


import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Enty.Direcciones;
import com.goodmarcom.apk.R;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class DireccionesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = "PUERTOMAR";

    static Context context;
    private List<Direcciones> items;

    private PuertomarApiServices direccioneService;

    private String token;

    private ProgressDialog progressDialog;

    private AdapterView.OnItemClickListener onItemClickListener;

    private int mPosition;

    private View.OnClickListener mClickListener;

    private View.OnClickListener mClickListenerEliminar;


    public DireccionesAdapter(List<Direcciones> items) {
        this.items = items;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {

        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_direccion, viewGroup, false);


        direccioneService = PuertomarApiAdapter.getApiService();

        SharedPreferences v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);



        return new DireccionesAdapter.DireccionesViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        final DireccionesViewHolder holder = (DireccionesViewHolder) viewHolder;

        holder.direccion.setText(items.get(i).getTx_direccion());

        if(items.get(i).isTienePedido())
        {
            holder.btnEliminar.setVisibility(View.GONE);
            holder.btnEditar.setVisibility(View.GONE);
        }

        holder.btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPosition = i;
                Log.e(TAG, "Posición:"+mPosition);
                mClickListenerEliminar.onClick(v);

                //eliminarDireccion(items.get(i).getCo_direccion(),i);

              }
        });

        holder.btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPosition = i;
                Log.e(TAG, "Posición:"+mPosition);
                mClickListener.onClick(v);
            }
        });


     /*   if(i%2 == 0)
        {
            holder.cardView.setBackgroundResource(R.color.gris_claro);
        }
        else
        {
            holder.cardView.setBackgroundResource(R.color.colorBlanco);
        }*/


    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public Direcciones getItem(int position) {
        return items.get(position);
    }

    public int getmPosition() {
        return mPosition;
    }

    protected class DireccionesViewHolder extends RecyclerView.ViewHolder {

        TextView direccion;
        ImageView btnEditar;
        ImageView btnEliminar;
        CardView cardView;


        public DireccionesViewHolder(@NonNull View itemView) {
            super(itemView);

            direccion = itemView.findViewById(R.id.dir_item_direccion);
            btnEditar = itemView.findViewById(R.id.dir_edit);
            btnEliminar = itemView.findViewById(R.id.dir_delete);
            cardView = itemView.findViewById(R.id.dir_carview);
        }


    }


    /*public void eliminarDireccion(Integer id_direccion, Integer position) {

        progressDialog.show();

        Call<DireccionesResponse> call = direccioneService.deleteDireccion("Bearer " + token, id_direccion);

        call.enqueue(new EliminarCallback(position));

    }

     private class EliminarCallback implements Callback<DireccionesResponse>
        {
            private int position;

            public EliminarCallback(int position) {

                this.position = position;
            }


            @Override
            public void onResponse(Call<DireccionesResponse> call, Response<DireccionesResponse> response) {
                progressDialog.dismiss();

                if(response.isSuccessful()){

                      items.remove(position);
                      notifyItemRemoved(position);
                      //notifyDataSetChanged();

                      Toast.makeText(context, "Dirección eliminada con éxito!", Toast.LENGTH_SHORT).show();


                }else{

                    try {

                        assert response.errorBody() != null;
                        JSONObject jObjError = new JSONObject(response.errorBody().string());

                        JSONArray arrJson = jObjError.getJSONArray("error");

                        Toast.makeText(context, arrJson.get(0).toString(), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<DireccionesResponse> call, Throwable t) {
                progressDialog.show();

                Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();

                Log.e("ERROR: ", t.getMessage());
            }
        }*/



    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

    public void setmClickListenerEliminar(View.OnClickListener callback) {
        mClickListenerEliminar = callback;
    }



}
