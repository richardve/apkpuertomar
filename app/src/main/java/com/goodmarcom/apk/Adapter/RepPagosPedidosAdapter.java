package com.goodmarcom.apk.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
/*import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;*/
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.Pedidos;
import com.goodmarcom.apk.Fragments.DetallePedidosOpcionFragment;
import com.goodmarcom.apk.R;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class RepPagosPedidosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Pedidos> items;
    private Context context;
    private ProgressDialog progressDialog;
    private SharedPreferences v_preferences;


    public RepPagosPedidosAdapter(List<Pedidos> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_rep_pedidos,viewGroup,false);

        context = viewGroup.getContext();

        //obtiene la referencia a la preferencia de la aplicacion
        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        return new RepPagosPedidosAdapter.RepPagosPedidosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull  RecyclerView.ViewHolder viewHolder, final int i) {

        final RepPagosPedidosViewHolder holder = (RepPagosPedidosViewHolder) viewHolder;


        DecimalFormat numberFormat = new DecimalFormat(" #,###.00");

        holder.estatus.setText(items.get(i).getEstatus());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatOut = new SimpleDateFormat("dd/MM/yyyy");
        try {
                Date date = format.parse(items.get(i).getFecha());
                holder.fecha.setText(formatOut.format(date));
        } catch (ParseException e) {
                e.printStackTrace();
        }


        String moneda = v_preferences.getString("simbmoneda", "$");

        Double total_pedido,
                recargo=0.00,
                subtotal=items.get(i).getTotal(),
                mNuPorc = Double.valueOf(items.get(i).getPago_pedido().getNu_porc());


        recargo = mNuPorc*subtotal;

        total_pedido = subtotal + recargo;

         holder.mTxTotal.setText(numberFormat.format(total_pedido)+moneda);
         holder.codigo.setText("Pedido N°: " + items.get(i).getCodigo());
         holder.cantidad.setText(String.format("%.0f", items.get(i).getCantidad()));
         holder.metodo.setText(items.get(i).getTx_forma_pago());
         holder.tipoPago.setText(items.get(i).getTx_tipo_pago());


        final int position = i;

        holder.btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle parametros = new Bundle();

                parametros.putString("opcion", "Pagar");
                parametros.putString("fragment_origen", "reporte_pagos");

                DetallePedidosOpcionFragment frag = new DetallePedidosOpcionFragment(items.get(position));

                frag.setArguments(parametros);

                ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_content, frag , "detpedidosopcion")
                        .addToBackStack(null)
                        .commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }


    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void remove(Pedidos p) {
        int position = items.indexOf(p);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public Pedidos getItem(int position) {
        return items.get(position);
    }

    public static class RepPagosPedidosViewHolder extends RecyclerView.ViewHolder{

        public TextView estatus;
        public TextView codigo;
        public TextView fecha;
        public TextView cantidad;
        public TextView total;
        public TextView moneda;
        public TextView metodo;
        public TextView tipoPago;

        public ImageView btnVer;

        /*private TextView mLbSubTotal;
        private TextView mLbRecargo;


        private TextView mTxSubTotal;
        private TextView mTxRecargo;*/
        private TextView mTxTotal;




        public RepPagosPedidosViewHolder(View itemView) {
            super(itemView);

            estatus =  itemView.findViewById(R.id.rep_estatus);
            fecha =    itemView.findViewById(R.id.rep_fecha);
            codigo =   itemView.findViewById(R.id.rep_codigo);
            cantidad = itemView.findViewById(R.id.rep_cantidad);
            metodo =  itemView.findViewById(R.id.rep_metodo_pago);
            tipoPago =  itemView.findViewById(R.id.rep_tipo_pago);
            btnVer =  itemView.findViewById(R.id.rep_btn_ver);

            /*mLbSubTotal = itemView.findViewById(R.id.rep_lb_subtotal);
            mLbRecargo = itemView.findViewById(R.id.rep_lb_recargo);


            mTxSubTotal = itemView.findViewById(R.id.rep_tx_subtotal);
            mTxRecargo = itemView.findViewById(R.id.rep_tx_recargo);*/
            mTxTotal = itemView.findViewById(R.id.rep_tx_total);


           /* mLbSubTotal.setVisibility(View.GONE);
            mLbRecargo.setVisibility(View.GONE);
            mTxSubTotal.setVisibility(View.GONE);
            mTxRecargo.setVisibility(View.GONE);*/

        }
    }

}
