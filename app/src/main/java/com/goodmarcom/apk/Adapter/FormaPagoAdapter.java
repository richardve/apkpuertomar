package com.goodmarcom.apk.Adapter;

import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

//import com.puertomar.apk.Enty.FormaPago;
import com.goodmarcom.apk.Enty.FormaPagoPerfil;
import com.goodmarcom.apk.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FormaPagoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PUERTOMAR";

    static Context context;
    private List<FormaPagoPerfil> items;

    private int mPosition;

    private View.OnClickListener mClickListener;

    private int lastSelectedPosition = 0;


    public FormaPagoAdapter(List<FormaPagoPerfil> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_forma_pago, viewGroup, false);


        return new FormaPagoAdapter.FormaPagoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        final FormaPagoViewHolder holder = (FormaPagoViewHolder) viewHolder;

        holder.formapago.setText(items.get(i).getMetodo().getTx_forma_pago());

        holder.opcion.setChecked(lastSelectedPosition == i);

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public FormaPagoPerfil getItem(int position) {
        return items.get(position);
    }

    public int getmPosition() {
        return mPosition;
    }


    protected class FormaPagoViewHolder extends RecyclerView.ViewHolder {

        TextView formapago;
        RadioButton opcion;


        public FormaPagoViewHolder(@NonNull View itemView) {
            super(itemView);

            formapago = itemView.findViewById(R.id.item_forma_pago);
            opcion = itemView.findViewById(R.id.radio_button_fpago);

            opcion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPosition = getAdapterPosition();;
                    mClickListener.onClick(v);
                    lastSelectedPosition = mPosition;
                    notifyDataSetChanged();
                }
            });
        }

    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

}
