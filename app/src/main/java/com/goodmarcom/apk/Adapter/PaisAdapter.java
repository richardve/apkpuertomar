package com.goodmarcom.apk.Adapter;

import android.content.Context;
/*import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;*/
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.Pais;
import com.goodmarcom.apk.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PaisAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = "PUERTOMAR";

    static Context context;
    private List<Pais> items;
    private List<Pais> PaisList;


    private int mPosition;

    private View.OnClickListener mClickListener;

    private int lastSelectedPosition = 0;


    public PaisAdapter(List<Pais> items) {

        this.items = items;
        this.PaisList = items;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_paises, viewGroup, false);


        return new PaisAdapter.PaisViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        final PaisViewHolder holder = (PaisViewHolder) viewHolder;

        holder.pais.setText(items.get(i).getTx_pais());

        holder.opcion.setChecked(lastSelectedPosition == i);

    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void remove(Pais p) {
        int position = items.indexOf(p);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public Pais getItem(int position) {
        return items.get(position);
    }

    public int getmPosition() {
        return mPosition;
    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String searchString = charSequence.toString();

                List<Pais> filteredList = new ArrayList<>();

                if (searchString.isEmpty()) {

                    filteredList = PaisList;

                } else {

                    for (Pais p : PaisList) {

                        if (p.getTx_pais().toLowerCase().contains(searchString.toLowerCase())) {

                            //Log.e(TAG,"pais daptersearch:"+p.getTx_pais());

                            filteredList.add(p);
                        }
                    }

                    //filteredPaisList = lteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                items = (ArrayList<Pais>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    protected class PaisViewHolder extends RecyclerView.ViewHolder {

        TextView pais;
        RadioButton opcion;


        public PaisViewHolder(@NonNull View itemView) {
            super(itemView);

            pais = itemView.findViewById(R.id.item_pais);
            opcion = itemView.findViewById(R.id.radio_button_pais);

            opcion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPosition = getAdapterPosition();;
                    mClickListener.onClick(v);
                    lastSelectedPosition = mPosition;
                    notifyDataSetChanged();


                }
            });
        }

    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }
}
