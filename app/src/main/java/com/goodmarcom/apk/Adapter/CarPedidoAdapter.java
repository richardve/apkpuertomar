package com.goodmarcom.apk.Adapter;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Db.DBHelper;
import com.goodmarcom.apk.Enty.Productos;
import com.goodmarcom.apk.Fragments.CarlistFragment;
import com.goodmarcom.apk.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;


public class CarPedidoAdapter extends RecyclerView.Adapter<CarPedidoAdapter.CarPedidoViewHolder>{

    private static final String TAG = "PUERTOMAR";

    private List<Productos> items;
    static Context context;
    private SharedPreferences v_preferences;
    private Boolean mGuardado;
    private Boolean mEliminar;
    private String txCantidad;

    public CarPedidoAdapter(List<Productos> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public CarPedidoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_carpedidos,viewGroup,false);

        context = v.getContext();

        //obtiene la referencia a la preferencia de la aplicacion
        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        return new CarPedidoViewHolder(v);

    }

    public void eliminarProducto(Long id, int position){

        //eliminar el elemento de la base de datos
        final DBHelper v_db_helper = new DBHelper(context);
        v_db_helper.deleteProd(id);

        MainActivity.notificationCountCart = MainActivity.notificationCountCart - Integer.valueOf(items.get(position).getCantidad());

        MainActivity.setBadge(MainActivity.notificationCountCart);

        //eliminar el elemento del recicleview
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,items.size());

    }


    @Override
    public void onBindViewHolder(@NonNull final CarPedidoViewHolder holder, int i) {

        DecimalFormat numberFormat = new DecimalFormat(" #,###.00");
        String simboloMoneda;
        mGuardado= false;
        mEliminar = true;

        simboloMoneda = v_preferences.getString("simbmoneda","$");

        //Picasso.with(context).load(items.get(i).getImagen()).placeholder(R.drawable.ic_progress_animation ).into(holder.imagen);

        Picasso.get().load(items.get(i).getImagen())
                .placeholder(R.drawable.ic_progress_animation)
                .into(holder.imagen);

        String nombre = "";

        nombre = items.get(i).getNombre();

        Log.e(TAG, "Unidad:"+items.get(i).getTx_unidad());
        Log.e(TAG, "Valor:"+items.get(i).getTx_valor());

        if(!items.get(i).getTx_valor().isEmpty()) {

            nombre = nombre + "\n" +  items.get(i).getTx_unidad() + "/"+ items.get(i).getTx_valor();
        }


        holder.nombre.setText(nombre);

        double monto = Double.valueOf(Double.valueOf(items.get(i).getPrecio()));

        holder.precio.setText(simboloMoneda+numberFormat.format(monto));
        holder.cantidad.setText(items.get(i).getCantidad());
        float subTotal = Integer.valueOf(items.get(i).getCantidad())*Float.valueOf(items.get(i).getPrecio());

        holder.subtotal.setText(simboloMoneda+numberFormat.format(subTotal));

        final int position = i;
        holder.btnEliminar.setOnClickListener(v -> {

            if (mEliminar) {

                eliminarProducto(items.get(position).getId(), position);

                if (items.size() == 0) {
                    BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);
                    navigation.setSelectedItemId(R.id.navigation_catalogo);
                } else {
                    //refresh el fragmento
                    Fragment currentFragment = ((MainActivity) context).getSupportFragmentManager().findFragmentById(R.id.fragment_content);
                    if (currentFragment instanceof CarlistFragment) {
                        FragmentTransaction fragTransaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
                        fragTransaction.detach(currentFragment);
                        fragTransaction.attach(currentFragment);
                        fragTransaction.commit();
                    }
                }

            }else {

                //activar el update de los datos en la db local
                Productos producto = items.get(position);

                //obtener el valor modificado
                String valor = holder.cantidad.getText().toString();

                float precio = Float.valueOf(producto.getPrecio());

                //actualizar la cantidad en el badge
                //TextView carBadge = (TextView) ((MainActivity) context).findViewById(R.id.tx_badge);
                //carBadge.setText(String.valueOf(Integer.valueOf(carBadge.getText().toString())-Integer.valueOf(producto.getCantidad())));

                MainActivity.notificationCountCart = MainActivity.notificationCountCart - Integer.valueOf(producto.getCantidad()) + Integer.valueOf(valor);


                float subTotal1 =  Float.valueOf(valor)*precio;

                holder.subtotal.setText(String.valueOf(subTotal1));

                if(MainActivity.notificationCountCart==0) MainActivity.mMenu.findItem(R.id.action_cart).setVisible(false);

                MainActivity.setBadge(MainActivity.notificationCountCart);

                producto.setCantidad(valor);

                items.get(position).setCantidad(valor);

                final DBHelper v_db_helper = new DBHelper(context);
                v_db_helper.updateProd(producto);

                mGuardado = true;
                mEliminar = true;

                //refresh el fragmento
                Fragment currentFragment = ((MainActivity) context).getSupportFragmentManager().findFragmentById(R.id.fragment_content);
                if (currentFragment instanceof CarlistFragment) {
                    FragmentTransaction fragTransaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
                    fragTransaction.detach(currentFragment);
                    fragTransaction.attach(currentFragment);
                    fragTransaction.commit();
                }

            }
        });


    /*    holder.cantidad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                    holder.icon.setImageResource(R.drawable.ic_save);
                    txCantidad = holder.cantidad.getText().toString();
                    mEliminar = false;
                    Log.e("PUERTOMAR", "Obtuvo el Focus.."+ holder.cantidad.getText().toString());
                }else {
                    Log.e("PUERTOMAR", "Perdió el Focus..");
                    if(!mGuardado)
                    {
                        holder.cantidad.setText(txCantidad);
                        holder.icon.setImageResource(R.drawable.ic_delete_black);
                        mEliminar = false;


                    }

                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class CarPedidoViewHolder extends RecyclerView.ViewHolder {

        public ImageView imagen;
        public TextView nombre;
        public TextView unidad;
        public TextView precio;
        public TextView subtotal;
        public TextView cantidad;
        public View btnEliminar;
        public ImageView icon;


        public CarPedidoViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre =  itemView.findViewById(R.id.lc_nombre);
            //unidad =  itemView.findViewById(R.id.lc_unidad);
            precio =  itemView.findViewById(R.id.lc_precio);
            subtotal = itemView.findViewById(R.id.lc_subtotal);
            imagen =  itemView.findViewById(R.id.lc_imagen);
            cantidad = itemView.findViewById(R.id.lc_cantidad);
            btnEliminar = itemView.findViewById(R.id.lc_eliminar);
            icon =  itemView.findViewById(R.id.lc_icon);

        }
    }

}
