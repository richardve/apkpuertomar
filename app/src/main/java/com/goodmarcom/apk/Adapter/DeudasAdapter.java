package com.goodmarcom.apk.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
/*import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;*/
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.Deudas;
import com.goodmarcom.apk.R;

import java.text.DecimalFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DeudasAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = "PUERTOMAR";

    private List<Deudas> items;

    static Context context;

    private SharedPreferences v_preferences;


    public DeudasAdapter(List<Deudas> items) {  this.items = items; }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_deudas, viewGroup, false);

        //obtiene la referencia a la preferencia de la aplicacion
        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);


        return new DeudasAdapter.DeudasViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        DecimalFormat numberFormat = new DecimalFormat(" #,##0.00");

        String moneda = v_preferences.getString("simbmoneda","$");


        final DeudasViewHolder holder = (DeudasViewHolder) viewHolder;

        holder.pedido.setText(String.format("Pedido N°: %s", String.valueOf(items.get(i).getCo_pedido())));
        holder.monto.setText(String.format("%s%s", moneda,numberFormat.format(Double.valueOf(items.get(i).getNu_monto()))));
        holder.saldo.setText(String.format("%s%s", moneda,numberFormat.format(Double.valueOf(items.get(i).getNu_saldo()))));

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }


    protected class DeudasViewHolder extends RecyclerView.ViewHolder{

        TextView pedido;
        TextView monto;
        TextView saldo;


        public DeudasViewHolder(@NonNull View itemView) {
            super(itemView);

            pedido = itemView.findViewById(R.id.deuda_pedido);
            monto = itemView.findViewById(R.id.dueda_monto);
            saldo = itemView.findViewById(R.id.deuda_saldo);

        }
    }



}
