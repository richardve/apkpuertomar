package com.goodmarcom.apk.Adapter;

//import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.goodmarcom.apk.Enty.Recetas;
import com.goodmarcom.apk.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecetasAdapter extends RecyclerView.Adapter<RecetasAdapter.RecetasViewHolder> {

    private List<Recetas> items;
    private Context context;

    public RecetasAdapter(List<Recetas> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public RecetasViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_recetas, viewGroup, false);

        return new RecetasViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecetasViewHolder holder, final int i) {

        float radius = context.getResources().getDimension(R.dimen._12sdp);
        holder.imagen.setShapeAppearanceModel(holder.imagen.getShapeAppearanceModel()
                .toBuilder()
                .setTopRightCorner(CornerFamily.ROUNDED,radius)
                .setTopLeftCorner(CornerFamily.ROUNDED,radius)
                .build());

        Picasso.get().load(items.get(i).getImagen()).placeholder( R.drawable.ic_progress_animation).into(holder.imagen);
        holder.titulo.setText(items.get(i).getTitulo());
        holder.descripcion.setText(items.get(i).getDescripcion());

    /*    holder.imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View v = LayoutInflater.from(context).inflate(R.layout.imagen_show
                        , null);
                ImageView image = (ImageView) v.findViewById(R.id.imagen_show);

                //Bitmap bm = ((BitmapDrawable) showImage.getDrawable()).getBitmap();

                //image.setImageBitmap(bm);
                Picasso.with(context).load(items.get(i).getImagen()).into(image);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                Dialog settingsDialog = new Dialog(context);
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                settingsDialog.setContentView(v);

                settingsDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                settingsDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT);

                settingsDialog.show();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public static class RecetasViewHolder extends RecyclerView.ViewHolder {

        public ShapeableImageView imagen;
        public TextView titulo;
        public TextView descripcion;

        public RecetasViewHolder(View itemView) {
            super(itemView);

            titulo =  itemView.findViewById(R.id.rc_titulo);
            descripcion =  itemView.findViewById(R.id.rc_descripcion);
            imagen = itemView.findViewById(R.id.rc_imagen);

        }
    }
}