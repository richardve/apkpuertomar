package com.goodmarcom.apk.Adapter;

import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.Ciudad;
import com.goodmarcom.apk.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CiudadAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = "PUERTOMAR";

    static Context context;
    private List<Ciudad> items;
    private List<Ciudad> CiudadList;

    private int mPosition;

    private View.OnClickListener mClickListener;

    private int lastSelectedPosition = 0;


    public CiudadAdapter(List<Ciudad> items) {

        this.items = items;
        this.CiudadList = items;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_ciudades, viewGroup, false);


        return new CiudadAdapter.CiudadViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        final CiudadViewHolder holder = (CiudadViewHolder) viewHolder;

        holder.ciudad.setText(items.get(i).getTx_ciudad());

        holder.opcion.setChecked(lastSelectedPosition == i);

    }

    public void clear() {
        //isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void remove(Ciudad c) {
        int position = items.indexOf(c);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public Ciudad getItem(int position) {
        return items.get(position);
    }

    public int getmPosition() {
        return mPosition;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String searchString = charSequence.toString();

                List<Ciudad> filteredList = new ArrayList<>();

                if (searchString.isEmpty()) {

                    filteredList = CiudadList;

                } else {

                    for (Ciudad c : CiudadList) {

                        if (c.getTx_ciudad().toLowerCase().contains(searchString.toLowerCase())) {

                            Log.e(TAG,"ciudad dapter search:"+c.getTx_ciudad());

                            filteredList.add(c);
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                items = (ArrayList<Ciudad>) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }


    protected class CiudadViewHolder extends RecyclerView.ViewHolder {

        TextView ciudad;
        RadioButton opcion;


        public CiudadViewHolder(@NonNull View itemView) {
            super(itemView);

            ciudad = itemView.findViewById(R.id.item_ciudad);
            opcion = itemView.findViewById(R.id.radio_button_ciudad);

            opcion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPosition = getAdapterPosition();;
                    mClickListener.onClick(v);
                    lastSelectedPosition = mPosition;
                    notifyDataSetChanged();
                }
            });
        }

    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

}
