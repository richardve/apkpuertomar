package com.goodmarcom.apk.Adapter;

import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.goodmarcom.apk.Enty.TipoDocumento;
import com.goodmarcom.apk.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TipoDocumentoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PUERTOMAR";


    static Context context;
    private List<TipoDocumento> items;

    private int mPosition;

    private View.OnClickListener mClickListener;

    private int lastSelectedPosition = 0;


    public TipoDocumentoAdapter(List<TipoDocumento> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_tipo_documento, viewGroup, false);


        return new TipoDocumentoAdapter.TipoDocumentoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        final TipoDocumentoViewHolder holder = (TipoDocumentoViewHolder) viewHolder;

        holder.tipodocumento.setText(items.get(i).getTx_documento());

        holder.opcion.setChecked(lastSelectedPosition == i);

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public TipoDocumento getItem(int position) {
        return items.get(position);
    }

    public int getmPosition() {
        return mPosition;
    }


    protected class TipoDocumentoViewHolder extends RecyclerView.ViewHolder {

        TextView tipodocumento;
        RadioButton opcion;


        public TipoDocumentoViewHolder(@NonNull View itemView) {
            super(itemView);

            tipodocumento = itemView.findViewById(R.id.item_tipo_documento);
            opcion = itemView.findViewById(R.id.radio_button_tdocumento);

            opcion.setOnClickListener(v -> {
                mPosition = getAdapterPosition();
                mClickListener.onClick(v);
                lastSelectedPosition = mPosition;
                notifyDataSetChanged();
            });
        }

    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

}
