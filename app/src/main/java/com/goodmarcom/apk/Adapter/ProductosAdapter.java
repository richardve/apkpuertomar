package com.goodmarcom.apk.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Db.DBHelper;
import com.goodmarcom.apk.Enty.Productos;
import com.goodmarcom.apk.Fragments.ComboFragment;
import com.goodmarcom.apk.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;


public class ProductosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = "PUERTOMAR";

    private static final int ITEM = 0;
    private static final int LOADING = 1;


    private List<Productos> items;
    private List<Productos> productos;

    ArrayList<Integer> counter = new ArrayList<Integer>();

    private Integer countFilter;

   // private boolean isLoadingAdded = false;

    static  Context context;
    private Productos mProducto;
    private SharedPreferences v_preferences;
    private DBHelper v_db_helper;
    private String moneda;
    private DecimalFormat numberFormat;


     public ProductosAdapter(List<Productos> items) {
        this.items = items;
        this.productos = items;

    }


    public List<Productos> getProductos() {
        return items;
    }

    public void setProductos(List<Productos> productos) {
         this.items = productos;
    }


    public void setCounter(){

        for (int i = 0; i < getItemCount(); i++) {
            counter.add(0);
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        context = viewGroup.getContext();

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_productos, viewGroup, false);

        v_db_helper = new DBHelper(context);

        setCounter();

        v_preferences = context.getSharedPreferences("session",MODE_PRIVATE);

        return new ProductosViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        final ProductosViewHolder holder = (ProductosViewHolder) viewHolder;

        numberFormat = new DecimalFormat(" #,##0.00");

        Picasso.get().load(items.get(i).getImagen())
                .placeholder(R.drawable.ic_progress_animation)
                .into(holder.imagen);

        holder.nombre.setText(items.get(i).getNombre());

        Productos dbProducto = v_db_helper.queryProdByNombre(items.get(i).getNombre());

        if(dbProducto.getCantidad().isEmpty()) {
            holder.cantidad.setText("0");
        }else {
            holder.cantidad.setText(dbProducto.getCantidad());
        }

        Double monto = Double.valueOf(items.get(i).getPrecio());

        Log.e(TAG, "Nombre Producto:" + items.get(i).getNombre());
        Log.e(TAG, "Es Combo:" + items.get(i).isCombo());

        if (items.get(i).isCombo()&&items.get(i).getCombo().size()>1) holder.imagenBotton.setVisibility(View.VISIBLE);

        holder.precio.setText(numberFormat.format(monto));
        ;
        moneda = v_preferences.getString("simbmoneda", "$");

        holder.moneda.setText(moneda);
        final Integer codigo = items.get(i).getCo_producto();
        final Integer position = i;


        Double monto1 = Double.valueOf(items.get(i).getPrecio());
        double precio;

        if (!items.get(i).getValor().isEmpty()) {

            holder.ly_detalle.setVisibility(View.VISIBLE);

            holder.unidad.setText(items.get(i).getUnidad() + "/" + items.get(i).getValor());

            String cantidad = items.get(i).getValor();

            cantidad = cantidad.replaceAll("[^0-9]", "");

            precio = monto1 / Double.valueOf(cantidad);

            Log.e(TAG, "Producto:" + items.get(i).getNombre());
            Log.e(TAG, "Precio:" + monto1);
            Log.e(TAG, "Cantidad:" + cantidad);


        } else {
            holder.unidad.setText("");
            precio = monto1;
        }

        holder.precio_und.setText(moneda+numberFormat.format(precio));


        holder.btnDown.setOnClickListener(v -> {
            //obtener el valor actual del view
            Integer cantidad = Integer.valueOf(holder.cantidad.getText().toString());

            if (cantidad > 0) {
                holder.cantidad.setText(String.valueOf(cantidad - 1));
                eliminarCarrito(holder,position);
            }
        });

        holder.btnUp.setOnClickListener(v -> {
            //obtener el valor actual del view
            Integer cantidad = Integer.valueOf(holder.cantidad.getText().toString());
            cantidad = cantidad +1;
            holder.cantidad.setText(String.valueOf(cantidad));

            agregarCarrito(holder,position);

        });

       /* holder.btnAgregar.setOnClickListener(v -> {

            if (holder.cantidad.getText().toString().trim().isEmpty()) {
                Toast.makeText(context, "Debe Introducir la cantidad del producto", Toast.LENGTH_SHORT).show();
                holder.cantidad.requestFocus();
            } else {
                int cantidad = Integer.valueOf(holder.cantidad.getText().toString());

                if (cantidad > 0) {

                    if (MainActivity.notificationCountCart == 0)
                        MainActivity.mMenu.findItem(R.id.action_cart).setVisible(true);

                    mProducto = items.get(position);

                    mProducto.setCantidad(String.valueOf(cantidad));

                    double precio = Double.valueOf(mProducto.getPrecio());

                    mProducto.setPrecio(Double.toString(precio));

                    //verificar si el producto ya esta registrado en db
                    Log.e("detalles-producto", mProducto.getNombre());
                    Productos dbProducto = v_db_helper.queryProdByNombre(mProducto.getNombre());
                    Log.e("detalles-producto", "nombre del producto encontrado " + dbProducto.getNombre());

                    if (dbProducto.getNombre() == null) {

                        try {

                            v_db_helper.insertProd(mProducto);
                            Toast.makeText(context, "Producto Agregado al Pedido", Toast.LENGTH_SHORT).show();
                            MainActivity.notificationCountCart = MainActivity.notificationCountCart + Integer.valueOf(mProducto.getCantidad());
                            MainActivity.setBadge(MainActivity.notificationCountCart);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error al guadar el producto", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //actualizar el producto
                        int total, total_db, total_badge, total_add;

                        total_db = Integer.parseInt(dbProducto.getCantidad());
                        total_add = Integer.parseInt(mProducto.getCantidad());
                        // total_badge = Integer.valueOf(carBadge.getText().toString());
                        total_badge = MainActivity.notificationCountCart;
                        total = total_add + total_db;

                        Log.e("agregar-producto", String.valueOf(total));

                        MainActivity.notificationCountCart = total_badge - total_db + total;

                        MainActivity.setBadge(MainActivity.notificationCountCart);

                        mProducto.setCantidad(Integer.toString(total));
                        mProducto.setId(dbProducto.getId());
                        v_db_helper.updateProd(mProducto);
                        Toast.makeText(context, "Producto Agregado al Pedido", Toast.LENGTH_SHORT).show();
                    }

                    holder.cantidad.setText("0");

                } else {
                    Toast.makeText(context, "Debe Introducir la cantidad del producto", Toast.LENGTH_SHORT).show();
                    holder.cantidad.requestFocus();
                }
            }

        });*/


        holder.imagen.setOnClickListener(view -> {

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
            View mView = LayoutInflater.from(context).inflate(R.layout.imagen_show, null);

            ImageView photoView = mView.findViewById(R.id.imagen_show);
            Picasso.get().load(items.get(i).getImagen()).into(photoView);


            mBuilder.setView(mView);
            AlertDialog mDialog = mBuilder.create();
            mDialog.setCancelable(true);
            mDialog.setCanceledOnTouchOutside(true);
            mDialog.show();

        });


      /* holder.detalle.setOnClickListener(view -> {

            if ((counter.get(i) % 2 == 0)) {
                holder.ly_detalle.setVisibility(View.VISIBLE);
                holder.detalle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less, 0);*/

            /*} else {
                holder.ly_detalle.setVisibility(View.GONE);
                holder.detalle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more, 0);
            }*/

            //counter.set(i, counter.get(i) + 1);
//      });


        holder.imagenBotton.setOnClickListener(view -> showPopupMenu(holder.imagenBotton, items.get(i).getCo_producto()));

    }

    private void eliminarCarrito(ProductosViewHolder holder, int position){

        if (holder.cantidad.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Debe Introducir la cantidad del producto", Toast.LENGTH_SHORT).show();
            holder.cantidad.requestFocus();
        } else {
            int cantidad = Integer.valueOf(holder.cantidad.getText().toString());

            if (cantidad >= 0) {

                if (MainActivity.notificationCountCart == 0)
                    MainActivity.mMenu.findItem(R.id.action_cart).setVisible(true);

                mProducto = items.get(position);

                mProducto.setCantidad(String.valueOf(cantidad));

                double precio = Double.valueOf(mProducto.getPrecio());

                mProducto.setPrecio(Double.toString(precio));

                //verificar si el producto ya esta registrado en db
                Log.e("detalles-producto", mProducto.getNombre());
                Productos dbProducto = v_db_helper.queryProdByNombre(mProducto.getNombre());
                Log.e("detalles-producto", "nombre del producto encontrado " + dbProducto.getNombre());

                    //actualizar el producto
                    int total, total_db, total_badge, total_prod;

                    total_db = Integer.parseInt(dbProducto.getCantidad());
                    //total_prod = Integer.parseInt(mProducto.getCantidad()) - 1;
                    // total_badge = Integer.valueOf(carBadge.getText().toString());
                    total_badge = MainActivity.notificationCountCart;
                    total = total_db -1;

                    Log.e("agregar-producto", String.valueOf(total));

                    MainActivity.notificationCountCart = total_badge - 1; // total_db + total;

                    MainActivity.setBadge(MainActivity.notificationCountCart);

                    mProducto.setCantidad(Integer.toString(total));
                    mProducto.setId(dbProducto.getId());

                    if(total == 0)
                    {
                        v_db_helper.deleteProd(dbProducto.getId());
                        Toast.makeText(context, "Producto Eliminado del Pedido", Toast.LENGTH_SHORT).show();
                    }else {
                        v_db_helper.updateProd(mProducto);
                        Toast.makeText(context, "Producto Aactualizado en el Pedido", Toast.LENGTH_SHORT).show();
                    }



                //holder.cantidad.setText("0");

            } else {
                Toast.makeText(context, "Debe Introducir la cantidad del producto", Toast.LENGTH_SHORT).show();
                holder.cantidad.requestFocus();
            }
        }

    }

    private void agregarCarrito(ProductosViewHolder holder, int position){

        if (holder.cantidad.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Debe Introducir la cantidad del producto", Toast.LENGTH_SHORT).show();
            holder.cantidad.requestFocus();
        } else {
            int cantidad = Integer.valueOf(holder.cantidad.getText().toString());

            if (cantidad >= 0) {

                if (MainActivity.notificationCountCart == 0)
                    MainActivity.mMenu.findItem(R.id.action_cart).setVisible(true);

                mProducto = items.get(position);

                mProducto.setCantidad(String.valueOf(cantidad));

                double precio = Double.valueOf(mProducto.getPrecio());

                mProducto.setPrecio(Double.toString(precio));

                //verificar si el producto ya esta registrado en db
                Log.e("detalles-producto", mProducto.getNombre());
                Productos dbProducto = v_db_helper.queryProdByNombre(mProducto.getNombre());
                Log.e("detalles-producto", "nombre del producto encontrado " + dbProducto.getNombre());

                if (dbProducto.getNombre() == null) {

                    try {

                        v_db_helper.insertProd(mProducto);

                        Toast.makeText(context, "Producto Agregado al Pedido", Toast.LENGTH_SHORT).show();
                        MainActivity.notificationCountCart = MainActivity.notificationCountCart + Integer.valueOf(mProducto.getCantidad());
                        MainActivity.setBadge(MainActivity.notificationCountCart);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Error al guadar el producto", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //actualizar el producto
                    int total, total_db, total_badge, total_add;

                    total_db = Integer.parseInt(dbProducto.getCantidad());
                    total_add = Integer.parseInt(mProducto.getCantidad());
                    // total_badge = Integer.valueOf(carBadge.getText().toString());
                    total_badge = MainActivity.notificationCountCart;
                    //total = total_add + total_db;

                    total = total_db + 1;

                    Log.e("agregar-producto", String.valueOf(total));

                    //MainActivity.notificationCountCart = total_badge - total_db + total;

                    MainActivity.notificationCountCart = total_badge +1;

                    MainActivity.setBadge(MainActivity.notificationCountCart);

                    mProducto.setCantidad(Integer.toString(total));
                    mProducto.setId(dbProducto.getId());
                    v_db_helper.updateProd(mProducto);
                    Toast.makeText(context, "Producto Agregado al Pedido", Toast.LENGTH_SHORT).show();
                }

                //holder.cantidad.setText("0");

            } else {
                Toast.makeText(context, "Debe Introducir la cantidad del producto", Toast.LENGTH_SHORT).show();
                holder.cantidad.requestFocus();
            }
        }

    }

    private void showPopupMenu( View view, Integer co_combo){

        // inflate menu PopupMenu
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.actions_productos, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(view,co_combo));
        popup.show();

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 :items.size();
    }


     /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(Productos p) {
        items.add(p);
        notifyItemInserted(items.size() - 1);
    }

    public void addAll(List<Productos> items) {
        for (Productos item : items) {
            add(item);
        }
    }

    public void remove(Productos p) {
        int position = items.indexOf(p);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        //isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public Productos getItem(int position) {
        return items.get(position);
    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String searchString = charSequence.toString();

                List<Productos> filteredList = new ArrayList<>();


                if (searchString.isEmpty()) {
                    filteredList = productos;

                } else {

                    for (Productos p : productos) {

                        if (p.getNombre().toLowerCase().contains(searchString.toLowerCase())) {
                            filteredList.add(p);
                            Log.e(TAG,"Productos:"+p.getNombre());

                        }
                    }

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                items = (ArrayList<Productos>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public Integer getCountFilter() {
        return countFilter;
    }


    protected class ProductosViewHolder extends RecyclerView.ViewHolder{

        public ImageView imagen;
        public TextView nombre;
        public TextView unidad;
        public TextView precio;
        public TextView moneda;
        public ImageView btnDown;
        public ImageView btnUp;
        public View btnAgregar;
        public EditText cantidad;
        //public TextView detalle;
        public LinearLayout ly_detalle;
        public TextView precio_und;
        public ImageButton imagenBotton;


        public ProductosViewHolder(final View itemView) {
            super(itemView);

            nombre =  itemView.findViewById(R.id.pr_nombre);
            unidad = itemView.findViewById(R.id.pr_unidad);
            precio =  itemView.findViewById(R.id.pr_precio);
            imagen =  itemView.findViewById(R.id.pr_imagen);
            moneda =  itemView.findViewById(R.id.pr_moneda);
            btnDown =  itemView.findViewById(R.id.btn_down);
            btnUp =  itemView.findViewById(R.id.btn_up);
            cantidad =  itemView.findViewById(R.id.tx_cantidad);
            ly_detalle = itemView.findViewById(R.id.pr_ly_datalle);
            precio_und = itemView.findViewById(R.id.pr_tx_precio_und);
            imagenBotton =  itemView.findViewById(R.id.imageButton);

            ly_detalle.setVisibility(View.GONE);
            imagenBotton.setVisibility(View.GONE);

        }

    }

}

class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
    private Integer co_combo;
    private Context context;

    public MyMenuItemClickListener(View view, Integer co_combo) {
        this.co_combo=co_combo;
        this.context = view.getContext();
    }
    @Override public boolean onMenuItemClick(MenuItem menuItem) {

       String TAG = "PUERTOMAR";

         switch (menuItem.getItemId())
         { case R.id.verProductos:

             Bundle parametros = new Bundle();

             parametros.putInt("co_combo", this.co_combo);

             Log.e(TAG, "Combo Recibido: " + co_combo);

             ComboFragment frag = new ComboFragment();

             frag.setArguments(parametros);

             ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                     .replace(R.id.fragment_content, frag,"combo")
                     .addToBackStack(null)
                     .commit();

             return true;
             default:
         }
         return false;
    }
}