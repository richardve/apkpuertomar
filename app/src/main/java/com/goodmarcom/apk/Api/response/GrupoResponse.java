package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Grupos;


import java.util.ArrayList;



public class GrupoResponse {

    private boolean productos;

    private SuccessGrupo success;


    public boolean isProductos() {
        return productos;
    }

    public void setProductos(boolean productos) {
        this.productos = productos;
    }

    public SuccessGrupo getSuccess() {
        return success;
    }

    public void setSuccess(SuccessGrupo success) {
        this.success = success;
    }

    public ArrayList<Grupos> getGrupos(){

        return success.getGrupos().getData();
    }

    public Integer getTotalPage(){

        return  success.getGrupos().getLast_page();

    }

}

class SuccessGrupo {

    private DatosPagProd grupos;

    private String sort;

    private String order;

    private String search;


    public DatosPagProd getGrupos() {
        return grupos;
    }

    public void setProductos(DatosPagProd grupos) {
        this.grupos = grupos;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

}

class DatosPagProd {

    private Integer current_page;

    private ArrayList<Grupos> data;

    private String first_page_url;

    private Integer from;

    private Integer last_page;

    private String last_page_url;

    private String next_page_url;

    private String path;

    private Integer per_page;

    private String prev_page_url;

    private Integer to;


    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public ArrayList<Grupos> getData() {
        return data;
    }

    public void setData(ArrayList<Grupos> data) {
        this.data = data;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public void setFirst_page_url(String first_page_url) {
        this.first_page_url = first_page_url;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getLast_page() {
        return last_page;
    }

    public void setLast_page(Integer last_page) {
        this.last_page = last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public void setLast_page_url(String last_page_url) {
        this.last_page_url = last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPer_page() {
        return per_page;
    }

    public void setPer_page(Integer per_page) {
        this.per_page = per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

}