package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Pedidos;

import java.lang.reflect.Array;
import java.util.ArrayList;



public class PedidosResponse {

    private boolean pedidos;

    private SuccessPedidos success;

    private String msg;

    private Array error;


    public boolean isPedido() {  return pedidos;   }

    public void setPedido(boolean pedidos) {   this.pedidos = pedidos;    }

    public SuccessPedidos getSuccess() {  return success;    }

    public void setSuccess(SuccessPedidos success) {  this.success = success;    }

    public ArrayList<Pedidos> getPedidos(){

        return success.getPedidos().getData();
    }

    public Integer getTotalPage(){

        return  success.getPedidos().getLast_page();

    }

}

class SuccessPedidos {

    private DatosPagPedidos pedidos;

    private String sort;

    private String order;

    private String search;

    public DatosPagPedidos getPedidos() {
        return pedidos;
    }

    public void setPedidos(DatosPagPedidos pedidos) {
        this.pedidos = pedidos;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}

class DatosPagPedidos {

    private Integer current_page;

    private ArrayList<Pedidos> data;

    private String first_page_url;

    private Integer from;

    private Integer last_page;

    private String last_page_url;

    private String next_page_url;

    private String path;

    private Integer per_page;

    private String prev_page_url;

    private Integer to;

    public Integer getCurrent_page() {
        return current_page;
    }

    public ArrayList<Pedidos> getData() {
        return data;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public Integer getFrom() {
        return from;
    }

    public Integer getLast_page() {
        return last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public String getPath() {
        return path;
    }

    public Integer getPer_page() {
        return per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public Integer getTo() {
        return to;
    }
}