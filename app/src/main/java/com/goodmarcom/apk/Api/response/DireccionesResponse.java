package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Direcciones;

import java.util.ArrayList;

public class DireccionesResponse {

   private boolean cliente_direccion;

   private SuccessDirecciones success;


    public boolean isCliente_direccion() {
        return cliente_direccion;
    }

    public void setCliente_direccion(boolean cliente_direccion) {
        this.cliente_direccion = cliente_direccion;
    }

    public SuccessDirecciones getSuccess() {
        return success;
    }

    public void setSuccess(SuccessDirecciones success) {
        this.success = success;
    }

    public ArrayList<Direcciones> getClienteDireccion() {

        return getSuccess().getCliente_direccion();
    }

}



class SuccessDirecciones {

    private ArrayList<Direcciones> cliente_direccion;


    public ArrayList<Direcciones> getCliente_direccion() {
        return cliente_direccion;
    }

    public void setCliente_direccion(ArrayList<Direcciones> cliente_direccion) {
        this.cliente_direccion = cliente_direccion;
    }
}

