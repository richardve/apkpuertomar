package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Pais;

import java.util.ArrayList;

public class PaisesResponse {

        private boolean paises;

        private SuccessPais success;


    public boolean isPaises() {
        return paises;
    }

    public void setPaises(boolean paises) {
        this.paises = paises;
    }

    public SuccessPais getSuccess() {
        return success;
    }

    public void setSuccess(SuccessPais success) {
        this.success = success;
    }

    public ArrayList<Pais> getPaises() {
        return getSuccess().getPais();
    }
}



class SuccessPais {

    private ArrayList<Pais> pais;

    public ArrayList<Pais> getPais() {
        return pais;
    }

    public void setPais(ArrayList<Pais> pais) {
        this.pais = pais;
    }
}