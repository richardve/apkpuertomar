package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Ciudad;

import java.util.ArrayList;

public class CiudadesResponse {

    private boolean ciudades;

    private SuccessCiudad success;


    public boolean isCiudades() {
        return ciudades;
    }

    public void setCiudades(boolean ciudades) {
        this.ciudades = ciudades;
    }

    public SuccessCiudad getSuccess() {
        return success;
    }

    public void setSuccess(SuccessCiudad success) {
        this.success = success;
    }

    public ArrayList<Ciudad> getCiudades() {
        return getSuccess().getCiudades();
    }
}

class SuccessCiudad {

    private ArrayList<Ciudad> ciudades;

    public ArrayList<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(ArrayList<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }
}