package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Clientes;

import java.util.ArrayList;

public class PerfilResponse {

    private boolean perfil;

    private String success;

    private ArrayList<Clientes> myperfil;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public boolean isPerfil() {
        return perfil;
    }

    public void setPerfil(boolean perfil) {
        this.perfil = perfil;
    }

    public ArrayList<Clientes> getMyperfil() {
        return myperfil;
    }

    public void setMyperfil(ArrayList<Clientes> myperfil) {
        this.myperfil = myperfil;
    }
}