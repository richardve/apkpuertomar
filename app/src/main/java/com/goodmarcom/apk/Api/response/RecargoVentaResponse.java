package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.RecargoVenta;

import java.util.ArrayList;

public class RecargoVentaResponse {

    private boolean recargo;

    private ArrayList<RecargoVenta> success;

    public boolean isRecargo() {
        return recargo;
    }

    public void setRecargo(boolean recargo) {
        this.recargo = recargo;
    }

    public ArrayList<RecargoVenta>  getSuccess() {
        return success;
    }

    public void setSuccess(ArrayList<RecargoVenta>  success) {
        this.success = success;
    }

    public String getPorcentaje(int posicion) {
        return getSuccess().get(posicion).getNu_porcentaje();
    }

}



