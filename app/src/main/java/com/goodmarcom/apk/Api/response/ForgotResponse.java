package com.goodmarcom.apk.Api.response;

import com.google.gson.annotations.SerializedName;

public class ForgotResponse {

    private boolean forgot;
    @SerializedName("msg")
    private String success;


    public boolean isForgot() {
        return forgot;
    }

    public void setForgot(boolean forgot) {
        this.forgot = forgot;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
