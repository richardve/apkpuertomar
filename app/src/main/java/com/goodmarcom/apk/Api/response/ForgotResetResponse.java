package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Clientes;

import java.util.ArrayList;

public class ForgotResetResponse {

    private boolean reset;

    private SuccessReset success;


    public boolean isReset() {
        return reset;
    }

    public void setReset(boolean reset) {
        this.reset = reset;
    }


    public SuccessReset getSuccess() {
        return success;
    }

    public void setSuccess(SuccessReset success) {
        this.success = success;
    }

    public ArrayList<Clientes> getPerfil(){

        return getSuccess().getPerfil();

    }

    public String getToken() {
        return getSuccess().getToken();
    }



}


class SuccessReset {

    private String token;

    private ArrayList<Clientes> perfil;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<Clientes> getPerfil() {
        return perfil;
    }

    public void setPerfil(ArrayList<Clientes> perfil) {
        this.perfil = perfil;
    }


}
