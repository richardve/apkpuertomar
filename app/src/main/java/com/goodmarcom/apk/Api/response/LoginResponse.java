package com.goodmarcom.apk.Api.response;


import com.goodmarcom.apk.Enty.Clientes;

import java.util.ArrayList;



public class LoginResponse {

    private boolean login;

    private SuccessLogin success;


    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public SuccessLogin getSucces() {
        return success;
    }

    public void setSucces(SuccessLogin succes) {
        this.success = succes;
    }


    public ArrayList<Clientes> getPerfil(){

        return success.getPerfil();

    }

    public String getToken() {
        return success.getToken();
    }


}


class SuccessLogin {

   private String token;

   private ArrayList<Clientes> perfil;


   public String getToken() {
       return token;
   }

   public void setToken(String token) {
       this.token = token;
   }

   public ArrayList<Clientes> getPerfil() {
       return perfil;
   }

   public void setPerfil(ArrayList<Clientes> perfil) {
       this.perfil = perfil;
   }
}

