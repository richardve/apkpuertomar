package com.goodmarcom.apk.Api.response;

import com.google.gson.annotations.SerializedName;

public class ForgotPinResponse {

    private boolean pin;
    @SerializedName("msg")
    private String success;

    public boolean isPin() {
        return pin;
    }

    public void setPin(boolean pin) {
        this.pin = pin;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
