package com.goodmarcom.apk.Api.response;

import com.google.gson.annotations.SerializedName;

public class GuardarPedidosResponse {

    private boolean pedidos;
    @SerializedName("msg")
    private String success;
    private Integer co_pedido;
    private Integer co_pago;
    private Double nu_porc;
    private boolean confirmar_pago;

    public boolean isPedidos() {
        return pedidos;
    }

    public void setPedidos(boolean pedidos) {
        this.pedidos = pedidos;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Integer getCo_pedido() {
        return co_pedido;
    }

    public void setCo_pedido(Integer co_pedido) {
        this.co_pedido = co_pedido;
    }

    public Integer getCo_pago() {
        return co_pago;
    }

    public void setCo_pago(Integer co_pago) {
        this.co_pago = co_pago;
    }


    public Double getNu_porc() {
        return nu_porc;
    }

    public void setNu_porc(Double nu_porc) {
        this.nu_porc = nu_porc;
    }

    public boolean isConfirmarPago() {
        return confirmar_pago;
    }

    public void setConfirmar_pago(boolean confirmar_pago) {
        this.confirmar_pago = confirmar_pago;
    }
}
