package com.goodmarcom.apk.Api.response;

import com.google.gson.annotations.SerializedName;

public class DireccionesGuardarResponse {
    private boolean cliente_direccion;

    @SerializedName("msg")
    private String success;

    private  Integer co_direccion;


    public boolean isCliente_direccion() {
        return cliente_direccion;
    }

    public void setCliente_direccion(boolean cliente_direccion) {
        this.cliente_direccion = cliente_direccion;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Integer getCo_direccion() {
        return co_direccion;
    }

    public void setCo_direccion(Integer co_direccion) {
        this.co_direccion = co_direccion;
    }
}
