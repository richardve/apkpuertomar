package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Clientes;

import java.util.ArrayList;

public class RegisterResponse {

    private boolean register;

    private String success;

    private String token;

    private ArrayList<Clientes> perfil;

    public boolean isRegister() {  return register;   }

    public void setRegister(boolean register) {   this.register = register;    }

    public String getSuccess() {  return success;    }

    public void setSuccess(String success) {  this.success = success;    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<Clientes> getPerfil() {
        return perfil;
    }

    public void setPerfil(ArrayList<Clientes> perfil) {
        this.perfil = perfil;
    }

}
