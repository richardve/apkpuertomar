package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.TipoPago;

import java.util.ArrayList;

public class TipoPagoResponse {

    private boolean tipo_pago;

    private SuccessTipoPago success;


    public boolean isTipo_pago() {
        return tipo_pago;
    }

    public void setTipo_pago(boolean tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    public SuccessTipoPago getSuccess() {
        return success;
    }

    public void setSuccess(SuccessTipoPago success) {
        this.success = success;
    }


    public ArrayList<TipoPago> getTipoPagos() {
        return getSuccess().getTipo_pago();
    }

}




class SuccessTipoPago {

    private ArrayList<TipoPago> tipo_pago;


    public ArrayList<TipoPago> getTipo_pago() {
        return tipo_pago;
    }

    public void setTipo_pago(ArrayList<TipoPago> tipo_pago) {
        this.tipo_pago = tipo_pago;
    }
}
