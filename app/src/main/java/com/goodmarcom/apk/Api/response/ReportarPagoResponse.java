package com.goodmarcom.apk.Api.response;

public class ReportarPagoResponse {

    private boolean pago;
    private String success;

    public boolean isPago() {
        return pago;
    }

    public void setPago(boolean pago) {
        this.pago = pago;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
