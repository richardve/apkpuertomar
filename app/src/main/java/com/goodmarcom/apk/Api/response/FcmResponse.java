package com.goodmarcom.apk.Api.response;

import com.google.gson.annotations.SerializedName;

public class FcmResponse {

   private boolean fcm;

   @SerializedName("msg")
   private String success;


    public boolean isFcm() {
        return fcm;
    }

    public void setFcm(boolean fcm) {
        this.fcm = fcm;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
