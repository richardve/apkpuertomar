package com.goodmarcom.apk.Api.response;

public class OptarCreditoResponse {

    private boolean creditos;

    private String success;


    public boolean isCreditos() {
        return creditos;
    }

    public void setCreditos(boolean creditos) {
        this.creditos = creditos;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
