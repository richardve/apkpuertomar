package com.goodmarcom.apk.Api.response;

public class CondicionesUsoResponse {

    private boolean condiciones;

    private String success;


    public boolean isCondiciones() {
        return condiciones;
    }

    public void setCondiciones(Boolean condiciones) {
        this.condiciones = condiciones;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }


}
