package com.goodmarcom.apk.Api;

import com.goodmarcom.apk.Api.response.CiudadesResponse;
import com.goodmarcom.apk.Api.response.CombosResponse;
import com.goodmarcom.apk.Api.response.CondicionesUsoResponse;
import com.goodmarcom.apk.Api.response.CuentaEntidadResponse;
import com.goodmarcom.apk.Api.response.DeudasResponse;
import com.goodmarcom.apk.Api.response.DireccionesGuardarResponse;
import com.goodmarcom.apk.Api.response.DireccionesResponse;
import com.goodmarcom.apk.Api.response.EmpresaResponse;
import com.goodmarcom.apk.Api.response.FcmResponse;
import com.goodmarcom.apk.Api.response.ForgotPinResponse;
import com.goodmarcom.apk.Api.response.ForgotResetResponse;
import com.goodmarcom.apk.Api.response.ForgotResponse;
import com.goodmarcom.apk.Api.response.GeneralResponse;
import com.goodmarcom.apk.Api.response.GrupoResponse;
import com.goodmarcom.apk.Api.response.GuardarPedidosResponse;
import com.goodmarcom.apk.Api.response.LoginResponse;
import com.goodmarcom.apk.Api.response.OptarCreditoResponse;
import com.goodmarcom.apk.Api.response.PaisesResponse;
import com.goodmarcom.apk.Api.response.PedidosResponse;
import com.goodmarcom.apk.Api.response.PerfilResponse;
import com.goodmarcom.apk.Api.response.PoliticasPrivacidadResponse;
import com.goodmarcom.apk.Api.response.ProductosResponse;
import com.goodmarcom.apk.Api.response.RecargoVentaResponse;
import com.goodmarcom.apk.Api.response.RecetasResponse;
import com.goodmarcom.apk.Api.response.RegisterResponse;
import com.goodmarcom.apk.Api.response.ReportarPagoResponse;
import com.goodmarcom.apk.Api.response.SliderCatalogoResponse;
import com.goodmarcom.apk.Api.response.SliderPortadaResponse;
import com.goodmarcom.apk.Api.response.TipoPagoResponse;


import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface PuertomarApiServices {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> postLogin(
            @Field("usuario") String usuario,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("register")
    Call<RegisterResponse> postRegister(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("empresa")
    Call<EmpresaResponse> postRegisterEmpresa(
            @Body RequestBody perfil,
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("pedidos")
    Call<PedidosResponse> getPedidos(
            @Header("Authorization") String auth,
            @Query("page") Integer page,
            @Query("search") String search
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("pedidos")
    Call<PedidosResponse> getPedidosPagos(
            @Header("Authorization") String auth,
            @Query("page") Integer page,
            @Query("tienepago") String search
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("pedidos")
    Call<GuardarPedidosResponse> guardarPedidos(
            @Header("Authorization") String auth,
            @Body RequestBody productos
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @DELETE("pedidos/{id}")
    Call<PedidosResponse> deletePedidos(
            @Header("Authorization") String auth,
            @Path("id") String id
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("productos_combo/{ciudad}/{combo}")
    Call<CombosResponse> getProductosCombo(
            @Path("ciudad") String ciudad,
            @Path("combo") Integer combo,
            @Query("search") String search,
            @Query("page") Integer page,
            @Header("Authorization") String auth
    );

   @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("productos_filter/{ciudad}/{grupo}")
    Call<ProductosResponse> getProductos(
            @Path("ciudad") String ciudad,
            @Path("grupo") Integer grupo,
            @Query("search") String search,
            @Query("page") Integer page,
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("productos/{ciudad}")
    Call<GrupoResponse> getGrupos(
            @Path("ciudad") String ciudad,
            @Query("search") String search,
            @Query("page") Integer page,
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("recetas")
    Call<RecetasResponse> getRecetas(
            @Query("page") Integer page,
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("imagenes_catalogo")
    Call<SliderCatalogoResponse> getImgSliderCatalogo(
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("imagenes_portada")
    Call<SliderPortadaResponse> getImgSliderPortada();

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("general")
    Call<GeneralResponse> getDatos();


    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("perfil")
    Call<PerfilResponse> actualizarPerfil(
            @Body RequestBody perfil,
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("perfil")
    Call<PerfilResponse> getPerfil(
            @Header("Authorization") String auth
    );

    @FormUrlEncoded
    @POST("forgot")
    Call<ForgotResponse> postForgot(
            @Field("usuario") String usuario
    );

    @FormUrlEncoded
    @POST("forgot/pin")
    Call<ForgotPinResponse> postForgotPin(
            @Field("usuario") String usuario,
            @Field("pin") String pin
    );

    @FormUrlEncoded
    @POST("forgot/reset")
    Call<ForgotResetResponse> postForgotReset(
            @Field("usuario") String usuario,
            @Field("password") String password
    );


    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("politicas")
    Call<PoliticasPrivacidadResponse> getPoliticasPrivacidad(
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("condiciones")
    Call<CondicionesUsoResponse> getCondiconesUso(
            @Header("Authorization") String auth
    );


    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("creditos")
    Call<OptarCreditoResponse> getOptarCreditos(
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("cuenta_entidad")
    Call<CuentaEntidadResponse> getCuentaEntidad(
            @Header("Authorization") String auth
    );

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET("cliente_direccion")
    Call<DireccionesResponse> getDirecciones(
            @Header("Authorization") String auth
    );

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @DELETE("cliente_direccion/{id_direccion}")
    Call<DireccionesResponse> deleteDireccion(
            @Header("Authorization") String auth,
            @Path("id_direccion") Integer id_direccion
    );

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("cliente_direccion")
    Call<DireccionesGuardarResponse> guardarDireccion(
            @Header("Authorization") String auth,
            @Body RequestBody direccion
    );

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    //@Headers({"Content-Type: application/json;charset=UTF-8"})
    @PUT("cliente_direccion/{id_direccion}")
    Call<DireccionesResponse> updateDireccion(
                @Path("id_direccion") Integer id_direccion,
                @Header("Authorization") String auth,
                @Body RequestBody direccion);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET("tipo_pago/{id_forma_pago}")
    Call<TipoPagoResponse> getTipoPagos(
            @Header("Authorization") String auth,
            @Path("id_forma_pago") String id_forma_pago);


    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("pagos/{id_pago_pedido}")
    Call<ReportarPagoResponse> reportarPago(
            @Header("Authorization") String auth,
            @Path("id_pago_pedido") Integer id_pago_pedido,
            @Body RequestBody datosPago
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("ciudades/{id_pais}")
    Call<CiudadesResponse> getCiudades(
            @Path("id_pais") String id_pais,
            @Query("search") String search,
            @Header("Authorization") String auth
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("ciudades/{id_pais}")
    Call<CiudadesResponse> getCiudades(
            @Path("id_pais") String id_pais,
            @Query("search") String search
    );


    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("paises")
    Call<PaisesResponse> getPaises(
            @Query("search") String search
    );

    @FormUrlEncoded
    @POST("fcm")
    Call<FcmResponse> postFcm(
            @Header("Authorization") String auth,
            @Field("token") String token
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("deudas")
    Call<DeudasResponse> getDeudas(
            @Header("Authorization") String auth
           /* @Query("page") Integer page,
            @Query("search") String search*/
    );

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("recargos")
    Call<RecargoVentaResponse> getPorcentajeRecargo(
            @Header("Authorization") String auth
    );


}
