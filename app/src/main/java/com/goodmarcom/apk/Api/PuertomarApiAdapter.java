package com.goodmarcom.apk.Api;


import com.goodmarcom.apk.Utils.Globals;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PuertomarApiAdapter {

        private static PuertomarApiServices API_SERVICE;

        public static PuertomarApiServices getApiService() {

            // Creamos un interceptor y le indicamos el log level a usar
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            // Asociamos el interceptor a las peticiones
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                                .callTimeout(2, TimeUnit.MINUTES)
                                .connectTimeout(60, TimeUnit.SECONDS)
                                .readTimeout(2, TimeUnit.MINUTES)
                                .writeTimeout(2, TimeUnit.MINUTES)
                                .addInterceptor(logging);


            //String baseUrl = "http://45.177.127.112/api/";

            //String baseUrl = "http://192.168.0.117/api/";

            final Globals globalVariable = new Globals();

            String baseUrl = globalVariable.SERVER_URL();



            if (API_SERVICE == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(httpClient.build()) // <-- usamos el log level
                        .build();
                API_SERVICE = retrofit.create(PuertomarApiServices.class);
            }

            return API_SERVICE;
        }


}
