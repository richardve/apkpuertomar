package com.goodmarcom.apk.Api.response;



import com.goodmarcom.apk.Enty.Ciudad;
import com.goodmarcom.apk.Enty.Pais;

import java.util.ArrayList;

public class GeneralResponse {


    private boolean general;

    private SuccessGeneral success;

    public boolean isGeneral() {
        return general;
    }

    public void setGeneral(boolean general) {
        this.general = general;
    }

    public SuccessGeneral getSuccess() {
        return success;
    }

    public void setSuccess(SuccessGeneral success) {
        this.success = success;
    }

    public ArrayList<Pais> getPais(){
       return this.success.getPais();
    }

    public ArrayList <String> getPaises(){

        ArrayList<String> paises = new ArrayList<String>();

        ArrayList<Pais> pais = this.success.getPais();


        for( Pais p : pais ){

            paises.add(p.getTx_pais());

        }

        return paises;
    }

    public ArrayList <Ciudad> getCiudades(Integer codigo){

        ArrayList<String> ciudades = new ArrayList<String>();

        ArrayList<Pais> paises = this.success.getPais();

        Pais pais = new Pais();

        Integer codPais=0;

        for(Pais p : paises){

            if(p.getCo_pais().equals(codigo)){
                pais = p;
            };
        }

        //paises.get(codPais).getCiudades()

        return pais.getCiudades();
    }


}


class SuccessGeneral {

    private ArrayList<Pais> pais;


    public ArrayList<Pais> getPais() {
        return pais;
    }

    public void setPais(ArrayList<Pais> pais) {
        this.pais = pais;
    }



}
