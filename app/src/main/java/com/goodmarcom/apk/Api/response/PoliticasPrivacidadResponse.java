package com.goodmarcom.apk.Api.response;

public class PoliticasPrivacidadResponse {

    private boolean politicas;

    private String success;


    public boolean isPoliticas() {
        return politicas;
    }

    public void setPoliticas(boolean politicas) {
        this.politicas = politicas;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
