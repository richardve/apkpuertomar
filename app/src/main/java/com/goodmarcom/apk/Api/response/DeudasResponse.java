package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Deudas;

import java.util.ArrayList;

public class DeudasResponse {

    private boolean deudas;

    private SuccessDeuda success;


    public boolean isDeudas() {
        return deudas;
    }

    public void setDeudas(boolean deudas) {
        this.deudas = deudas;
    }

    public SuccessDeuda getSuccess() {
        return success;
    }

    public void setSuccess(SuccessDeuda success) {
        this.success = success;
    }


    public ArrayList<Deudas> getDeudas(){

        return success.getDeudas().getData();
    }

    public String getDeuda_total() {
        return success.getDeuda_total();
    }

    public String getMonto_total() {
        return success.getMonto_total();
    }


}



class SuccessDeuda {

    private  String deuda_total;

    private String monto_total;

    private DatosPagDeudas deudas;


    public String getDeuda_total() {
        return deuda_total;
    }

    public void setDeuda_total(String deuda_total) {
        this.deuda_total = deuda_total;
    }

    public String getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(String monto_total) {
        this.monto_total = monto_total;
    }

    public DatosPagDeudas getDeudas() {
        return deudas;
    }

    public void setDeudas(DatosPagDeudas deudas) {
        this.deudas = deudas;
    }
}


class DatosPagDeudas {

    private Integer current_page;

    private ArrayList<Deudas> data;

    private String first_page_url;

    private Integer from;

    private Integer last_page;

    private String last_page_url;

    private String next_page_url;

    private String path;

    private Integer per_page;

    private String prev_page_url;

    private Integer to;

    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public ArrayList<Deudas> getData() {
        return data;
    }

    public void setData(ArrayList<Deudas> data) {
        this.data = data;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public void setFirst_page_url(String first_page_url) {
        this.first_page_url = first_page_url;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getLast_page() {
        return last_page;
    }

    public void setLast_page(Integer last_page) {
        this.last_page = last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public void setLast_page_url(String last_page_url) {
        this.last_page_url = last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPer_page() {
        return per_page;
    }

    public void setPer_page(Integer per_page) {
        this.per_page = per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }
}