package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.CuentaEntidad;

import java.util.ArrayList;

public class CuentaEntidadResponse {

    private boolean cuenta_entidad;

    private SuccessCuentaEntidad success;

    public boolean isCuenta_entidad() {
        return cuenta_entidad;
    }

    public void setCuenta_entidad(boolean cuenta_entidad) {
        this.cuenta_entidad = cuenta_entidad;
    }

    public SuccessCuentaEntidad getSuccess() {
        return success;
    }

    public void setSuccess(SuccessCuentaEntidad success) {
        this.success = success;
    }

    public ArrayList<CuentaEntidad>  getCuentaEntidad(){

        return getSuccess().getCuenta_entidad();
    }
}


class SuccessCuentaEntidad {

    private ArrayList<CuentaEntidad> cuenta_entidad;

    public ArrayList<CuentaEntidad> getCuenta_entidad() {
        return cuenta_entidad;
    }

    public void setCuenta_entidad(ArrayList<CuentaEntidad> cuenta_entidad) {
        this.cuenta_entidad = cuenta_entidad;
    }
}