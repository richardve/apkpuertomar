package com.goodmarcom.apk.Api.response;


import com.goodmarcom.apk.Enty.Recetas;

import java.util.ArrayList;



public class RecetasResponse {

    private boolean recetas;

    private SuccessReceta success;


    public boolean isRecetas() {   return recetas;   }

    public void setRecetas(boolean recetas) {   this.recetas = recetas;  }

    public SuccessReceta getSucces() { return success; }

    public void setSucces(SuccessReceta succes) { this.success = succes;   }

    public ArrayList<Recetas> getRecetas(){

        return success.getRecetas().getData();

    }

    public Integer getTotalPage(){

        return success.getRecetas().getLast_page();

    }


}


class SuccessReceta{

    private DatosPagRecetas recetas;

    private String sort;

    private String order;

    private String search;


    public DatosPagRecetas getRecetas() {   return recetas;   }

    public void setRecetas(DatosPagRecetas recetas) {  this.recetas = recetas;  }

    public String getSort() { return sort;  }

    public void setSort(String sort) {  this.sort = sort;  }

    public String getOrder() {  return order;   }

    public void setOrder(String order) {   this.order = order;   }

    public String getSearch() {  return search;   }

    public void setSearch(String search) { this.search = search; }
}


class DatosPagRecetas {

    private Integer current_page;

    private ArrayList<Recetas> data;

    private String first_page_url;

    private Integer from;

    private Integer last_page;

    private String last_page_url;

    private String next_page_url;

    private String path;

    private Integer per_page;

    private String prev_page_url;

    private Integer to;


    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public ArrayList<Recetas> getData() {
        return data;
    }

    public void setData(ArrayList<Recetas> data) {
        this.data = data;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public void setFirst_page_url(String first_page_url) {
        this.first_page_url = first_page_url;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getLast_page() {
        return last_page;
    }

    public void setLast_page(Integer last_page) {
        this.last_page = last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public void setLast_page_url(String last_page_url) {
        this.last_page_url = last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPer_page() {
        return per_page;
    }

    public void setPer_page(Integer per_page) {
        this.per_page = per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }


}

