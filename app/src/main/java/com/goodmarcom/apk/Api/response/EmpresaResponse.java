package com.goodmarcom.apk.Api.response;

import com.goodmarcom.apk.Enty.Clientes;

import java.util.ArrayList;

public class EmpresaResponse {

    private boolean empresa;

    private String success;

    private String token;

    private ArrayList<Clientes> perfil;


    public String getSuccess() {  return success;    }

    public void setSuccess(String success) {  this.success = success;    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<Clientes> getPerfil() {
        return perfil;
    }

    public void setPerfil(ArrayList<Clientes> perfil) {
        this.perfil = perfil;
    }

    public boolean isEmpresa() {
        return empresa;
    }

    public void setEmpresa(boolean empresa) {
        this.empresa = empresa;
    }
}
