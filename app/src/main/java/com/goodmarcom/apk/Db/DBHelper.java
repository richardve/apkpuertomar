package com.goodmarcom.apk.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.goodmarcom.apk.Enty.Productos;

import java.util.ArrayList;
import java.util.List;



public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = "PUERTOMAR";
    private String tableName = "car_productos";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    public DBHelper(Context context) {

        super(context,context.getSharedPreferences("session",
                                Context.MODE_PRIVATE).getString("usuario","puertomar"),
                null, 6);
    }


    @Override
    public void onCreate(SQLiteDatabase db_app) {
        StringBuilder v_query_builder = new StringBuilder();
        Log.e("Create-table-database",tableName);
        v_query_builder.append("CREATE TABLE "+tableName+" (")
                .append("id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ")
                .append("co_producto INTEGER NOT NULL,")
                .append("nombre TEXT NOT NULL,")
                .append("unidad TEXT NOT NULL,")
                .append("valor TEXT NOT NULL,")
                .append("imagen TEXT NOT NULL,")
                .append("precio TEXT NOT NULL,")
                .append("cantidad TEXT NOT NULL);");

        db_app.execSQL(v_query_builder.toString());
        v_query_builder.setLength(0);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        android.util.Log.v("BBDD",
                "BD actualizándose. Se perderán los datos antiguos");

        //para borrar las tablas existentes cuando existe un cambio el la estructura de la base de datos
        db.execSQL("DROP TABLE IF EXISTS " + tableName + " ;");
        onCreate(db);
    }


    public long insertProd (Productos productos) throws Exception
    {
        //obtiene la referencia de la base de datos para poder escribir

        SQLiteDatabase v_db = this.getWritableDatabase();

        //preparar los datos que se van a guardar

        ContentValues v_content = new ContentValues();

        v_content.put("co_producto",productos.getCo_producto());
        v_content.put("nombre",productos.getNombre());
        v_content.put("unidad",productos.getUnidad());
        v_content.put("valor",productos.getValor());
        v_content.put("imagen",productos.getImagen());
        v_content.put("precio", productos.getPrecio());
        v_content.put("cantidad", productos.getCantidad());

        Log.e(TAG, " unidad " + productos.getUnidad());
        Log.e(TAG,"valor"+productos.getValor());


        long v_inserted_id = 0;

        //agrega los datos en la tabla

        try {
            v_inserted_id = v_db.insertOrThrow(tableName,null,v_content);

            Log.e(TAG, " Insertó " + v_inserted_id);

            v_db.close();

        }
        catch (SQLException ex)
        {
            v_db.close();

            throw ex;

        }


        return v_inserted_id;


    }


    public boolean updateProd (Productos productos)
    {
        //obtiene la referencia de la base de datos para poder escribir

        SQLiteDatabase v_db = this.getWritableDatabase();

        //preparar los datos que se van a guardar

        ContentValues v_content = new ContentValues();

        v_content.put("co_producto",productos.getCo_producto());
        v_content.put("nombre",productos.getNombre());
        if(!productos.getTx_unidad().isEmpty()) {
            v_content.put("unidad", productos.getTx_unidad());
        }else
        {
            v_content.put("unidad", productos.getUnidad());
        }

        if(!productos.getTx_valor().isEmpty()) {
            v_content.put("valor", productos.getTx_valor());
        }else
        {
            v_content.put("valor", productos.getValor());
        }
        v_content.put("imagen",productos.getImagen());
        v_content.put("precio", productos.getPrecio());
        v_content.put("cantidad", productos.getCantidad());


        //agrega los datos en la tabla

        boolean v_ok = ( 1 == v_db.update(
                tableName, //tabla
                v_content, //nuevos datos
                "id=" + String.valueOf(productos.getId()), //clausula where
                null)); // parameros del where

        v_db.close();

        return v_ok;


    }


    public boolean deleteProd(Long id)
    {

        //obtiene la referencia de la base de datos para poder escribir

        SQLiteDatabase v_db = this.getWritableDatabase();

        //eliminar el registro


        int v_result = v_db.delete(
                tableName, //nombre de la tabla
                "id = ?", // campos de la clausula where
                new String[] {String.valueOf(id)} //parametros de la clausula where
        );


        v_db.close();


        return 1 == v_result;
    }

    public boolean deleteAllProd()
    {

        //obtiene la referencia de la base de datos para poder escribir

        SQLiteDatabase v_db = this.getWritableDatabase();

        //eliminar el registro
        int v_result = v_db.delete(
                tableName, //nombre de la tabla
                null, // campos de la clausula where
                null //parametros de la clausula where
        );


        v_db.close();


        return 1 == v_result;
    }



    public Productos queryProdByNombre(String nombre)
    {

        //ontiene los datos para lectura
        SQLiteDatabase v_db = this.getReadableDatabase();

        //consulta los datos de la mascota

        Cursor v_cursor = v_db.query(
                tableName, //nombre de la tabla
                new String[]{"id","co_producto","nombre","unidad","valor","imagen","precio","cantidad"}, //campos del select
                "nombre ='"+nombre+"'", // where
                null,//new String[]{nombre}, // parametros del where
                null, // group by
                null, // having
                null // order by
        );



        Productos v_productos = new Productos(null,null,null,null,null,null);

        if(null == v_cursor || v_cursor.getCount()<=0)
        {

            v_db.close();

            Log.e(TAG,"resultado null");

            return v_productos;

        }

        // se posiciona sobre el registro obtenido

        v_cursor.moveToFirst();

        v_productos.setId(v_cursor.getLong(0));
        v_productos.setCo_producto(v_cursor.getInt(1));
        v_productos.setNombre(v_cursor.getString(2));
        v_productos.setTx_unidad(v_cursor.getString(3));
        v_productos.setTx_valor(v_cursor.getString(4));
        v_productos.setImagen(v_cursor.getString(5));
        v_productos.setPrecio(v_cursor.getString(6));
        v_productos.setCantidad(v_cursor.getString(7));

        v_cursor.close();
        v_db.close();
        Log.e(TAG,"consultando por nombre");
        return v_productos;

    }


    public List<Productos> queryAllProductos()
    {
        List<Productos> v_productos = new ArrayList<Productos>();


        //ontiene los datos para lectura
        SQLiteDatabase v_db = this.getReadableDatabase();



        try {
            //consulta los datos

            Cursor v_cursor = v_db.query(
                    tableName, //nombre de la tabla
                    new String[]{"id","co_producto", "nombre","unidad","valor", "imagen", "precio", "cantidad"}, //campos del select
                    null, // where
                    null, // parametros del where
                    null, // group by
                    null, // having
                    "id ASC" // order by
            );


            //evalua si se obtuvieron los resultados
            if (null != v_cursor) {
                while (v_cursor.moveToNext()) {
                    Log.e(TAG, String.valueOf(v_cursor.getLong(0)));

                    //inicializa el objeto que guardara los datos
                    Productos v_producto = new Productos(null, null, null, null,null,null);

                    v_producto.setId(v_cursor.getLong(0));
                    v_producto.setCo_producto(v_cursor.getInt(1));
                    v_producto.setNombre(v_cursor.getString(2));
                    v_producto.setTx_unidad(v_cursor.getString(3));
                    v_producto.setTx_valor(v_cursor.getString(4));
                    v_producto.setImagen(v_cursor.getString(5));
                    v_producto.setPrecio(v_cursor.getString(6));
                    v_producto.setCantidad(v_cursor.getString(7));

                    v_productos.add(v_producto);


                }
                v_cursor.close();

            }
        }
         catch (SQLException e)
            {
                Log.e(TAG, e.toString());
                e.printStackTrace();
            }


            return  v_productos;

    }

    public String getTotalProductos(){

        //ontiene los datos para lectura
        SQLiteDatabase v_db = this.getReadableDatabase();

        Cursor v_cursor = v_db.rawQuery("select sum(cantidad)  from "+tableName, null);

        if(v_cursor.moveToFirst()){
            return v_cursor.getString(0);
        }

        return "0";
    }


}
