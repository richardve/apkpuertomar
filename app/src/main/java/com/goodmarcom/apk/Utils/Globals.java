package com.goodmarcom.apk.Utils;

import android.app.Application;

public class Globals extends Application {

    // TODO .- Local
    //private String SERVER_URL = "http://192.168.0.117/api/";
    //private String URL_IMAGEN = "http://192.168.0.117/storage/productos/";

    // TODO .- Desarrollo Server
   //private String SERVER_URL = "http://api.puertomar.tk/api/";
   //private String URL_IMAGEN = "http://api.puertomar.tk/storage/productos/";

    // TODO.- Producción Servidor
    private String SERVER_URL = "http://45.177.127.112/api/";
    private String URL_IMAGEN = "http://45.177.127.112/storage/productos/";

    public String SERVER_URL() {
        return SERVER_URL;
    }

    public String URL_IMAGEN() {
        return URL_IMAGEN;
    }
}
