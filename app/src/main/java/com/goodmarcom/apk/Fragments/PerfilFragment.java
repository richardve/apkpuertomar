package com.goodmarcom.apk.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.goodmarcom.apk.Activity.DatosEmpresaActivity;
import com.goodmarcom.apk.Activity.DatosUsuarioActivity;
import com.goodmarcom.apk.Activity.DireccionActivity;
import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.R;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilFragment extends Fragment {

    //private static final String TAG = "PUERTOMAR";

    private FragmentManager fm;
    private Context context;


    public PerfilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.fragment_perfil, container, false);

        context = myFragmentView.getContext();


        fm = getFragmentManager();


        View mDatosusuario = myFragmentView.findViewById(R.id.pf_user);
        View mDatosEmpresa = myFragmentView.findViewById(R.id.pf_emp);
        View mDirEntrega = myFragmentView.findViewById(R.id.pf_dir);


        Toolbar toolbar_pf =  myFragmentView.findViewById(R.id.toolbar_pf);

        ((MainActivity)Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar_pf);


        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setTitle("Perfil");

        }



        mDatosusuario.setOnClickListener(view -> {


            Intent DatosUsuario = new Intent(context, DatosUsuarioActivity.class);
            startActivity(DatosUsuario);


        });


        mDatosEmpresa.setOnClickListener(view -> {

                Intent DatosEmpresa = new Intent(context, DatosEmpresaActivity.class);

                Bundle parametros = new Bundle();

                parametros.putBoolean("perfil", true);

                DatosEmpresa.putExtras(parametros);

                startActivity(DatosEmpresa);

            });


        mDirEntrega.setOnClickListener(view -> {

            Intent Principal = new Intent(context, DireccionActivity.class);

            startActivity(Principal);

        });


        return myFragmentView;
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item=menu.findItem(R.id.search);

       if(item!=null) item.setVisible(false);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {
            fm.popBackStack();

            return true;
        }
        return super.onOptionsItemSelected(item);


    }

}