package com.goodmarcom.apk.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goodmarcom.apk.Activity.ListCuentaActivity;
import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.ReportarPagoResponse;
import com.goodmarcom.apk.BuildConfig;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.ImageUtils;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.core.content.ContextCompat.checkSelfPermission;

//import static android.support.v4.content.PermissionChecker.checkSelfPermission;


public class ReportarPagoFragment extends Fragment {

    private static final String TAG = "PUERTOMAR";

    private ProgressDialog progressDialog;

    private Context context;

    private String token;

    SharedPreferences v_preferences;

    private TextView mTviewCuentaEntidad;

    private TextView mTxCuenta;

    private Integer mIdCuentaEntidad=-1;

    static final int REQUEST_TAKE_PHOTO = 101;
    static final int REQUEST_GALLERY_PHOTO = 102;
    static final int CAMERA_PERMISSION_REQUEST_CODE = 103;

    private EditText mReferencia;

    private EditText mMonto;

    private TextView mTviewRecibo;

    private String currentPhotoPath;

    private String imageBase64="";

    private ImageView mRecibo;

    private Integer mIdPagoPedido;

    private Double totalPedido;

    private Double mNuPorc;

    private Uri photoURI;



    public ReportarPagoFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.fragment_reportar_pago, container, false);


        context = myFragmentView.getContext();


        Toolbar toolbar_rep_pago = myFragmentView.findViewById(R.id.toolbar_rep_pago);

        ((MainActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar_rep_pago);


        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setTitle("Reportar Pago");

        }


        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );


        mReferencia = myFragmentView.findViewById(R.id.rep_pago_referencia);
        mMonto = myFragmentView.findViewById(R.id.rep_pago_monto);
        mTxCuenta = myFragmentView.findViewById(R.id.rep_tx_cuenta);
        mTviewCuentaEntidad =  myFragmentView.findViewById(R.id.rep_pago_cuenta);
        mTviewRecibo = myFragmentView.findViewById(R.id.rep_recibo);
        mRecibo = myFragmentView.findViewById(R.id.img_recibo);

        mTviewCuentaEntidad.setOnClickListener(view -> {

            Intent Principal = new Intent(context, ListCuentaActivity.class);

            startActivityForResult(Principal, 2);

        });


        mTviewRecibo.setOnClickListener(view -> selectImage());


        Bundle parametros=getArguments();

        if(parametros!=null)
        {
            mIdPagoPedido = parametros.getInt("mIdPagoPedido");
            totalPedido = parametros.getDouble("totalPedido");
            mNuPorc = parametros.getDouble("mNuPorc");

            Double monto;

            if(mNuPorc>0){
                monto = totalPedido + totalPedido*mNuPorc;
            }else
            {
                monto = totalPedido;
            }

            DecimalFormat numberFormat = new DecimalFormat(" #,##0.00");

            String moneda = v_preferences.getString("simbmoneda","$");

            mMonto.setText(numberFormat.format(monto)+moneda);
            mMonto.setFocusable(false);


            Log.e(TAG, "Total Pedido Parametro:"+totalPedido);
        }


        FloatingActionButton mBtnPagar =  myFragmentView.findViewById(R.id.rep_btn_confirmar);


        mBtnPagar.setOnClickListener(v -> {

            if(mReferencia.getText().toString().isEmpty()){

                mReferencia.setError("Debe Introducir una Referencia");

            }else  if(mIdCuentaEntidad.equals(-1)){

                Toast.makeText(context, "Debe Seleccionar una Cuenta", Toast.LENGTH_LONG).show();

            }else if (imageBase64.isEmpty())
            {
                Toast.makeText(context, "Debe Seleccionar una Imagen de Recibo", Toast.LENGTH_LONG).show();

            }else {
                reportarPago();
            }

        });

        mReferencia.requestFocus();

        return myFragmentView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item_car = menu.findItem(R.id.action_cart);

        if (item_car != null) item_car.setVisible(false);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.actions_registro, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                MainActivity.currentTab="";

                BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);

                navigation.setSelectedItemId(R.id.navigation_catalogo);

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null) {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == REQUEST_TAKE_PHOTO) {
                    displayImagePreview(photoURI);
                } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                    Uri imageUri = data.getData();
                    displayImagePreview(imageUri);
                }
            }
            if (requestCode == 2) {
                Bundle parametros;
                parametros = data.getExtras();
                mIdCuentaEntidad = parametros.getInt("mIdCuentaEntidad");
                mTxCuenta.setText(parametros.getString("mTxCuenta"));

                Log.e(TAG, "mIdCuentaEntidad New:" + mIdCuentaEntidad);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            // we have heard back from our request for camera and write external storage.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                invokeCamera();
            } else {
                Toast.makeText(context, R.string.cannotopencamera, Toast.LENGTH_LONG).show();
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_gallery),getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Cámara")) {
                onTakePhotoClicked();
            } else if (items[item].equals("Galería")) {
                onImageGalleryClicked();
            } else if (items[item].equals("Cancelar")) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onTakePhotoClicked() {
        if(checkSelfPermission(getActivity(),Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            invokeCamera();
        } else {
            // let's request permission.
            String[] permissionRequest = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            requestPermissions(permissionRequest, CAMERA_PERMISSION_REQUEST_CODE);
        }
    }

    private void invokeCamera() {

        photoURI = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".provider", newFile());

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

        intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        startActivityForResult(intent, REQUEST_TAKE_PHOTO);

    }


    public void onImageGalleryClicked() {
        // invoke the image gallery using an implict intent.
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);

        // where do we want to find the data?
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        // finally, get a URI representation
        Uri data = Uri.parse(pictureDirectoryPath);

        // set the data and type.  Get all image types.
        photoPickerIntent.setDataAndType(data, "image/*");

        // we will invoke this activity, and get something back from it.
        startActivityForResult(photoPickerIntent, REQUEST_GALLERY_PHOTO);
    }


    public File getFilePath() {
        return getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public File newFile() {
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTimeInMillis();
        String mFileName = String.valueOf(timeInMillis) + ".jpeg";
        File mFilePath = getFilePath();
        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            newFile.createNewFile();

            currentPhotoPath = newFile.getAbsolutePath();

            return newFile;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void displayImagePreview(Uri mFileUri) {
        //Picasso.with(context).load(mFileUri).placeholder(R.drawable.avatar_default).into(userProfilePhoto);

        InputStream inputStream;

        // we are getting an input stream, based on the URI of the image.
        try {
            inputStream = getActivity().getContentResolver().openInputStream(mFileUri);

            // get a bitmap from the stream.
            Bitmap image = BitmapFactory.decodeStream(inputStream);

            // show the image to the user
            mRecibo.setImageBitmap(image);

            imageBase64 = ImageUtils.encodeBase64(image);

            Log.e(TAG,"imageBase64:"+imageBase64);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(context, "No se puede abrir imagen", Toast.LENGTH_LONG).show();
        }
    }

    public void reportarPago() {

        progressDialog = new ProgressDialog(context, R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        Call<ReportarPagoResponse> call = PuertomarApiAdapter.getApiService().reportarPago("Bearer " + token,mIdPagoPedido ,getJsonEncode());

        call.enqueue(new PedidosCallback());
    }

    class PedidosCallback implements Callback<ReportarPagoResponse> {

        public PedidosCallback() {
        }

        @Override
        public void onResponse(Call<ReportarPagoResponse> call, Response<ReportarPagoResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                ReportarPagoResponse pagoResponse = response.body();

                if (pagoResponse.isPago()) {

                    Toast.makeText(context, "Pago reportado con éxito ", Toast.LENGTH_SHORT).show();

                    BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);

                    navigation.setSelectedItemId(R.id.navigation_catalogo);

                }

            } else {

                try {

                    assert response.errorBody() != null;
                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    JSONArray arrJson = jObjError.getJSONArray("error");

                    Toast.makeText(context, arrJson.get(0).toString(), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(@NonNull Call<ReportarPagoResponse> call, Throwable t) {
            progressDialog.dismiss();
            //Log.e("PUERTOMAR pedidos",t.getLocalizedMessage());
            Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();
        }
    }


    public RequestBody getJsonEncode() {

        Log.e(TAG, "JsonEncode mIdCuentaEntidad:"+mIdCuentaEntidad);

        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("referencia", mReferencia.getText().toString());
        jsonParams.put("recibo", mReferencia.getText()+"_"+mIdPagoPedido);
        jsonParams.put("foto_recibo", imageBase64);
        jsonParams.put("cuenta", String.valueOf(mIdCuentaEntidad));
        jsonParams.put("monto", totalPedido);

        Log.e(TAG, "Params:"+jsonParams.toString());

        //RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());


        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.Companion.create((new JSONObject(jsonParams)).toString(), JSON);

        return body;
    }

}
