package com.goodmarcom.apk.Fragments;

import android.content.Context;
import android.content.Intent;
/*import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;*/
import android.os.Bundle;
//import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.goodmarcom.apk.Activity.CondicionUsoActivity;
import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Activity.PoliticaPrivacidadActivity;
import com.goodmarcom.apk.R;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


public class SeguridadPrivacidadFragment extends Fragment {


    private static final String TAG = "PUERTOMAR";


    private Context context;

    private View mPolPrivacidad;
    private View mCondUso;
    private FragmentManager fm;



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.fragment_seguridad_privacidad, container, false);

        context = myFragmentView.getContext();


        fm = getFragmentManager();

        mPolPrivacidad =  myFragmentView.findViewById(R.id.cfg_pol);
        mCondUso =  myFragmentView.findViewById(R.id.cfg_conduser);

        Toolbar toolbar_seg =  myFragmentView.findViewById(R.id.toolbar_seg);

        ((MainActivity)Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar_seg);


        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setTitle("Seguridad y Privacidad");

        }


        mPolPrivacidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent Principal = new Intent(context, PoliticaPrivacidadActivity.class);

                startActivity(Principal);

            }
        });

        mCondUso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent Principal = new Intent(context, CondicionUsoActivity.class);

                startActivity(Principal);

            }
        });

        return myFragmentView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                fm.popBackStack();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
