package com.goodmarcom.apk.Fragments;


import android.content.Context;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.R;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private Context context;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_main, container, false);


        context = myFragmentView.getContext();


        Toolbar toolbar_main =  myFragmentView.findViewById(R.id.toolbar_main);

        ((MainActivity)getActivity()).setSupportActionBar(toolbar_main);


        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("");

        }

        return myFragmentView;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item=menu.findItem(R.id.action_cart);

        if(MainActivity.notificationCountCart==0) {
            item.setVisible(false);
        }
        else {
            item.setVisible(true);
        }
    }


}
