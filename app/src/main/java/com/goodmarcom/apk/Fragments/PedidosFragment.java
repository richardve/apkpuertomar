package com.goodmarcom.apk.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.PedidosAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.PedidosResponse;
import com.goodmarcom.apk.Enty.Pedidos;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.EndlessRecyclerViewScrollListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * A simple {@link Fragment} subclass.
 */
public class PedidosFragment extends Fragment implements  SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private static final String TAG = "PUERTOMAR";

    private FragmentManager fm;
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager lManager;
    private Context context;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeLayout;

    private static final int PAGE_START = 1;

    private int TOTAL_PAGES = 0;
    private int currentPage = PAGE_START;

    private ArrayList<Pedidos> mPedidos = new ArrayList<>();

    private String token;

    private String mEstatus;

    private  Integer section_number;

    private Menu mMenu;

    private EndlessRecyclerViewScrollListener scrollListener;

    public PedidosFragment() {
        // Required empty public constructor
    }

  /*  public static PedidosFragment newInstance(int sectionNumber) {
        PedidosFragment fragment = new PedidosFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.fragment_pedidos, container, false);


        context = myFragmentView.getContext();

        fm = getFragmentManager();


        Toolbar toolbar_pedidos = myFragmentView.findViewById(R.id.toolbar_fped);

        ((MainActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar_pedidos);



        //obtiene la referencia a la preferencia de la aplicacion
        SharedPreferences v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","");

        // Obtener el Recycler
        recycler =  myFragmentView.findViewById(R.id.rv_pedidos);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new PedidosAdapter(mPedidos);
        recycler.setAdapter(adapter);

            //buscar los datos en el api
        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);

        Bundle bundle=getArguments();

        boolean detpedidos;

        if(bundle!=null)
        {
            detpedidos = bundle.getBoolean("detpedidos");
            section_number = bundle.getInt("ARG_SECTION_NUMBER");
            Log.e("PUERTOMAR", "detpedidod: " + detpedidos);
            Log.e("PUERTOMAR", "ARG_SECTION_NUMBER: " + section_number);
        }else
        {
            detpedidos = false;
        }

        String TipoOrden="";

        Log.e("PUERTOMAR", "Tamaño Pedidos: " + mPedidos.size());


        switch (section_number) {
            case 0:
                TipoOrden = "Pedidos Abiertos";
                mEstatus = "Nuevo,Planificado,Despachado,En Progreso,Aprobado";
                //obtenerDatos(PAGE_START, mEstatus);
                Log.e(TAG, "Estatus:" + mEstatus);
                break;
            case 1:
                TipoOrden = "Pedidos Procesados";
                mEstatus = "Cerrado";
                //obtenerDatos(PAGE_START, mEstatus);
                Log.e(TAG, "Estatus:" + mEstatus);
                break;
            case 2:
                TipoOrden = "Pedidos Anulados";
                mEstatus = "Anulado";
                //obtenerDatos(PAGE_START, mEstatus);
                Log.e(TAG, "Estatus:" + mEstatus);
                break;
            default:
        }



        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setTitle(TipoOrden);

        }


        if(!detpedidos) {

            progressDialog.show();
            progressDialog.setContentView(R.layout.progress_dialog);
            if(mPedidos.size()>0) {

                mPedidos.removeAll(mPedidos);
                adapter.notifyDataSetChanged();
                recycler.removeAllViewsInLayout();
                scrollListener.resetState();
            }
            obtenerDatos(PAGE_START, mEstatus);

           // loadData();

         }




        swipeLayout = myFragmentView.findViewById(R.id.swipe_pedidos);

        swipeLayout.setOnRefreshListener(this);
        //Podemos espeficar si queremos, un patron de colores diferente al patrón por defecto.
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        // Retain an instance so that you can call `resetState()` for fresh searches
        scrollListener = new EndlessRecyclerViewScrollListener(lManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                Log.e("Scroll",String.valueOf(TOTAL_PAGES));

                if(page <= TOTAL_PAGES || TOTAL_PAGES ==  0) {

                    obtenerDatos(page,mEstatus);

                }
            }
        };

        // Adds the scroll listener to RecyclerView
        recycler.addOnScrollListener(scrollListener);

        return myFragmentView;
    }

    @Override
    public void onRefresh() {

        Log.e("PUERTOMAR", "Tamaño Pedidos: " + mPedidos.size());
        Log.e(TAG, " Pedidos: " + "Refrescó");
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        if(mPedidos.size()>0) {
            mPedidos.removeAll(mPedidos);
            adapter.notifyDataSetChanged();
            recycler.removeAllViewsInLayout();
            scrollListener.resetState();
        }

        obtenerDatos(PAGE_START,mEstatus);

        swipeLayout.setRefreshing(false);

    }


    /*@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) { // fragment is visible and have created

            Log.e("PUERTOMAR", "Posicion Pedidos: " + section_number + " SetUser");
            loadData();
        }
    }*/

    public void loadData(){

        String TipoOrden="";

        Log.e("PUERTOMAR", "Tamaño Pedidos: " + mPedidos.size());

        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        if(mPedidos.size()>0) {

            mPedidos.removeAll(mPedidos);
            adapter.notifyDataSetChanged();
            recycler.removeAllViewsInLayout();
            scrollListener.resetState();
        }

        //section_number = getArguments().getInt("ARG_SECTION_NUMBER");


        switch (section_number) {
            case 0:
                TipoOrden = "Pedidod Abiertos";
                mEstatus = "Nuevo,Planificado,Despachado,En Progreso,Aprobado";
                obtenerDatos(PAGE_START, mEstatus);
                Log.e(TAG, "Estatus:" + mEstatus);
                break;
            case 1:
                TipoOrden = "Pedidos Procesados";
                mEstatus = "Cerrado";
                obtenerDatos(PAGE_START, mEstatus);
                Log.e(TAG, "Estatus:" + mEstatus);
                break;
            case 2:
                TipoOrden = "Pedidos Anulados";
                mEstatus = "Anulado";
                obtenerDatos(PAGE_START, mEstatus);
                Log.e(TAG, "Estatus:" + mEstatus);
                break;
            default:
        }

        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setTitle(TipoOrden);

        }

    }


    class PedidosCallback implements Callback<PedidosResponse> {

        @Override
        public void onResponse(Call<PedidosResponse> call, Response<PedidosResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                PedidosResponse pedidosResponse = response.body();
                TOTAL_PAGES = pedidosResponse.getTotalPage();
                ///scrollListener.resetState();
                //scrollListener.setTotalItem(TOTAL_PAGES);
                if(pedidosResponse.isPedido()){

                    // Crear un nuevo adaptador
                    adapter = new PedidosAdapter(mPedidos);
                    recycler.setAdapter(adapter);

                    mPedidos.addAll(pedidosResponse.getPedidos());
                    adapter.notifyDataSetChanged();

                   // Log.e("PUERTOMAR", "Tamaño Pedidos-BD: " + mPedidos.size());
                }


            } else {

                try {

                    progressDialog.dismiss();
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(Call<PedidosResponse> call, Throwable t) {
            progressDialog.dismiss();
            //Log.e("PUERTOMAR pedidos",t.getLocalizedMessage());
            Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();

        }
    }


    private void obtenerDatos(int page, String search) {

        Call<PedidosResponse> call = PuertomarApiAdapter.getApiService()
                .getPedidos("Bearer " + token,page,search);
        call.enqueue(new PedidosCallback());

    }


/*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.ations_filter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //setRetainInstance(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

       mMenu = menu;

        //mMenu.findItem(R.id.action_cart).setVisible(false);

        //mMenu.findItem(R.id.status).setVisible(false);


    }


    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                //Bundle bundle = getArguments();

               // String fragment_origen = bundle.getString("fragment_origen");

               // Log.e(TAG, "Detalles Pedido FrmOrigen:" + fragment_origen);

                fm.popBackStack();
                /*Fragment frg = null;
                frg = getActivity().getSupportFragmentManager().findFragmentByTag(fragment_origen);
                if (fragment_origen.equals("historial")) {
                    bundle.putBoolean("detpedidos", true);
                    frg.setArguments(bundle);

                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    assert frg != null;
                    ft.detach(frg);
                    ft.attach(frg);
                    ft.commit();
                }*/

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


  /*  @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        //do your stuff here
        Log.e("PUERTOMAR", "onResument " );
    }
*/


}
