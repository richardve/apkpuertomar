package com.goodmarcom.apk.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.GruposAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.GrupoResponse;
import com.goodmarcom.apk.Api.response.SliderCatalogoResponse;
import com.goodmarcom.apk.Enty.Grupos;
import com.goodmarcom.apk.Enty.Imagen;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.EndlessRecyclerViewScrollListener;
import com.goodmarcom.apk.Utils.SliderItem;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class CatalogoGrupoFragment extends Fragment implements SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener, SwipeRefreshLayout.OnRefreshListener{


    private static final String TAG = "PUERTOMAR";


    private Context context;
    private GruposAdapter Adapter;
    private RecyclerView recycler;

    //private LinearLayoutManager lManager;

    private GridLayoutManager Manager;

    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private int TOTAL_PAGES = 0;

    private PuertomarApiServices gruposService;

    private String token;
    private String co_ciudad;
    private String search="";
    private Timer timer = new Timer();
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeLayout;

    //SliderView sliderView;
   // private ImageSlideAdapter adapterSlider;


    ArrayList<Grupos> mGrupos = new ArrayList<>();

    private EndlessRecyclerViewScrollListener scrollListener;


    public CatalogoGrupoFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.fragment_catalogo_grupo, container, false);

        context = myFragmentView.getContext();

        Adapter = new GruposAdapter(mGrupos);
        Manager =  new GridLayoutManager(context, 2);


        /*sliderView = myFragmentView.findViewById(R.id.imageSliderCatalogo);

        adapterSlider = new ImageSlideAdapter(context);
        adapterSlider.setGrupo(true);
        sliderView.setSliderAdapter(adapterSlider);

        sliderView.setIndicatorAnimation(IndicatorAnimations.THIN_WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.WHITE);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();*/


        Toolbar toolbar_fct =  myFragmentView.findViewById(R.id.toolbar_cgf);

        ((MainActivity)getActivity()).setSupportActionBar(toolbar_fct);


        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("");

        }

        SharedPreferences v_preferences = getActivity().getSharedPreferences("session",Context.MODE_PRIVATE);


        token = v_preferences.getString("token","" );
        co_ciudad = v_preferences.getString("cociudad","" );

        Log.e(TAG, "token: " + token );
        Log.e(TAG, "co_ciudad" + co_ciudad );


        swipeLayout =  myFragmentView.findViewById(R.id.swipe_grupo);

        swipeLayout.setOnRefreshListener(this);
        //Podemos especificar si queremos, un patron de colores diferente al patrón por defecto.
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //init service and load data
        gruposService = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        obtenerGrupos(search,PAGE_START);

        recycler = myFragmentView.findViewById(R.id.rv_grupo);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);
        recycler.setLayoutManager(Manager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(Adapter);

        scrollListener = new EndlessRecyclerViewScrollListener(Manager ) {

            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if( page <= TOTAL_PAGES || TOTAL_PAGES ==  0) {

                    Log.e(TAG, "Scroll: " + search );
                    Log.e(TAG, "Page Grupos: " + page );
                    progressDialog.show();
                    currentPage = page;
                    obtenerGrupos(search, page);

                }
            }

        };

        recycler.addOnScrollListener(scrollListener);

        //sliderView.setOnIndicatorClickListener(position -> sliderView.setCurrentPagePosition(position));


        //obtenerImagenes();


        return myFragmentView;
    }


    private List<Grupos> fetchResults(Response<GrupoResponse> response) {
        GrupoResponse productoResponse = response.body();
        return productoResponse.getGrupos();
    }


    private void obtenerGrupos(final String search, final int page) {

        if(!Adapter.isEmpty()) Adapter.clear();

        callGruposApi(search, page).enqueue(new Callback<GrupoResponse>() {
            @Override
            public void onResponse(Call<GrupoResponse> call, Response<GrupoResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<Grupos> grupos = fetchResults(response);

                    mGrupos.addAll(grupos);
                    Adapter.notifyDataSetChanged();
                    Adapter.setCounter();
                    if(!search.isEmpty()) Adapter.setBusqueda(true);

                    TOTAL_PAGES = response.body().getTotalPage();

                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        if(search.isEmpty()) {
                            Toast.makeText(context, jObjError.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                        }

                        Log.e(TAG,jObjError.getJSONObject("error").getString("message"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<GrupoResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }


    private Call<GrupoResponse> callGruposApi(String search, int page) {

        Log.e(TAG, "search: " + search );

        return gruposService.getGrupos(co_ciudad,search,page, "Bearer "+token);
    }



    private List<Imagen> fetchResultsImagenes(Response<SliderCatalogoResponse> response) {
        SliderCatalogoResponse imagenResponse = response.body();
        return imagenResponse.getImagenes();
    }


    private void obtenerImagenes() {

        callImagenesApi().enqueue(new Callback<SliderCatalogoResponse>() {
            @Override
            public void onResponse(Call<SliderCatalogoResponse> call, Response<SliderCatalogoResponse> response) {


                if (response.isSuccessful()) {

                    List<Imagen> imagenes = fetchResultsImagenes(response);

                    SliderItem sliderItem = new SliderItem();

                    List<SliderItem> sliderItemList = new ArrayList<>();

                    for(Imagen img:imagenes) {
                        sliderItem.setDescription("");
                        sliderItem.setImageUrl(img.getUrlImagen());
                        sliderItemList.add(sliderItem);
                    }
                    //adapterSlider.renewItems(sliderItemList);


                } else {

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());

                        Log.e(TAG, jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<SliderCatalogoResponse> call, Throwable t) {
                t.printStackTrace();
                //progressDialog.dismiss();
                Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private Call<SliderCatalogoResponse> callImagenesApi() {

        return gruposService.getImgSliderCatalogo("Bearer "+token);
    }





    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.actions_search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView sv = (SearchView) item.getActionView();
        if (sv != null){
            sv.setSubmitButtonEnabled(false);
            sv.setOnQueryTextListener(this);
            sv.setMaxWidth(Integer.MAX_VALUE);
            sv.setQueryHint("Productos");
        }

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item=menu.findItem(R.id.search);

        if(item!=null) item.setVisible(true);

    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return false;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {

        search = s;

        timer.cancel();
        timer = new Timer();
        int sleep = 350;
        if (search.length() == 1)
            sleep = 1500;
        else if (search.length() <= 3)
            sleep = 1000;
        else if (search.length() <= 5)
            sleep = 800;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                    scrollListener.resetState();
                    obtenerGrupos(search, PAGE_START);
            }
        }, sleep);

        return false;
    }



    @Override
    public void onRefresh() {

        progressDialog.show();

        if (mGrupos.size() > 0) {
                mGrupos.removeAll(mGrupos);
                Adapter.notifyDataSetChanged();
                recycler.removeAllViewsInLayout();
                scrollListener.resetState();
        }

            obtenerGrupos(search, PAGE_START);

        swipeLayout.setRefreshing(false);

    }
}

