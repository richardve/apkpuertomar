package com.goodmarcom.apk.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.ListProductoAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.CombosResponse;
import com.goodmarcom.apk.Enty.Combo;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.EndlessRecyclerViewScrollListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class ComboFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private static final String TAG = "PUERTOMAR";

    private FragmentManager fm;
    private Context context;

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager lManager;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeLayout;


    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private int TOTAL_PAGES = 0;

    private PuertomarApiServices productosService;

    private String token;
    private String co_ciudad;
    private String search = "";


    ArrayList<Combo> mProductos = new ArrayList<>();

    private Integer co_combo;

    private EndlessRecyclerViewScrollListener scrollListener;


    public ComboFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View myFragmentView = inflater.inflate(R.layout.fragment_combo, container, false);

        context = myFragmentView.getContext();

        fm = getFragmentManager();

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getContext());
        adapter = new ListProductoAdapter(mProductos);


        co_combo = getArguments().getInt("co_combo");

        Log.e(TAG, "Combo Recibido: " + co_combo);


        Toolbar toolbar_fct = myFragmentView.findViewById(R.id.toolbar_fcombo);

        ((MainActivity) getActivity()).setSupportActionBar(toolbar_fct);

        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("Productos Combos");

        }



        SharedPreferences v_preferences = getActivity().getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );

        co_ciudad = v_preferences.getString("cociudad", "");


        swipeLayout = myFragmentView.findViewById(R.id.swipe_listproductos);

        swipeLayout.setOnRefreshListener(this);
        //Podemos especificar si queremos, un patron de colores diferente al patrón por defecto.
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //init service and load data
        productosService = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context, R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        //Código agregado
        if (mProductos.size() > 0) {
            mProductos.removeAll(mProductos);
            adapter.notifyDataSetChanged();
            recycler.removeAllViewsInLayout();
            scrollListener.resetState();
        }

        obtenerProductos(search, PAGE_START);


        recycler = myFragmentView.findViewById(R.id.rv_listproducto);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);
        recycler.setLayoutManager(lManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(adapter);




        scrollListener = new EndlessRecyclerViewScrollListener(lManager) {

            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if (page <= TOTAL_PAGES || TOTAL_PAGES == 0) {

                    Log.e(TAG, "Scroll Productos: " + search);
                    Log.e(TAG, "Page Productos: " + page);
                    Log.e(TAG, "Total Pages: " + TOTAL_PAGES);
                    // progressDialog.show();
                    currentPage = page;

                    obtenerProductos(search, page);


                }
            }

        };

        recycler.addOnScrollListener(scrollListener);



        //mCombo = (ArrayList<Combo>) getArguments().getSerializable("combos");

        //mProductos.addAll((ArrayList<Combo>) getArguments().getSerializable("combos"));
        //adapter.notifyDataSetChanged();


        return myFragmentView;
    }


    private List<Combo> fetchResultsProductos(Response<CombosResponse> response) {
        CombosResponse productoResponse = response.body();
        return productoResponse.getProductos();
    }


    private void obtenerProductos(final String search, int page) {

        callGProductosApi(search, page).enqueue(new Callback<CombosResponse>() {
            @Override
            public void onResponse(Call<CombosResponse> call, Response<CombosResponse> response) {

                progressDialog.dismiss();

                if (response.isSuccessful()) {

                    List<Combo> productos = fetchResultsProductos(response);

                    mProductos.addAll(productos);
                    adapter.notifyDataSetChanged();
                   // adapter.setCounter();

                    TOTAL_PAGES = response.body().getTotalPage();

                } else {

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        if (search.isEmpty()) {
                            Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();
                        }

                        Log.e(TAG, jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<CombosResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private Call<CombosResponse> callGProductosApi(String search, int page) {

        Log.e(TAG, "search: " + search);

        return productosService.getProductosCombo(co_ciudad, co_combo,search, page, "Bearer " + token);
    }

    @Override
    public void onRefresh() {

        progressDialog.show();

        if (mProductos.size() > 0) {
            mProductos.removeAll(mProductos);
            adapter.notifyDataSetChanged();
            recycler.removeAllViewsInLayout();
            scrollListener.resetState();
        }
        obtenerProductos(search, PAGE_START);
        swipeLayout.setRefreshing(false);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                fm.popBackStack();
               /* Fragment frg = null;
                frg = getActivity().getSupportFragmentManager().findFragmentByTag("catalogo_productos");
                final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                assert frg != null;
                ft.detach(frg);
                ft.attach(frg);
                ft.commit();*/

                /*MainActivity.currentTab="";

                BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);

                navigation.setSelectedItemId(R.id.navigation_catalogo);*/


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
