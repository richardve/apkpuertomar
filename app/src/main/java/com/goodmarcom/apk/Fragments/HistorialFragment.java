package com.goodmarcom.apk.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;



public class HistorialFragment extends Fragment {

    private View mPedidosAbiertos;
    private View mPedidosProcesados;
    private View mPedidosAnulados;


    private static Integer mPosicion=0;

    //private PagerAdapter mAdapter;

    public static boolean detpedidos;

    private FragmentManager fm;


    public HistorialFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.fragment_historial, container, false);

        Toolbar toolbar_hist =  myFragmentView.findViewById(R.id.toolbar_hist);


        fm = getFragmentManager();

        mPedidosAbiertos =  myFragmentView.findViewById(R.id.hist_ped_abiertos);
        mPedidosProcesados =  myFragmentView.findViewById(R.id.hist_ped_procesados);
        mPedidosAnulados =  myFragmentView.findViewById(R.id.hist_ped_anulados);





        mPedidosAbiertos.setOnClickListener(view -> {

            Bundle parametros = new Bundle();

            parametros.putInt("ARG_SECTION_NUMBER", 0);

            PedidosFragment frag = new PedidosFragment();

            frag.setArguments(parametros);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_content, frag,"historial")
                    .addToBackStack(null)
                    .commit();

        });

        mPedidosProcesados.setOnClickListener(view -> {

            Bundle parametros = new Bundle();

            parametros.putInt("ARG_SECTION_NUMBER", 1);

            PedidosFragment frag = new PedidosFragment();

            frag.setArguments(parametros);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_content, frag,"historial")
                    .addToBackStack(null)
                    .commit();

        });


        mPedidosAnulados.setOnClickListener(view -> {

            Bundle parametros = new Bundle();

            parametros.putInt("ARG_SECTION_NUMBER", 2);
            PedidosFragment frag = new PedidosFragment();

            frag.setArguments(parametros);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_content, frag,"historial")
                    .addToBackStack(null)
                    .commit();

        });


        ((MainActivity)getActivity()).setSupportActionBar(toolbar_hist);


       // PedidosFragment.newInstance(0);




  /*      // Preparar las pestañas
        mTabs = myFragmentView.findViewById(R.id.tabs);


        mTabs.addTab(mTabs.newTab().setText(getString(R.string.title_section1)));
        mTabs.addTab(mTabs.newTab().setText(getString(R.string.title_section2)));
        mTabs.addTab(mTabs.newTab().setText(getString(R.string.title_section3)));
        mTabs.setTabGravity(TabLayout.GRAVITY_FILL);*/


        /*mViewPager = myFragmentView.findViewById(R.id.pager);

        if (mViewPager != null) {


            mAdapter = new PagerAdapter(getChildFragmentManager(), mTabs.getTabCount());
            mViewPager.setAdapter(mAdapter);

            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabs));


        }

        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()

        {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                detpedidos = false;

                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/


        Bundle bundle=getArguments();


       if(bundle!=null)
       {
           detpedidos = bundle.getBoolean("detpedidos");
       }else
       {
           detpedidos = false;
       }

        return myFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.actions_pedidos, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onResume() {
        super.onResume();

        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("Historial");
            ((AppCompatActivity)getActivity()).getSupportActionBar().show();

        }

    }
    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        /*MenuItem item_car = menu.findItem(R.id.action_cart);

        if (item_car != null) item_car.setVisible(false);*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                fm.popBackStack();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
