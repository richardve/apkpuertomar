package com.goodmarcom.apk.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
/*import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;*/
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.goodmarcom.apk.Activity.InfoEmpresarialActivity;
import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Activity.OptarCreditoActivity;
import com.goodmarcom.apk.R;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;


public class ConfiguracionFragment extends Fragment {

    private static final String TAG = "PUERTOMAR";


    private Context context;
    private View mDatosusuario;
    private View mDatosEmpresa;
    private View mSegPrivacidad;
    private View mOptCredito;
    private View mRepPago;


    public ConfiguracionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myFragmentView =  inflater.inflate(R.layout.fragment_configuracion, container, false);

        context = myFragmentView.getContext();

        mDatosusuario =  myFragmentView.findViewById(R.id.cfg_usuario);
        mDatosEmpresa =  myFragmentView.findViewById(R.id.cfg_empresa);
        mSegPrivacidad =  myFragmentView.findViewById(R.id.cfg_segPrivacidad);
        mOptCredito =  myFragmentView.findViewById(R.id.cfg_optCredito);




        Toolbar toolbar_cfg =  myFragmentView.findViewById(R.id.toolbar_cfg);

        ((MainActivity)getActivity()).setSupportActionBar(toolbar_cfg);


        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("Ajustes");

        }


        mDatosusuario.setOnClickListener(view -> getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, new PerfilFragment(),"perfil")
                .addToBackStack(null)
                .commit());


        mDatosEmpresa.setOnClickListener(view -> {

            Intent Principal = new Intent(context, InfoEmpresarialActivity.class);
            startActivity(Principal);

        });

        mSegPrivacidad.setOnClickListener(view -> getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, new SeguridadPrivacidadFragment(),"seguridad")
                .addToBackStack(null)
                .commit());

        mOptCredito.setOnClickListener(view -> {

           Intent Principal = new Intent(context, OptarCreditoActivity.class);

            startActivity(Principal);

        });


        return myFragmentView;
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item=menu.findItem(R.id.search);

        if(item!=null) item.setVisible(false);

    }

}
