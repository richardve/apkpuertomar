package com.goodmarcom.apk.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.R;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;


public class OpcionPediosFragment extends Fragment {

    private Context context;

    private View mHistorial;
    private View mRepPago;
    private View mDeudas;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View myFragmentView =  inflater.inflate(R.layout.fragment_opcion_pedidos, container, false);

        context = myFragmentView.getContext();


        mHistorial =  myFragmentView.findViewById(R.id.ped_historial);
        mRepPago =  myFragmentView.findViewById(R.id.ped_rep_pagos);
        mDeudas =  myFragmentView.findViewById(R.id.ped_deudas);


        Toolbar toolbar_ped =  myFragmentView.findViewById(R.id.toolbar_pedidos);

        ((MainActivity)getActivity()).setSupportActionBar(toolbar_ped);


        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("Pedidos");

        }


        mHistorial.setOnClickListener(view -> {

            HistorialFragment frag = new HistorialFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_content, frag,"historial")
                    .addToBackStack(null)
                    .commit();

        });


        mRepPago.setOnClickListener(view -> {

            ReportePagosFragment frag = new ReportePagosFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_content, frag,"reporte_pagos")
                    .addToBackStack(null)
                    .commit();


        });

        mDeudas.setOnClickListener(view -> {

            DeudasFragment frag = new DeudasFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_content, frag,"deudas")
                    .addToBackStack(null)
                    .commit();


        });


        return myFragmentView;


    }


}





