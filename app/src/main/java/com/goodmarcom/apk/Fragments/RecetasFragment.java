package com.goodmarcom.apk.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
/*import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;*/
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.RecetasAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.RecetasResponse;
import com.goodmarcom.apk.Enty.Recetas;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.EndlessRecyclerViewScrollListener;

import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Response;



/**
 * A simple {@link Fragment} subclass.
 */
public class RecetasFragment extends Fragment implements  SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = "PUERTOMAR";

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager lManager;
    private Context context;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeLayout;

    private static final int PAGE_START = 1;

    private int TOTAL_PAGES = 0;
    private int currentPage = PAGE_START;

    ArrayList<Recetas> mRecetas = new ArrayList<>();

    private  String token;


    private EndlessRecyclerViewScrollListener scrollListener;


    public RecetasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_recetas, container, false);

        context = myFragmentView.getContext();

        Toolbar toolbar_fr =  myFragmentView.findViewById(R.id.toolbar_fr);

        ((MainActivity)getActivity()).setSupportActionBar(toolbar_fr);


        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("");

        }


        SharedPreferences v_preferences = getActivity().getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );


        swipeLayout =  myFragmentView.findViewById(R.id.swipe_recetas);


        swipeLayout.setOnRefreshListener(this);
        //Podemos espeficar si queremos, un patron de colores diferente al patrón por defecto.
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);



        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);


        obtenerDatos(PAGE_START);


        // Obtener el Recycler
        recycler = myFragmentView.findViewById(R.id.rv_recetas);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(lManager);

        adapter = new RecetasAdapter(mRecetas);
        recycler.setAdapter(adapter);


        scrollListener = new EndlessRecyclerViewScrollListener(lManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                Log.e("Scroll Recetas",String.valueOf(TOTAL_PAGES));
                if(page <= TOTAL_PAGES || TOTAL_PAGES ==  0) {
                    obtenerDatos(page);
                }
            }
        };

        // Adds the scroll listener to RecyclerView
        recycler.addOnScrollListener(scrollListener);

        return myFragmentView;
    }


    private void obtenerDatos(int page) {

        Call<RecetasResponse> call = PuertomarApiAdapter.getApiService().getRecetas(page, "Bearer "+token);

        call.enqueue(new RecetasCallback());
    }

    @Override
    public void onRefresh() {

        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);
        if(mRecetas.size()>0) {
            mRecetas.removeAll(mRecetas);
            adapter.notifyDataSetChanged();
            recycler.removeAllViewsInLayout();
            scrollListener.resetState();
        }

        obtenerDatos(PAGE_START);

        swipeLayout.setRefreshing(false);

    }


    private class RecetasCallback implements retrofit2.Callback<RecetasResponse> {
        @Override
        public void onResponse(Call<RecetasResponse> call, Response<RecetasResponse> response) {

            progressDialog.dismiss();


            if(response.isSuccessful()){
                RecetasResponse recetasResponse = response.body();

                TOTAL_PAGES = recetasResponse.getTotalPage();
                ///scrollListener.resetState();

                if(recetasResponse.isRecetas()){
                    Log.e(TAG, "Se Obtuvo las recetas... ");


                    ArrayList<Recetas> recetas = recetasResponse.getRecetas();
                    if(recetas.size()==0) {
                        Toast.makeText(context, "No hay recetas para mostrar", Toast.LENGTH_LONG).show();
                    }
                    // Crear un nuevo adaptador
                   /* adapter = new RecetasAdapter(recetas);
                    recycler.setAdapter(adapter);*/

                    mRecetas.addAll(recetas);
                    adapter.notifyDataSetChanged();

                }

            }else{

                try {

                    assert response.errorBody() != null;
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(context, jObjError.getJSONObject("error").getString("message") , Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }
            }

        }

        @Override
        public void onFailure(Call<RecetasResponse> call, Throwable t) {
            progressDialog.dismiss();
            //Toast.makeText(getActivity(),t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        /*MenuItem item=menu.findItem(R.id.action_cart);
        item.setVisible(false);*/
    }
}
