package com.goodmarcom.apk.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.DetallePedidoAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.PedidosResponse;
import com.goodmarcom.apk.Enty.Pedidos;
import com.goodmarcom.apk.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetallePedidoFragment extends Fragment {

    private static final String TAG = "PUERTOMAR";

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    private Context context;
    private Pedidos mPedido;
    private ProgressDialog progressDialog;
    private FragmentManager fm;
    private SharedPreferences v_preferences;

    private Integer mIdPagoPedido=0;

    private Boolean mPagar;

    private String mOpcion;


    private Double mNuPorc=0.00;

    private TextView mLbSubTotal;
    private TextView mLbRecargo;
    private TextView mLbTotal;

    private TextView mTxSubTotal;
    private TextView mTxRecargo;
    private TextView mTxTotal;


    public DetallePedidoFragment() {
        // Required empty public constructor
    }

    public DetallePedidoFragment(Pedidos pedidos) {
        this.mPedido = pedidos;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_detalle_pedido, container, false);


        Toolbar toolbar_dp =  myFragmentView.findViewById(R.id.toolbar_dp);

        ((MainActivity)getActivity()).setSupportActionBar(toolbar_dp);


        if (((MainActivity)getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("Detalle de Pedidos");
        }


        context = myFragmentView.getContext();

        fm = getFragmentManager();

        mPagar = false;

        mOpcion = "";


        //obtiene la referencia a la preferencia de la aplicacion
        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        // Obtener el Recycler
        recycler = myFragmentView.findViewById(R.id.rv_productos);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(lManager);

        DecimalFormat numberFormat = new DecimalFormat(" #,##0.00");

        //obtener los datos del pedido selecionado
        TextView codigo =  myFragmentView.findViewById(R.id.pd_codigo);
        TextView fecha =  myFragmentView.findViewById(R.id.pd_fecha);
        TextView estatus =  myFragmentView.findViewById(R.id.pd_estatus);
        TextView metodo =  myFragmentView.findViewById(R.id.pd_metodo);


        mLbSubTotal = myFragmentView.findViewById(R.id.dp_lb_subtotal);
        mLbRecargo = myFragmentView.findViewById(R.id.dp_lb_recargo);
        mLbTotal = myFragmentView.findViewById(R.id.dp_lb_total);

        mTxSubTotal = myFragmentView.findViewById(R.id.dp_tx_subtotal);
        mTxRecargo = myFragmentView.findViewById(R.id.dp_tx_recargo);
        mTxTotal = myFragmentView.findViewById(R.id.dp_tx_total);



        codigo.setText(mPedido.getCodigo());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatOut = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = format.parse(mPedido.getFecha());
            fecha.setText(formatOut.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        estatus.setText(mPedido.getEstatus());
        //total.setText(numberFormat.format(mPedido.getTotal()));

        String moneda =v_preferences.getString("simbmoneda","$");


        metodo.setText(mPedido.getTx_forma_pago());

        Double total_pedido,recargo=0.00, subtotal=mPedido.getTotal();

        mNuPorc = Double.valueOf(mPedido.getPago_pedido().getNu_porc());

        recargo = mNuPorc*subtotal;

        mLbRecargo.setText("Recargo "+mNuPorc*100+"%");

        total_pedido = subtotal + recargo;

        mTxSubTotal.setText(moneda+numberFormat.format(subtotal));
        mTxRecargo.setText(moneda+numberFormat.format(recargo));
        mTxTotal.setText(moneda+numberFormat.format(total_pedido));



        // Crear un nuevo adaptador
        adapter = new DetallePedidoAdapter(mPedido.getDetalle());
        recycler.setAdapter(adapter);


   /*     Button mBtnOpcion = myFragmentView.findViewById(R.id.dp_btn_pagar);


        Bundle parametros=getArguments();

        if(parametros!=null) mOpcion = parametros.getString("opcion");


       if(mOpcion.equals("Pagar") || mOpcion.equals("Anular")){
           mBtnOpcion.setVisibility(View.VISIBLE);
           mBtnOpcion.setText(mOpcion);
       }else {
           mBtnOpcion.setVisibility(View.GONE);
       }


        mBtnOpcion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mOpcion.equals("Pagar")) {

                    //verificar si el usuario esta activo
                    Boolean activo = v_preferences.getBoolean("in_activo", false);


                    Bundle parametros = new Bundle();


                    double totalPedido = mPedido.getTotal();

                    mIdPagoPedido = mPedido.getPago_pedido().getCo_pago();


                    Log.e(TAG, "mIdPagoPedido:" + mIdPagoPedido);

                    Log.e(TAG, "Total Pedido:" + totalPedido);


                    if (mIdPagoPedido != null) {
                        parametros.putInt("mIdPagoPedido", mIdPagoPedido);
                        parametros.putDouble("totalPedido", totalPedido);
                        parametros.putDouble("mNuPorc", mNuPorc);
                    }

                    ReportarPagoFragment frag = new ReportarPagoFragment();
                    frag.setArguments(parametros);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_content, frag, "reportar_pago")
                            .addToBackStack(null)
                            .commit();


                }

                if (mOpcion.equals("Anular")) {

                    anularPedido(mPedido.getCodigo());

                }
            }

        });*/


        return myFragmentView;
    }



    public void anularPedido(String codigo){

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        //obtiene la referencia a la preferencia de la aplicacion
        SharedPreferences v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        String token = v_preferences.getString("token","");

        Call<PedidosResponse> call = PuertomarApiAdapter.getApiService().deletePedidos("Bearer " + token,codigo);

        call.enqueue(new PedidosCallback());
    }


    class PedidosCallback implements Callback<PedidosResponse> {


        @Override
        public void onResponse(Call<PedidosResponse> call, Response<PedidosResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                PedidosResponse pedidosResponse = response.body();

                if(pedidosResponse.isPedido()){

                   String itemLabel = mPedido.getCodigo();

                    Toast.makeText(context,"Pedido "+ itemLabel +" anulado con éxito: " ,Toast.LENGTH_SHORT).show();

                    MainActivity.currentTab="";

                    BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);

                    navigation.setSelectedItemId(R.id.navigation_pedidos);

                }


            } else {

                try {

                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    JSONArray arrJson = jObjError.getJSONArray("error");

                    Toast.makeText(context, arrJson.get(0).toString(), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(Call<PedidosResponse> call, Throwable t) {
            progressDialog.dismiss();
            Log.e("PUERTOMAR pedidos",t.getLocalizedMessage());
            Toast.makeText(context,t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Log.e("Carrito", "Menu:"+item.getItemId());

        switch (item.getItemId()) {

            case android.R.id.home:

                Bundle bundle = getArguments();

                String fragment_origen = bundle.getString("fragment_origen");

                Log.e(TAG, "Detalles Pedido FrmOrigen:" + fragment_origen);

                fm.popBackStack();
               /* Fragment frg = null;
                frg = getActivity().getSupportFragmentManager().findFragmentByTag(fragment_origen);
                if (fragment_origen.equals("historial")) {

                    if(mPedido.getEstatus().equals("Nuevo")  ||
                            mPedido.getEstatus().equals("Planificado")  ||
                            mPedido.getEstatus().equals("Despachado") ||
                            mPedido.getEstatus().equals("En Progreso") ||
                            mPedido.getEstatus().equals("En Aprobado"))
                            bundle.putInt("ARG_SECTION_NUMBER", 0);
                    else if (mPedido.getEstatus().equals("Cerrado")){
                        bundle.putInt("ARG_SECTION_NUMBER", 1);
                    }else if (mPedido.getEstatus().equals("Anulado")){
                        bundle.putInt("ARG_SECTION_NUMBER", 2);
                    }

                    bundle.putBoolean("detpedidos", true);
                    frg.setArguments(bundle);

                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    assert frg != null;
                    ft.detach(frg);
                    ft.attach(frg);
                    ft.commit();
                }
*/                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.action_cart);
        item.setVisible(false);
    }


    @Override
    public void onResume() {
        super.onResume();

    }



}
