package com.goodmarcom.apk.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.DeudasAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.DeudasResponse;
import com.goodmarcom.apk.Enty.Deudas;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeudasFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "PUERTOMAR";


    private Context context;
    private DeudasAdapter adapter;
    private RecyclerView recycler;

    private LinearLayoutManager lManager;


    private PuertomarApiServices apiServices;

    private String token;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeLayout;

    ArrayList<Deudas> mDeudas = new ArrayList<>();

    private FragmentManager fm;

    private TextView mTotalMonto;
    private TextView mTotalSaldo;
    private String moneda;
    private RelativeLayout lyFooter;




    public DeudasFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.fragment_deudas, container, false);

        context = myFragmentView.getContext();


        fm = getFragmentManager();


        Toolbar toolbar_deuda = myFragmentView.findViewById(R.id.toolbar_deudas);

        ((MainActivity) getActivity()).setSupportActionBar(toolbar_deuda);


        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("Control de Deudas");

        }

        SharedPreferences v_preferences = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);


        token = v_preferences.getString("token", "");

        moneda = v_preferences.getString("simbmoneda","$");


        mTotalMonto = myFragmentView.findViewById(R.id.deuda_total_monto);
        mTotalSaldo = myFragmentView.findViewById(R.id.deuda_total_saldo);
        lyFooter = myFragmentView.findViewById(R.id.ly_footer);


        lyFooter.setVisibility(View.GONE);


        mTotalMonto.setText("");
        mTotalSaldo.setText("");

        swipeLayout = myFragmentView.findViewById(R.id.swipe_deudas);

        swipeLayout.setOnRefreshListener(this);
        //Podemos espeficar si queremos, un patron de colores diferente al patrón por defecto.
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        recycler = myFragmentView.findViewById(R.id.recycler_view_deudas);

        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        lManager = new LinearLayoutManager (getContext());

        recycler.setLayoutManager(lManager);

        recycler.setItemAnimator(new DefaultItemAnimator());

        adapter = new DeudasAdapter(mDeudas);

        recycler.setAdapter(adapter);


        //init service and load data
        apiServices = PuertomarApiAdapter.getApiService();

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        obtenerDeudas();


        return myFragmentView;
    }

    @Override
    public void onRefresh() {

    }

    private List<Deudas> fetchResults(Response<DeudasResponse> response) {
        DeudasResponse deudasResponse = response.body();
        return deudasResponse.getDeudas();
    }


    private void obtenerDeudas() {

        callDeudasApi().enqueue(new Callback<DeudasResponse>() {
            @Override
            public void onResponse(Call<DeudasResponse> call, Response<DeudasResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<Deudas> deudas = fetchResults(response);

                    mDeudas.addAll(deudas);
                    adapter.notifyDataSetChanged();

                    DecimalFormat numberFormat = new DecimalFormat(" #,##0.00");


                    lyFooter.setVisibility(View.VISIBLE);

                    mTotalMonto.setText(moneda+numberFormat.format(Double.valueOf(response.body().getMonto_total())));
                    mTotalSaldo.setText(moneda+numberFormat.format(Double.valueOf(response.body().getDeuda_total())));


                    //TOTAL_PAGES = response.body().getTotalPage();

                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<DeudasResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Call<DeudasResponse> callDeudasApi() {

        return apiServices.getDeudas( "Bearer "+token);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        /*MenuItem item_car = menu.findItem(R.id.action_cart);

        if (item_car != null) item_car.setVisible(false);*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                fm.popBackStack();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
