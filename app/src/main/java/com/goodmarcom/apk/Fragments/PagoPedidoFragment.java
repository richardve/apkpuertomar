package com.goodmarcom.apk.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.goodmarcom.apk.Activity.ListDireccionActivity;
import com.goodmarcom.apk.Activity.ListFormaPagoActivity;
import com.goodmarcom.apk.Activity.ListTipoPagoActivity;
import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Activity.ReportarPagoActivity;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.GuardarPedidosResponse;
import com.goodmarcom.apk.Api.response.PerfilResponse;
import com.goodmarcom.apk.Api.response.RecargoVentaResponse;
import com.goodmarcom.apk.Db.DBHelper;
import com.goodmarcom.apk.Enty.FormaPago;
import com.goodmarcom.apk.Enty.FormaPagoPerfil;
import com.goodmarcom.apk.Enty.Productos;
import com.goodmarcom.apk.Enty.RecargoVenta;
import com.goodmarcom.apk.Enty.TipoPago;
import com.goodmarcom.apk.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PagoPedidoFragment extends Fragment {

    private static final String TAG = "PUERTOMAR";

    private FragmentManager fm;

    private String mCociudad;

    private ProgressDialog progressDialog;

    private Context context;

    private String token;

    private Integer mIdDireccion=0;

    private boolean in_contado;

    private String mIdFormaPago="";
    private String mIdTipoPago="";
    private FormaPagoPerfil[] fp_class;
    private FormaPago[] fp_update;
    private TipoPago [] tipoPagos;
    private DecimalFormat numberFormat;
    private String mFormaPagoUpdate;
    private List<Productos> mProductos;
    private DBHelper v_db_helper;
    SharedPreferences v_preferences;

    private TextView mTviewFormaPago;
    private TextView mTviewTipoPago;
    private TextView mTviewDireccion;

    private TextView mTxDireccion;
    private TextView mTxFpago;
    private TextView mTxTpago;

    //private LinearLayout mLyTipoPago;

    private CardView mCardTipoPago;

    private Integer mIdPagoPedido;
    private Float mNuPorc;

    private TextView mLbSubTotal;
    private TextView mLbRecargo;
    private TextView mLbTotal;

    private TextView mTxSubTotal;
    private TextView mTxRecargo;
    private TextView mTxTotal;
    private Float mCupo;
    private Float mDeuda;
    private Float mTotal;
    private String moneda;

    private Float total_pedido;

    private ArrayList<RecargoVenta> mRecargoVenta;


    public PagoPedidoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myFragmentView =  inflater.inflate(R.layout.fragment_pago_pedido, container, false);

        context = myFragmentView.getContext();

        numberFormat = new DecimalFormat(" #,##0.00");

        mTviewFormaPago =  myFragmentView.findViewById(R.id.cp_fpago);
        mTviewTipoPago =  myFragmentView.findViewById(R.id.cp_tpago);
        mTviewDireccion =  myFragmentView.findViewById(R.id.cp_direccion);

        mTxDireccion = myFragmentView.findViewById(R.id.cp_tx_direccion);
        mTxFpago = myFragmentView.findViewById(R.id.cp_tx_fpago);
        mTxTpago = myFragmentView.findViewById(R.id.cp_tx_tpago);

       // mLyTipoPago = myFragmentView.findViewById(R.id.ly_tipo_pago);
        mCardTipoPago = myFragmentView.findViewById(R.id.cardViewTipoPago);


        mLbSubTotal = myFragmentView.findViewById(R.id.cp_lb_subtotal);
        mLbRecargo = myFragmentView.findViewById(R.id.cp_lb_recargo);
        mLbTotal = myFragmentView.findViewById(R.id.cp_lb_total);

        mTxSubTotal = myFragmentView.findViewById(R.id.cp_tx_subtotal);
        mTxRecargo = myFragmentView.findViewById(R.id.cp_tx_recargo);
        mTxTotal = myFragmentView.findViewById(R.id.cp_tx_total);

        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);


        moneda = v_preferences.getString("simbmoneda","$");


        mTviewFormaPago.setOnClickListener(view -> {

            Intent Principal = new Intent(context, ListFormaPagoActivity.class);

            startActivityForResult(Principal, 2);

        });


        mTviewTipoPago.setOnClickListener(view -> {

        if(mIdFormaPago.isEmpty())
        {
            Toast.makeText(context, "Debe seleccionar una forma de pago", Toast.LENGTH_SHORT).show();

        }else {

            Intent Principal = new Intent(context, ListTipoPagoActivity.class);

            Bundle parametros = new Bundle();

            parametros.putString("mIdFormaPago", mIdFormaPago);

            Principal.putExtras(parametros);

            startActivityForResult(Principal, 3);
        }

        });

        mTviewDireccion.setOnClickListener(view -> {

            Intent Principal = new Intent(context, ListDireccionActivity.class);

            startActivityForResult(Principal, 4);

        });


        fm = getFragmentManager();

        Button mBtnPagar = myFragmentView.findViewById(R.id.pp_btn_confirmar);


        mBtnPagar.setOnClickListener(v -> {

            //verificar si el usuario esta activo
            Boolean activo = v_preferences.getBoolean("in_activo", false);

            if (activo) {
                if (mProductos.size() > 0) {

                    if (mTxFpago.getText().toString().isEmpty()) {

                        //((TextView) mPago.getSelectedView()).setError("Campo Requerido");

                        Toast.makeText(context, "Debe seleccionar una forma de pago", Toast.LENGTH_SHORT).show();

                    } else if(in_contado && mTxTpago.getText().toString().isEmpty()){

                        //((TextView) mTipoPago.getSelectedView()).setError("Campo Requerido");

                        Toast.makeText(context, "Debe seleccionar un tipo de pago", Toast.LENGTH_SHORT).show();

                    }else if (mIdDireccion == 0) {

                        Toast.makeText(context, "Debe seleccionar una dirección", Toast.LENGTH_SHORT).show();

                    }else {
                        if(in_contado)
                        {
                            guardarPedido();
                        }else {
                            if ((mCupo != -1)) {
                                if ((mTotal > mCupo)) {
                                    Float disponible = mCupo - mDeuda;
                                    if(disponible>0) {
                                        Toast.makeText(context, "Su cupo disponible es: " + moneda + " " + numberFormat.format(disponible), Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(context, "No tiene cupo disponible: ", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    guardarPedido();
                                }
                            } else {
                                guardarPedido();
                                //Toast.makeText(context, "No tiene cupo disponible", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else {
                    Toast.makeText(context, "Debes tener almenos un produto para realizar el pedido", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Tu cuenta se encuentra desactivada", Toast.LENGTH_SHORT).show();
            }
        });

        Toolbar toolbar_pp = myFragmentView.findViewById(R.id.toolbar_pp);

        ((MainActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar_pp);


        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setTitle("Confirmar Pedido");

        }



        token = v_preferences.getString("token","" );

        v_db_helper = new DBHelper(context);

        mProductos = v_db_helper.queryAllProductos();

        //DecimalFormat numberFormat = new DecimalFormat(" #,##0.00");

        String moneda = v_preferences.getString("simbmoneda","$");

        Float recargo=0.0f, subtotal;

        subtotal = getTotalPedido();


        mTxSubTotal.setText(moneda+numberFormat.format(subtotal) );
        mLbRecargo.setText("Recargo 0.0%");
        mTxRecargo.setText(moneda+numberFormat.format(recargo) );
        mTxTotal.setText(moneda+numberFormat.format(subtotal));


        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        getRecargo();


        return myFragmentView;
    }


    public void getFormaPago() {


        Call<PerfilResponse> call = PuertomarApiAdapter.getApiService()
                .getPerfil("Bearer " + token);
        call.enqueue(new Callback<PerfilResponse>() {
            @Override
            public void onResponse(Call<PerfilResponse> call, Response<PerfilResponse> response) {

                if (response.isSuccessful()) {

                    progressDialog.dismiss();

                    PerfilResponse perfilResponse = response.body();

                    if (perfilResponse.isPerfil()) {

                        mFormaPagoUpdate = perfilResponse.getMyperfil().get(0).getJsonFormaPago();

                        mCupo = perfilResponse.getMyperfil().get(0).getCupo();
                        mDeuda = perfilResponse.getMyperfil().get(0).getDeudaTotal();

                        Gson gson = new Gson();
                        fp_class = gson.fromJson(mFormaPagoUpdate, FormaPagoPerfil[].class);

                        if(fp_class.length==1){
                            mIdFormaPago = fp_class[0].getCo_forma_pago();
                            mTxFpago.setText(fp_class[0].getMetodo().getTx_forma_pago());
                            in_contado = fp_class[0].getMetodo().getIn_contado();
                            mTviewFormaPago.setEnabled(false);
                        }else
                        {
                            mTviewFormaPago.setEnabled(true);
                            for(int i=0;i<fp_class.length;i++)
                            {
                                if(fp_class[i].getCo_forma_pago().equals("1")){
                                    mIdFormaPago = fp_class[i].getCo_forma_pago();
                                    mTxFpago.setText(fp_class[i].getMetodo().getTx_forma_pago());
                                    in_contado = fp_class[i].getMetodo().getIn_contado();
                                    break;
                                }
                            }
                        }

                        setTotal(mIdFormaPago);
                    }

                } else {

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<PerfilResponse> call, Throwable t) {

                progressDialog.dismiss();

            }

        });

    }


    public void getRecargo() {

        Call<RecargoVentaResponse> call = PuertomarApiAdapter.getApiService()
                .getPorcentajeRecargo("Bearer "+token );
        call.enqueue(new Callback<RecargoVentaResponse>() {
            @Override
            public void onResponse(Call<RecargoVentaResponse> call, Response<RecargoVentaResponse> response) {

               // progressDialog.dismiss();

                if (response.isSuccessful()) {

                    RecargoVentaResponse recargoVentaResponse = response.body();

                    if (recargoVentaResponse.isRecargo()) {

                        mRecargoVenta = recargoVentaResponse.getSuccess();

                        getFormaPago();

                    }


                } else {

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());

                        Log.e(TAG,jObjError.getString("error") );

                       // Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {

                        Log.e(TAG,e.getMessage() );

                        //Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<RecargoVentaResponse> call, Throwable t) {
                Log.e(TAG,"Error de Conexión" );
                //progressDialog.dismiss();
            }

        });

    }

    public void setTotal(String forma_pago){

        //DecimalFormat numberFormat = new DecimalFormat(" #,##0.00");

        Float recargo=0.0f, subtotal;

        subtotal = getTotalPedido();

        mNuPorc = 0.0f;

        for(RecargoVenta recargoVenta:mRecargoVenta) {
            if (recargoVenta.getCo_forma_pago().equals(forma_pago) && (recargoVenta.getCo_tipo_pago())==null) {
                mNuPorc =  Float.valueOf(recargoVenta.getNu_porcentaje());
                break;
            }
        }


            mLbRecargo.setText("Recargo " + mNuPorc * 100.0 + "%");

            recargo = mNuPorc * subtotal;

            total_pedido = subtotal + recargo;

            mTotal = mDeuda + total_pedido;


            mTxSubTotal.setText(moneda+numberFormat.format(subtotal));
            mTxRecargo.setText(moneda+numberFormat.format(recargo));


            mTxTotal.setText(moneda+numberFormat.format(total_pedido));

    }


    public void setTotal(String forma_pago,String tipo_pago){

        //DecimalFormat numberFormat = new DecimalFormat(" #,##0.00");

        String moneda = v_preferences.getString("simbmoneda","$");

        Float recargo=0.0f, subtotal;

        subtotal = getTotalPedido();

        mNuPorc = 0.0f;

        for(RecargoVenta recargoVenta:mRecargoVenta) {

            Log.e(TAG, "Config:"+recargoVenta.getCo_config() + " "+ recargoVenta.getCo_forma_pago() );

            if (recargoVenta.getCo_forma_pago().equals(forma_pago) && (recargoVenta.getCo_tipo_pago()).equals(tipo_pago)) {
                mNuPorc =  Float.valueOf(recargoVenta.getNu_porcentaje());
                break;
            }
        }

        mLbRecargo.setText("Recargo " + mNuPorc * 100 + "%");

        recargo = mNuPorc * subtotal;

        total_pedido = subtotal + recargo;

        mTxSubTotal.setText(moneda+numberFormat.format(subtotal));
        mTxRecargo.setText(moneda+numberFormat.format(recargo));

        mTxTotal.setText(moneda+numberFormat.format(total_pedido));

    }



    public void setSession() {

        //obtiene la referencia a la preferencia de la aplicacion
        SharedPreferences v_preferences = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);

        //obtiene el editor de las preferencias
        SharedPreferences.Editor v_editor = v_preferences.edit();

        v_editor.putBoolean("in_pedidos", true);
        //v_editor.putString("formapago", mFormaPagoUpdate);

        //confirma los cambios realizados
        v_editor.commit();

    }

    public void guardarPedido() {

        progressDialog = new ProgressDialog(context, R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        Call<GuardarPedidosResponse> call = PuertomarApiAdapter.getApiService().guardarPedidos("Bearer " + token, getJsonEncode());

        call.enqueue(new PedidosCallback());

    }

    class PedidosCallback implements Callback<GuardarPedidosResponse> {

        public PedidosCallback() {
        }

        @Override
        public void onResponse(Call<GuardarPedidosResponse> call, Response<GuardarPedidosResponse> response) {

            progressDialog.dismiss();

            if (response.isSuccessful()) {

                GuardarPedidosResponse pedidosResponse = response.body();

                if (pedidosResponse.isPedidos()) {

                    //eliminar los registros de la base de datos
                    v_db_helper.deleteAllProd();

                    MainActivity.notificationCountCart = 0;
                    MainActivity.setBadge(MainActivity.notificationCountCart);

                    //TextView carBadge = (TextView) ((MainActivity) context).findViewById(R.id.tx_badge);
                    //carBadge.setText("0");


                    mIdPagoPedido = pedidosResponse.getCo_pago();

                    //mNuPorc = pedidosResponse.getNu_porc();

                    Log.e(TAG, "mIdPagoPedido:"+mIdPagoPedido);

                    setSession();

                    int codPedido = pedidosResponse.getCo_pedido();

                    boolean reportarPago = pedidosResponse.isConfirmarPago();

                    // Show the removed item label`enter code here`
                    Toast.makeText(context, "Pedido Nro. " + codPedido + " guardado con éxito " , Toast.LENGTH_SHORT).show();

                    if(reportarPago) {

                        /*if ((mIdTipoPago.equals("0")) || (mIdTipoPago.equals("2")) && reportarPago) {*/

                            AlertDialog.Builder confirmDialog = new AlertDialog.Builder(context, R.style.AlertDialog);

                            confirmDialog.setTitle("Alerta");
                            confirmDialog.setMessage("Desea reportar el pago del pedido?");
                            confirmDialog.setCancelable(false);
                            confirmDialog.setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {

                                    Bundle parametros = new Bundle();

                                    double totalPedido = getTotalPedido();

                                    Log.e(TAG, "Total Pedido:"+totalPedido);

                                    if(mIdPagoPedido!=null)
                                    {
                                        parametros.putInt("mIdPagoPedido", mIdPagoPedido);
                                        parametros.putDouble("totalPedido", totalPedido);
                                        parametros.putDouble("mNuPorc", mNuPorc);
                                        parametros.putBoolean("isPagoPedido", true);
                                    }

                                    Intent Principal = new Intent(context, ReportarPagoActivity.class);

                                    Principal.putExtras(parametros);

                                    startActivityForResult(Principal, 1);

                                   /* ReportarPagoFragment frag = new ReportarPagoFragment();
                                    frag.setArguments(parametros);
                                    getActivity().getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment_content, frag,"reportar_pago")
                                            .addToBackStack(null)
                                            .commit();*/


                                }
                            });
                            confirmDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);

                                    navigation.setSelectedItemId(R.id.navigation_catalogo);

                                }
                            });
                            confirmDialog.show();


                    }else {

                        BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);

                        navigation.setSelectedItemId(R.id.navigation_catalogo);
                    }
                }


            } else {

                try {

                    assert response.errorBody() != null;
                    JSONObject jObjError = new JSONObject(response.errorBody().string());

                    JSONArray arrJson = jObjError.getJSONArray("error");

                    Toast.makeText(context, arrJson.get(0).toString(), Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

        }

        @Override
        public void onFailure(@NonNull Call<GuardarPedidosResponse> call, Throwable t) {
            progressDialog.dismiss();
            //Log.e("PUERTOMAR pedidos",t.getLocalizedMessage());
            Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();
        }
    }

    public RequestBody getJsonEncode() {
        JSONArray jsonProductos = new JSONArray();

        for (int i = 0; i < mProductos.size(); i++) {

            JSONObject obj = new JSONObject();
            try {
                obj.put("co_producto", "" + mProductos.get(i).getCo_producto());
                obj.put("nu_cantidad", "" + mProductos.get(i).getCantidad());
                obj.put("nu_precio", "" + mProductos.get(i).getPrecio());
                JSONArray put = jsonProductos.put(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("productos", jsonProductos);
        jsonParams.put("direccion", mIdDireccion);
        jsonParams.put("forma_pago", mIdFormaPago);
        jsonParams.put("tipo_pago", mIdTipoPago);

        Log.e(TAG, jsonParams.toString());

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());

        return body;
    }


    public Float getTotalPedido(){

        Float totalPedido = 0.0f;

        for (Productos str : mProductos) {
            totalPedido += Float.valueOf(str.getCantidad()) * Float.valueOf(str.getPrecio());
        }

        return totalPedido;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( data != null)
        {
            Bundle parametros;

            switch (requestCode) {

                case 1:

                    parametros = data.getExtras();

                    if( parametros.getBoolean("resul")) {

                        MainActivity.currentTab="";

                        BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);

                        navigation.setSelectedItemId(R.id.navigation_catalogo);

                    }

                case 2:

                   parametros = data.getExtras();

                    mIdFormaPago = parametros.getString("mIdFormaPago");

                    mTxFpago.setText(parametros.getString("tx_forma_pago"));

                    in_contado = parametros.getBoolean("in_contado");

                    if(in_contado){
                        //mTviewTipoPago.setEnabled(true);
                        //mLyTipoPago.setVisibility(View.VISIBLE);
                        mCardTipoPago.setVisibility(View.VISIBLE);

                    }else {
                        //mTviewTipoPago.setEnabled(false);
                        mIdTipoPago = "";
                        //mLyTipoPago.setVisibility(View.GONE);
                        mCardTipoPago.setVisibility(View.GONE);

                    }

                    setTotal(mIdFormaPago);
                    Log.e(TAG, "mFormaPago:"+mIdFormaPago);

                    break;

                case 3:

                    parametros = data.getExtras();

                    mIdTipoPago = parametros.getString("mIdTipoPago");

                    mTxTpago.setText(parametros.getString("tx_tipo_pago"));

                    setTotal(mIdFormaPago,mIdTipoPago);

                    Log.e(TAG, "mIdTipoPago:"+mIdTipoPago);

                    break;

                case 4:

                    parametros = data.getExtras();

                    mIdDireccion = parametros.getInt("mIdDireccion");

                    mTxDireccion.setText(parametros.getString("tx_direccion"));

                    Log.e(TAG, "mIdDireccion:"+mIdDireccion);

                    break;

                default:

            }

        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item_car = menu.findItem(R.id.action_cart);

        if (item_car != null) item_car.setVisible(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                fm.popBackStack();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
