package com.goodmarcom.apk.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.ProductosAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.ProductosResponse;
import com.goodmarcom.apk.Enty.Productos;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.EndlessRecyclerViewScrollListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class CatalogoFragment extends Fragment implements SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "PUERTOMAR";


    private Context context;
    private ProductosAdapter Adapter;
    private RecyclerView recycler;
    private LinearLayoutManager Manager;

    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private int TOTAL_PAGES = 0;

    private PuertomarApiServices productosService;

    private String token;
    private String co_ciudad;
    private String search = "";
    private Integer co_grupo;
    private String tx_grupo;
    private Timer timer = new Timer();
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeLayout;
    private FragmentManager fm;

    ArrayList<Productos> mProductos = new ArrayList<>();

    private EndlessRecyclerViewScrollListener scrollListener;



    public CatalogoFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myFragmentView = inflater.inflate(R.layout.fragment_catalogo, container, false);

        context = myFragmentView.getContext();

        fm = getFragmentManager();

        //mProductos = (ArrayList<Productos>) getArguments().getSerializable("productos");

        Adapter = new ProductosAdapter(mProductos);
        Manager = new LinearLayoutManager(getContext()); //new GridLayoutManager(context, 2);

        co_grupo = getArguments().getInt("co_grupo");
        tx_grupo = getArguments().getString("tx_grupo");

        Toolbar toolbar_fct = myFragmentView.findViewById(R.id.toolbar_fct);

        ((MainActivity) getActivity()).setSupportActionBar(toolbar_fct);


        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(tx_grupo);

        }


        SharedPreferences v_preferences = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);


        token = v_preferences.getString("token", "");
        co_ciudad = v_preferences.getString("cociudad", "");


        Log.e(TAG, "token: " + token);
        Log.e(TAG, "co_ciudad" + co_ciudad);


        swipeLayout = myFragmentView.findViewById(R.id.swipe_productos);

        swipeLayout.setOnRefreshListener(this);
        //Podemos especificar si queremos, un patron de colores diferente al patrón por defecto.
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //init service and load data
        productosService = PuertomarApiAdapter.getApiService();

       progressDialog = new ProgressDialog(context, R.style.ProgressDialog);
       progressDialog.setCancelable(false);
       progressDialog.show();
       progressDialog.setContentView(R.layout.progress_dialog);

        if (mProductos.size() > 0) {
            mProductos.removeAll(mProductos);
            Adapter.notifyDataSetChanged();
            recycler.removeAllViewsInLayout();
            scrollListener.resetState();
        }
        obtenerProductos(search, PAGE_START);
        swipeLayout.setRefreshing(false);

        recycler = myFragmentView.findViewById(R.id.rv_catalogo);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);
        recycler.setLayoutManager(Manager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(Adapter);


        scrollListener = new EndlessRecyclerViewScrollListener(Manager) {

            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if (page <= TOTAL_PAGES || TOTAL_PAGES == 0) {

                    Log.e(TAG, "Scroll Productos: " + search);
                    Log.e(TAG, "Page Productos: " + page);
                    Log.e(TAG, "Total Pages: " + TOTAL_PAGES);
                    // progressDialog.show();
                    currentPage = page;

                    obtenerProductos(search, page);


                }
            }

        };

        recycler.addOnScrollListener(scrollListener);

        return myFragmentView;
    }


     private List<Productos> fetchResultsProductos(Response<ProductosResponse> response) {
        ProductosResponse productoResponse = response.body();
        return productoResponse.getProductos();
    }


    private void obtenerProductos(final String search, int page) {

        callGProductosApi(search, page).enqueue(new Callback<ProductosResponse>() {
            @Override
            public void onResponse(Call<ProductosResponse> call, Response<ProductosResponse> response) {

                progressDialog.dismiss();

                if (response.isSuccessful()) {

                    List<Productos> productos = fetchResultsProductos(response);

                    mProductos.addAll(productos);
                    Adapter.notifyDataSetChanged();
                    Adapter.setCounter();

                    TOTAL_PAGES = response.body().getTotalPage();

                } else {

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        if (search.isEmpty()) {
                            Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();
                        }

                        Log.e(TAG, jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<ProductosResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private Call<ProductosResponse> callGProductosApi(String search, int page) {

        Log.e(TAG, "search: " + search);

        return productosService.getProductos(co_ciudad, co_grupo,search, page, "Bearer " + token);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.actions_search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView sv = (SearchView) item.getActionView();
        if (sv != null) {
            sv.setSubmitButtonEnabled(false);
            sv.setOnQueryTextListener(this);
            sv.setMaxWidth(Integer.MAX_VALUE);
            sv.setQueryHint("Productos");
            sv.setFocusable(true);
            sv.setFocusableInTouchMode(true);
            sv.requestFocus();
            sv.requestFocusFromTouch();
        }

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        search = s;
        scrollListener.resetState();
        Adapter.getFilter().filter(s);

     /* search = s;

        timer.cancel();
        timer = new Timer();
        int sleep = 350;
        if (search.length() == 1)
            sleep = 1500;
        else if (search.length() <= 3)
            sleep = 1000;
        else if (search.length() <= 5)
            sleep = 800;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                scrollListener.resetState();
                obtenerProductos(search, PAGE_START);
                swipeLayout.setRefreshing(false);
                Log.e(TAG, "Busqueda: " + search);
            }
        }, sleep);*/

        return false;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {

        return false;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return false;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.search);

        if (item != null) item.setVisible(true);

    }

    @Override
    public void onRefresh() {

        progressDialog.show();

        if (mProductos.size() > 0) {
            mProductos.removeAll(mProductos);
            Adapter.notifyDataSetChanged();
            recycler.removeAllViewsInLayout();
            scrollListener.resetState();
        }
        obtenerProductos(search, PAGE_START);
        swipeLayout.setRefreshing(false);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                //fm.popBackStack();

                MainActivity.currentTab="";

                BottomNavigationView navigation = ((MainActivity) context).findViewById(R.id.navigation);

                navigation.setSelectedItemId(R.id.navigation_catalogo);


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}




