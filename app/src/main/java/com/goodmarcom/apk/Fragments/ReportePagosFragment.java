package com.goodmarcom.apk.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
/*import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;*/
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.RepPagosPedidosAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.PuertomarApiServices;
import com.goodmarcom.apk.Api.response.PedidosResponse;
import com.goodmarcom.apk.Enty.Pedidos;
import com.goodmarcom.apk.R;
import com.goodmarcom.apk.Utils.EndlessRecyclerViewScrollListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReportePagosFragment extends Fragment implements  SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "PUERTOMAR";

    private PuertomarApiServices apiServices;

    SharedPreferences v_preferences;

    private Context context;

    RepPagosPedidosAdapter adapter;

    private RecyclerView recycler;

    private String token;

    private ProgressDialog progressDialog;

    ArrayList<Pedidos> mPedidos = new ArrayList<>();

    private LinearLayoutManager lManager;

    private SwipeRefreshLayout swipeLayout;

    private static final int PAGE_START = 1;

    private int TOTAL_PAGES = 0;
    private int currentPage = PAGE_START;

    private FragmentManager fm;

    private EndlessRecyclerViewScrollListener scrollListener;


    public ReportePagosFragment() {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Log.e(TAG,"onAttach");

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) {

           Log.e(TAG, "Visible"+isVisibleToUser);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myFragmentView =  inflater.inflate(R.layout.fragment_reporte_pagos, container, false);


        context = myFragmentView.getContext();

        fm = getFragmentManager();


        Toolbar toolbar_rep_pago = myFragmentView.findViewById(R.id.toolbar_rep_pagos);

        ((MainActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar_rep_pago);


        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setTitle("Reporte Pagos");

        }


        v_preferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);

        token = v_preferences.getString("token","" );


        recycler =  myFragmentView.findViewById(R.id.rv_rep_pagos_pedidos);
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);

        LinearLayoutManager lManager = new LinearLayoutManager (context);

        recycler.setLayoutManager(lManager);

        recycler.setItemAnimator(new DefaultItemAnimator());

        adapter = new RepPagosPedidosAdapter(mPedidos);

        recycler.setAdapter(adapter);

        progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
        progressDialog.setCancelable(false);
        //progressDialog.show();


        swipeLayout = myFragmentView.findViewById(R.id.swipe_rep_pagos_pedidos);

        swipeLayout.setOnRefreshListener(this);
        //Podemos espeficar si queremos, un patron de colores diferente al patrón por defecto.
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        apiServices = PuertomarApiAdapter.getApiService();


        scrollListener = new EndlessRecyclerViewScrollListener(lManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                Log.e(TAG,"Paginación - Total Página:"+String.valueOf(TOTAL_PAGES));

                if(page <= TOTAL_PAGES || TOTAL_PAGES ==  0) {

                    obtenerDatos(page);

                }
            }
        };

        // Adds the scroll listener to RecyclerView
        recycler.addOnScrollListener(scrollListener);

      /*  if (getUserVisibleHint()) {
            Log.e(TAG,"Visible");
        }
*/



        Log.e(TAG,"Inicio");

        obtenerDatos(PAGE_START);

        return myFragmentView;
    }


    private void obtenerDatos(int page) {

        Log.e(TAG,"Datos");

        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dialog);

        if(!adapter.isEmpty()) adapter.clear();

        callPedidosApi(page).enqueue(new Callback<PedidosResponse>() {
            @Override
            public void onResponse(Call<PedidosResponse> call, Response<PedidosResponse> response) {

                progressDialog.dismiss();

                if(response.isSuccessful()){

                    List<Pedidos> pedidos = fetchResults(response);

                    mPedidos.addAll(pedidos);
                    adapter.notifyDataSetChanged();

                    TOTAL_PAGES = response.body().getTotalPage();


                }else{

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        Log.e(TAG,jObjError.getString("error"));

                    } catch (Exception e) {

                        Log.e(TAG, " Error:: " + e.getMessage());

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<PedidosResponse> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(context,"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }


    private Call<PedidosResponse> callPedidosApi(int page) {

        return  apiServices.getPedidosPagos("Bearer "+token,page,"false");
    }

    private List<Pedidos> fetchResults(Response<PedidosResponse> response) {
        PedidosResponse pedidosResponse = response.body();
        return pedidosResponse.getPedidos();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item_car = menu.findItem(R.id.action_cart);

        if (item_car != null) item_car.setVisible(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                fm.popBackStack();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onRefresh() {


        Log.e(TAG,"Refresh");

        //progressDialog.show();
        if(mPedidos.size()>0) {
            mPedidos.removeAll(mPedidos);
            adapter.notifyDataSetChanged();
            recycler.removeAllViewsInLayout();
            scrollListener.resetState();
        }

        obtenerDatos(PAGE_START);

        swipeLayout.setRefreshing(false);

    }
}
