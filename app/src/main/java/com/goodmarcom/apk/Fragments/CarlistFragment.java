package com.goodmarcom.apk.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
/*import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;*/
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.goodmarcom.apk.Activity.MainActivity;
import com.goodmarcom.apk.Adapter.CarPedidoAdapter;
import com.goodmarcom.apk.Api.PuertomarApiAdapter;
import com.goodmarcom.apk.Api.response.PerfilResponse;
import com.goodmarcom.apk.Db.DBHelper;
import com.goodmarcom.apk.Enty.Productos;
import com.goodmarcom.apk.R;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class CarlistFragment extends Fragment {

    private static final String TAG = "PUERTOMAR";

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private Context context;
    private List<Productos> mProductos;
    private ProgressDialog progressDialog;
    private DBHelper v_db_helper;
    private SharedPreferences v_preferences;
    private FragmentManager fm;


    //private TextView mDeuda;
    private DecimalFormat numberFormat;
    //private float deuda_total;
    private String moneda = "";
    private Button mBtnRevisar;

    private String token;

    public CarlistFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View myFragmentView = inflater.inflate(R.layout.fragment_carlist, container, false);

        context = myFragmentView.getContext();

        fm = getFragmentManager();

        mBtnRevisar = myFragmentView.findViewById(R.id.cl_btn_revisar);


        mBtnRevisar.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {

                 getActivity().getSupportFragmentManager().beginTransaction()
                         .replace(R.id.fragment_content, new PagoPedidoFragment(),"pago")
                         .addToBackStack(null)
                         .commit();

            }

         });


        Toolbar toolbar_fc = myFragmentView.findViewById(R.id.toolbar_fc);

        ((MainActivity) getActivity()).setSupportActionBar(toolbar_fc);


        if (((MainActivity) getActivity()).getSupportActionBar() != null) {

            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("Carrito");

        }


        v_db_helper = new DBHelper(context);
        //obtiene la referencia a la preferencia de la aplicacion
        v_preferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);

        token = v_preferences.getString("token", "");

        String simboloMoneda = v_preferences.getString("simbmoneda", "$");

        // Obtener el Recycler
        recycler = myFragmentView.findViewById(R.id.rv_carpedidos);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        RecyclerView.LayoutManager lManager = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(lManager);

        mProductos = v_db_helper.queryAllProductos();

        numberFormat = new DecimalFormat(" #,##0.00");

        double total = 0.0;

        for (Productos str : mProductos) {
            total += Double.valueOf(str.getCantidad()) * Double.valueOf(str.getPrecio());
        }

        TextView totalView = myFragmentView.findViewById(R.id.lc_total);
        TextView cantidadView = myFragmentView.findViewById(R.id.lc_cantidad);
        //TextView monedaView = myFragmentView.findViewById(R.id.lc_moneda);

        totalView.setText(simboloMoneda+numberFormat.format(total));
        String cant = v_db_helper.getTotalProductos();
        if (null == cant)
            cant = "0";
        cantidadView.setText(cant);


        // Crear un nuevo adaptador
        adapter = new CarPedidoAdapter(mProductos);
        recycler.setAdapter(adapter);

        //getDeuda();

        return myFragmentView;
    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            Log.i("IsRefresh", "Yes");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.search);
        MenuItem item_car = menu.findItem(R.id.action_cart);

        if (item != null) item.setVisible(false);

        if (item_car != null) item_car.setVisible(false);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.actions_registro, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Log.e("Carrito", "Menu:"+item.getItemId());

        switch (item.getItemId()) {

            case android.R.id.home:

                Bundle bundle = getArguments();

                String fragment_origen = bundle.getString("fragment_origen");

                Log.e(TAG, "Carrito FrmOrigen:" + fragment_origen);

                fm.popBackStack();
                Fragment frg = null;
                frg = getActivity().getSupportFragmentManager().findFragmentByTag(fragment_origen);
                if (fragment_origen.equals("compra")) {
                    bundle.putBoolean("detpedidos", true);
                    frg.setArguments(bundle);
                }
                final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                assert frg != null;
                ft.detach(frg);
                ft.attach(frg);
                ft.commit();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getDeuda() {

        Call<PerfilResponse> call = PuertomarApiAdapter.getApiService()
                .getPerfil("Bearer " + token);
        call.enqueue(new Callback<PerfilResponse>() {
            @Override
            public void onResponse(Call<PerfilResponse> call, Response<PerfilResponse> response) {

                if (response.isSuccessful()) {

                    PerfilResponse perfilResponse = response.body();

                    if (perfilResponse.isPerfil()) {

                        //deuda_total = perfilResponse.getMyperfil().get(0).getDeudaTotal();
                        //moneda = perfilResponse.getMyperfil().get(0).getTx_simbolo_moneda();
                        //mDeuda.setText(numberFormat.format(deuda_total) + moneda);

                    }

                } else {

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.getString("error"), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }

                }


            }


            @Override
            public void onFailure(Call<PerfilResponse> call, Throwable t) {

            }

        });

    }


}
