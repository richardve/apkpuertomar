package com.goodmarcom.apk.Enty;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Clientes {

    private Integer co_cuenta;
    private String tx_usuario;
    @SerializedName("tx_correo_user")
    private String tx_correo_usuario;
    private Boolean in_activo;
    private String tx_token_fcm;
    private String co_empresa;
    private String tx_empresa;
    private String tx_representante_legal;
    private String tx_correo;
    private String tx_tlf;
    private String tx_direccion;
    private String tx_documento;
    private String co_ciudad;
    private String fe_registro;
    private String co_documento;
    private String co_pais;
    private String tx_ciudad;
    private String tx_zona_horaria;
    private String tx_pais;
    private String tx_moneda;
    private String tx_codiso;
    private String tx_codmoneda;
    private String tx_simbolo_moneda;
    private String tx_documento_legal;
    private Boolean in_pedidos;
    private Float deuda_total;
    private Float cupo;
    private String foto;
    private ArrayList<FormaPagoPerfil> formapago;
    private String tx_clave;


    public Integer getCo_cuenta() {
        return co_cuenta;
    }

    public void setCo_cuenta(Integer co_cuenta) {
        this.co_cuenta = co_cuenta;
    }

    public String getTx_usuario() {
        return tx_usuario;
    }

    public void setTx_usuario(String tx_usuario) {
        this.tx_usuario = tx_usuario;
    }

    public String getTx_correo() {
        return tx_correo;
    }

    public void setTx_correo(String tx_correo) {
        this.tx_correo = tx_correo;
    }

    public String getCo_empresa() {
        return co_empresa;
    }

    public void setCo_empresa(String co_empresa) {
        this.co_empresa = co_empresa;
    }

    public String getTx_empresa() {
        return tx_empresa;
    }

    public void setTx_empresa(String tx_empresa) {
        this.tx_empresa = tx_empresa;
    }

    public String getTx_representante_legal() {
        return tx_representante_legal;
    }

    public void setTx_representante_legal(String tx_representante_legal) {
        this.tx_representante_legal = tx_representante_legal;
    }

    public String getTx_tlf() {
        return tx_tlf;
    }

    public void setTx_tlf(String tx_tlf) {
        this.tx_tlf = tx_tlf;
    }

    public String getTx_direccion() {
        return tx_direccion;
    }

    public void setTx_direccion(String tx_direccion) {
        this.tx_direccion = tx_direccion;
    }

    public String getTx_documento() {
        return tx_documento;
    }

    public void setTx_documento(String tx_documento) {
        this.tx_documento = tx_documento;
    }

    public String getCo_ciudad() {
        return co_ciudad;
    }

    public void setCo_ciudad(String co_ciudad) {
        this.co_ciudad = co_ciudad;
    }

    public String getFe_registro() {
        return fe_registro;
    }

    public void setFe_registro(String fe_registro) {
        this.fe_registro = fe_registro;
    }

    public String getTx_pais() {
        return tx_pais;
    }

    public void setTx_pais(String tx_pais) {
        this.tx_pais = tx_pais;
    }

    public String getTx_ciudad() {
        return tx_ciudad;
    }

    public void setTx_ciudad(String tx_ciudad) {
        this.tx_ciudad = tx_ciudad;
    }

    public String getTx_clave() {
        return tx_clave;
    }

    public void setTx_clave(String tx_clave) {
        this.tx_clave = tx_clave;
    }

    public String getTx_correo_usuario() {
        return tx_correo_usuario;
    }

    public void setTx_correo_usuario(String tx_correo_usuario) {
        this.tx_correo_usuario = tx_correo_usuario;
    }

    public String getCo_pais() {
        return co_pais;
    }

    public void setCo_pais(String co_pais) {
        this.co_pais = co_pais;
    }

    public String getTx_zona_horaria() {
        return tx_zona_horaria;
    }

    public void setTx_zona_horaria(String tx_zona_horaria) {
        this.tx_zona_horaria = tx_zona_horaria;
    }

    public String getTx_codiso() {
        return tx_codiso;
    }

    public void setTx_codiso(String tx_codiso) {
        this.tx_codiso = tx_codiso;
    }

    public String getTx_codmoneda() {
        return tx_codmoneda;
    }

    public void setTx_codmoneda(String tx_codmoneda) {
        this.tx_codmoneda = tx_codmoneda;
    }

    public String getTx_moneda() {
        return tx_moneda;
    }

    public void setTx_moneda(String tx_moneda) {
        this.tx_moneda = tx_moneda;
    }

    public String getTx_simbolo_moneda() {
        return tx_simbolo_moneda;
    }

    public void setTx_simbolo_moneda(String tx_simbolo_moneda) {
        this.tx_simbolo_moneda = tx_simbolo_moneda;
    }

    public String getJsonFormaPago(){

        Gson gson = new Gson();

        return gson.toJson(getFormapago());

    }

    public ArrayList<FormaPagoPerfil> getFormapago() {
        return formapago;
    }

    public void setFormapago(ArrayList<FormaPagoPerfil> formapago) {
        this.formapago = formapago;
    }

    public Boolean getIn_pedidos() {
        return in_pedidos;
    }

    public void setIn_pedidos(Boolean in_pedidos) {
        this.in_pedidos = in_pedidos;
    }

    public Boolean getIn_activo() {
        return in_activo;
    }

    public void setIn_activo(Boolean in_activo) {
        this.in_activo = in_activo;
    }


    public Boolean getInEmpresa() {
        boolean isEmpresa=false;
        if(!(co_empresa==null)){
            isEmpresa = true;
            Log.e("PUERTOMAR", "En la Clase  Clientes: in_empresa" +  isEmpresa);
        }
        return isEmpresa;
    }

    public String getCo_documento() {
        return co_documento;
    }

    public void setCo_documento(String co_documento) {
        this.co_documento = co_documento;
    }

    public String getFoto() {

        if(foto==null)
        {
            return "";
        }else
        {
            return foto;
        }


    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getTx_documento_legal() {
        return tx_documento_legal;
    }

    public void setTx_documento_legal(String tx_documento_legal) {
        this.tx_documento_legal = tx_documento_legal;
    }

    public Float getDeudaTotal() {
        return deuda_total;
    }

    public void setDeudaTotal(Float deuda_total) {
        this.deuda_total = deuda_total;
    }

    public String getTx_token_fcm() {
        return tx_token_fcm;
    }

    public void setTx_token_fcm(String tx_token_fcm) {
        this.tx_token_fcm = tx_token_fcm;
    }

    public Float getCupo() {
        return cupo;
    }

    public void setCupo(Float cupo) {
        this.cupo = cupo;
    }
}



