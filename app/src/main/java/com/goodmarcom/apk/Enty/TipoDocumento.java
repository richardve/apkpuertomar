package com.goodmarcom.apk.Enty;

public class TipoDocumento {

   private Integer co_documento;
   private String co_pais;
   private String tx_documento;


    public Integer getCo_documento() {
        return co_documento;
    }

    public void setCo_documento(Integer co_documento) {
        this.co_documento = co_documento;
    }


    public String getCo_pais() {
        return co_pais;
    }

    public void setCo_pais(String co_pais) {
        this.co_pais = co_pais;
    }

    public String getTx_documento() {
        return tx_documento;
    }

    public void setTx_documento(String tx_documento) {
        this.tx_documento = tx_documento;
    }


}
