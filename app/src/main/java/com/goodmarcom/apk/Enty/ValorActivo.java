package com.goodmarcom.apk.Enty;

public class ValorActivo {

  private Integer co_caracteristica;
  private String co_activo;
  private String tx_valor;
  private Caracteristica caracteristica;


    public Integer getCo_caracteristica() {
        return co_caracteristica;
    }

    public void setCo_caracteristica(Integer co_caracteristica) {
        this.co_caracteristica = co_caracteristica;
    }

    public String getCo_activo() {
        return co_activo;
    }

    public void setCo_activo(String co_activo) {
        this.co_activo = co_activo;
    }

    public String getTx_valor() {

            if(tx_valor == null)
            {
                return "";
            }else {
                return tx_valor;
            }

        }

    public void setTx_valor(String tx_valor) {
        this.tx_valor = tx_valor;
    }

    public Caracteristica getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(Caracteristica caracteristica) {
        this.caracteristica = caracteristica;
    }
}


class Caracteristica {

    private Integer co_caracteristica;
    private String tx_caracteristica;
    private String tx_tipo_dato;


    public Integer getCo_caracteristica() {
        return co_caracteristica;
    }

    public void setCo_caracteristica(Integer co_caracteristica) {
        this.co_caracteristica = co_caracteristica;
    }

    public String getTx_caracteristica() {

        if(tx_caracteristica == null)
        {
            return "";
        }else {
            return tx_caracteristica;
        }
    }

    public void setTx_caracteristica(String tx_caracteristica) {
        this.tx_caracteristica = tx_caracteristica;
    }

    public String getTx_tipo_dato() {
        return tx_tipo_dato;
    }

    public void setTx_tipo_dato(String tx_tipo_dato) {
        this.tx_tipo_dato = tx_tipo_dato;
    }

}