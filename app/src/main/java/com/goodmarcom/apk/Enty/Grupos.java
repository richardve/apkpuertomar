package com.goodmarcom.apk.Enty;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Grupos {

    private Integer co_grupo;
    private String tx_grupo;
    private String tx_imagen;
    private String tx_ext_imagen;
    private boolean in_activo;
    @SerializedName("url_imagen")
    private String imagen;

    private ArrayList<Productos> productos;


    public Integer getCo_grupo() {
        return co_grupo;
    }

    public void setCo_grupo(Integer co_grupo) {
        this.co_grupo = co_grupo;
    }

    public String getTx_grupo() {
        return tx_grupo;
    }

    public void setTx_grupo(String tx_grupo) {
        this.tx_grupo = tx_grupo;
    }

    public ArrayList<Productos> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Productos> productos) {
        this.productos = productos;
    }

    public String getTx_imagen() {
        return tx_imagen;
    }

    public void setTx_imagen(String tx_imagen) {
        this.tx_imagen = tx_imagen;
    }

    public String getTx_ext_imagen() {
        return tx_ext_imagen;
    }

    public void setTx_ext_imagen(String tx_ext_imagen) {
        this.tx_ext_imagen = tx_ext_imagen;
    }

    public boolean isIn_activo() {
        return in_activo;
    }

    public void setIn_activo(boolean in_activo) {
        this.in_activo = in_activo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
