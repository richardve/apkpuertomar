package com.goodmarcom.apk.Enty;

public class Combo {
    private Integer co_combo;
    private Integer co_producto;
    private String nu_cantidad;
    private String tx_producto;
    private String nu_unidades;
    private String nu_precio;
    private String tx_ext_imagen;


    public Integer getCo_combo() {
        return co_combo;
    }

    public void setCo_combo(Integer co_combo) {
        this.co_combo = co_combo;
    }

    public Integer getCo_producto() {
        return co_producto;
    }

    public void setCo_producto(Integer co_producto) {
        this.co_producto = co_producto;
    }


    public String getNu_cantidad() {
        return nu_cantidad;
    }

    public void setNu_cantidad(String nu_cantidad) {
        this.nu_cantidad = nu_cantidad;
    }


    public String getTx_producto() {
        return tx_producto;
    }

    public void setTx_producto(String tx_producto) {
        this.tx_producto = tx_producto;
    }

    public String getNu_precio() {
        return nu_precio;
    }

    public void setNu_precio(String nu_precio) {
        this.nu_precio = nu_precio;
    }


    public String getTx_ext_imagen() {
        return tx_ext_imagen;
    }

    public void setTx_ext_imagen(String tx_ext_imagen) {
        this.tx_ext_imagen = tx_ext_imagen;
    }

    public String getNu_unidades() {
        if(this.nu_unidades == null) {
            return "";
        }else {
            return this.nu_unidades;
        }
    }

    public void setNu_unidades(String nu_unidades) {
        this.nu_unidades = nu_unidades;
    }
}
