package com.goodmarcom.apk.Enty;

public class Deudas {
    private  Integer co_pedido;
    private  String co_empresa;
    private  String co_estatus;
    private  String fe_registro;
    private  String co_cuenta;
    private  String nu_monto;
    private  String nu_saldo;
    private  String fe_vencimiento;
    private  String tx_estatus;
    private  PagoPedidos pago_pedido;


    public Integer getCo_pedido() {
        return co_pedido;
    }

    public void setCo_pedido(Integer co_pedido) {
        this.co_pedido = co_pedido;
    }

    public String getCo_empresa() {
        return co_empresa;
    }

    public void setCo_empresa(String co_empresa) {
        this.co_empresa = co_empresa;
    }

    public String getCo_estatus() {
        return co_estatus;
    }

    public void setCo_estatus(String co_estatus) {
        this.co_estatus = co_estatus;
    }

    public String getFe_registro() {
        return fe_registro;
    }

    public void setFe_registro(String fe_registro) {
        this.fe_registro = fe_registro;
    }

    public String getCo_cuenta() {
        return co_cuenta;
    }

    public void setCo_cuenta(String co_cuenta) {
        this.co_cuenta = co_cuenta;
    }

    public String getNu_monto() {
        return nu_monto;
    }

    public void setNu_monto(String nu_monto) {
        this.nu_monto = nu_monto;
    }

    public String getNu_saldo() {
        return nu_saldo;
    }

    public void setNu_saldo(String nu_saldo) {
        this.nu_saldo = nu_saldo;
    }

    public String getFe_vencimiento() {
        return fe_vencimiento;
    }

    public void setFe_vencimiento(String fe_vencimiento) {
        this.fe_vencimiento = fe_vencimiento;
    }

    public String getTx_estatus() {
        return tx_estatus;
    }

    public void setTx_estatus(String tx_estatus) {
        this.tx_estatus = tx_estatus;
    }

    public PagoPedidos getPago_pedido() {
        return pago_pedido;
    }

    public void setPago_pedido(PagoPedidos pago_pedido) {
        this.pago_pedido = pago_pedido;
    }


}




