package com.goodmarcom.apk.Enty;

import com.google.gson.annotations.SerializedName;

public class Direcciones {

    private Integer co_direccion;
    private String co_empresa;
    private String co_ciudad;
    private String tx_direccion;
    private String fe_registro;
    private String tx_codigo_postal;
    @SerializedName("nu_latitud")
    private Double latitud;
    @SerializedName("nu_longitud")
    private Double longitud;
    private String tx_sector;
    private boolean tiene_pedido;
    private Ciudad ciudad;


    public Integer getCo_direccion() {
        return co_direccion;
    }

    public void setCo_direccion(Integer co_direccion) {
        this.co_direccion = co_direccion;
    }

    public String getCo_empresa() {
        return co_empresa;
    }

    public void setCo_empresa(String co_empresa) {
        this.co_empresa = co_empresa;
    }

    public String getTx_direccion() {
        return tx_direccion;
    }

    public void setTx_direccion(String tx_direccion) {
        this.tx_direccion = tx_direccion;
    }

    public String getFe_registro() {
        return fe_registro;
    }

    public void setFe_registro(String fe_registro) {
        this.fe_registro = fe_registro;
    }

    public String getCo_ciudad() {
        return co_ciudad;
    }

    public void setCo_ciudad(String co_ciudad) {
        this.co_ciudad = co_ciudad;
    }

    public String getTxCiudad() {
        return getCiudad().getTx_ciudad();
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public String getTx_codigo_postal() {
        return tx_codigo_postal;
    }

    public void setTx_codigo_postal(String tx_codigo_postal) {
        this.tx_codigo_postal = tx_codigo_postal;
    }

    public boolean isTienePedido() {
        return tiene_pedido;
    }

    public void setTienePedido(boolean tiene_pedido) {
        this.tiene_pedido = tiene_pedido;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getTx_sector() {
        return tx_sector;
    }

    public void setTx_sector(String tx_sector) {
        this.tx_sector = tx_sector;
    }
}


