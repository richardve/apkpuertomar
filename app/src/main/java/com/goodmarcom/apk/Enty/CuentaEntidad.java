package com.goodmarcom.apk.Enty;

public class CuentaEntidad {

    private Integer co_cuenta;
    private String co_tipo_cuenta;
    private String co_entidad;
    private String tx_documento;
    private String tx_propietario;
    private String tx_nro_cuenta;
    private String co_sucursal;
    private String tx_entidad;
    private String  tx_sucursal;
    private Boolean in_fabrica;
    private String co_ciudad;
    private String tx_tipo_cuenta;


    public Integer getCo_cuenta() {
        return co_cuenta;
    }

    public void setCo_cuenta(Integer co_cuenta) {
        this.co_cuenta = co_cuenta;
    }

    public String getCo_tipo_cuenta() {
        return co_tipo_cuenta;
    }

    public void setCo_tipo_cuenta(String co_tipo_cuenta) {
        this.co_tipo_cuenta = co_tipo_cuenta;
    }

    public String getCo_entidad() {
        return co_entidad;
    }

    public void setCo_entidad(String co_entidad) {
        this.co_entidad = co_entidad;
    }

    public String getTx_documento() {
        return tx_documento;
    }

    public void setTx_documento(String tx_documento) {
        this.tx_documento = tx_documento;
    }

    public String getTx_propietario() {
        return tx_propietario;
    }

    public void setTx_propietario(String tx_propietario) {
        this.tx_propietario = tx_propietario;
    }

    public String getTx_nro_cuenta() {
        return tx_nro_cuenta;
    }

    public void setTx_nro_cuenta(String tx_nro_cuenta) {
        this.tx_nro_cuenta = tx_nro_cuenta;
    }

    public String getCo_sucursal() {
        return co_sucursal;
    }

    public void setCo_sucursal(String co_sucursal) {
        this.co_sucursal = co_sucursal;
    }

    public String getTx_entidad() {
        return tx_entidad;
    }

    public void setTx_entidad(String tx_entidad) {
        this.tx_entidad = tx_entidad;
    }

    public String getTx_sucursal() {
        return tx_sucursal;
    }

    public void setTx_sucursal(String tx_sucursal) {
        this.tx_sucursal = tx_sucursal;
    }

    public Boolean getIn_fabrica() {
        return in_fabrica;
    }

    public void setIn_fabrica(Boolean in_fabrica) {
        this.in_fabrica = in_fabrica;
    }

    public String getCo_ciudad() {
        return co_ciudad;
    }

    public void setCo_ciudad(String co_ciudad) {
        this.co_ciudad = co_ciudad;
    }

    public String getTx_tipo_cuenta() {
        return tx_tipo_cuenta;
    }

    public void setTx_tipo_cuenta(String tx_tipo_cuenta) {
        this.tx_tipo_cuenta = tx_tipo_cuenta;
    }
}
