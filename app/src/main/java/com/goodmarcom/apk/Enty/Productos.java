package com.goodmarcom.apk.Enty;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Productos {

    private Integer co_producto;
    private String co_activo;
    private String co_pais;
    @SerializedName("tx_producto")
    private String nombre;
    @SerializedName("nu_precio")
    private String precio;
    private String tx_imagen;
    private String tx_ext_imagen;
    private boolean in_activo;
    @SerializedName("url_imagen")
    private String imagen;
    private Pais pais;
    private Activo activo;
    private ValorActivo valor_activo;
    private ArrayList<Combo> combo;
    private String tx_unidad;
    private String tx_valor;
    private String cantidad;
    private Long id;



    public Productos() {
    }

    public Productos(Integer co_producto, String co_activo, String co_pais, String nombre, String precio, String tx_imagen, String tx_ext_imagen, boolean in_activo, String imagen) {
        this.setCo_producto(co_producto);
        this.setCo_activo(co_activo);
        this.setCo_pais(co_pais);
        this.setNombre(nombre);
        this.setPrecio(precio);
        this.setTx_imagen(tx_imagen);
        this.setTx_ext_imagen(tx_ext_imagen);
        this.setIn_activo(in_activo);
        this.setImagen(imagen);
    }


    public Productos(String imagen, String nombre, String precio, String cantidad,String tx_unidad, String tx_valor) {
        this.setImagen(imagen);
        this.setNombre(nombre);
        this.setPrecio(precio);
        this.setCantidad(cantidad);
        this.setTx_unidad(tx_unidad);
        this.setTx_valor(tx_valor);
    }

    public String getImagen() {
        return imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }


    public Integer getCo_producto() { return co_producto; }

    public void setCo_producto(Integer co_producto) { this.co_producto = co_producto; }

    public String getCo_activo() { return co_activo; }

    public void setCo_activo(String co_activo) { this.co_activo = co_activo; }

    public String getCo_pais() { return co_pais; }

    public void setCo_pais(String co_pais) { this.co_pais = co_pais; }

    public String getTx_imagen() { return tx_imagen;  }

    public void setTx_imagen(String tx_imagen) { this.tx_imagen = tx_imagen; }

    public String getTx_ext_imagen() { return tx_ext_imagen;  }

    public void setTx_ext_imagen(String tx_ext_imagen) {  this.tx_ext_imagen = tx_ext_imagen;  }

    public boolean isIn_activo() {  return in_activo;  }

    public void setIn_activo(boolean in_activo) { this.in_activo = in_activo; }

    public String getCantidad() {
        if(this.cantidad == null) {
            return "";
        }else {
            return this.cantidad;
        }
    }

    public void setCantidad(String cantidad) { this.cantidad = cantidad; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("co_producto", ""+co_producto);
            obj.put("nu_cantidad", ""+cantidad);
            obj.put("nu_precio", ""+precio);

        } catch (JSONException e) {

        }

        return obj;
    }

   public Activo getActivo() {
        return activo;
    }

    public void setActivo(Activo activo) {
        this.activo = activo;
    }

    public ValorActivo getValor_activo() {
        return valor_activo;
    }

    public void setValor_activo(ValorActivo valor_activo) {
        this.valor_activo = valor_activo;
    }

    public String getUnidad(){

        //return getActivo().getUnidad().getTx_unidad();

        if (getActivo() == null)
        {
            return "";
        }else {
            return getActivo().getUnidad().getTx_unidad();
        }

    }

    public String getValor(){

        if (getValor_activo() == null)
        {
            return "";
        }else {
            return getValor_activo().getTx_valor();
        }
    }

    public String getTx_unidad() {

        if(tx_unidad == null)
        {
            return "";
        }else {
            return tx_unidad;
        }
    }


    public void setTx_unidad(String tx_unidad) {
        this.tx_unidad = tx_unidad;
    }

    public String getTx_valor() {

        if(tx_valor==null){
            return "";
        }else
        {
            return tx_valor;
        }

    }

    public void setTx_valor(String tx_valor) {
        this.tx_valor = tx_valor;
    }

    public ArrayList<Combo> getCombo() {
        return combo;
    }

    public void setCombo(ArrayList<Combo> combo) {
        this.combo = combo;
    }

    public boolean isCombo(){
        if(combo.isEmpty()){
            return false;
        }else {
            return true;
        }
    }
}
