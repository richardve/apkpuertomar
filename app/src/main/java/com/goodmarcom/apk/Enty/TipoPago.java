package com.goodmarcom.apk.Enty;

public class TipoPago {

    private Integer co_tipo_pago;
    private String tx_tipo_pago;
    private String co_forma_pago;
    private boolean in_referencia;


    public Integer getCo_tipo_pago() {
        return co_tipo_pago;
    }

    public void setCo_tipo_pago(Integer co_tipo_pago) {
        this.co_tipo_pago = co_tipo_pago;
    }

    public String getTx_tipo_pago() {

        if(this.tx_tipo_pago == null) {
            return "";
        }else {
            return this.tx_tipo_pago;
        }
        //return tx_tipo_pago;
    }

    public void setTx_tipo_pago(String tx_tipo_pago) {
        this.tx_tipo_pago = tx_tipo_pago;
    }

    public String getCo_forma_pago() {
        return co_forma_pago;
    }

    public void setCo_forma_pago(String co_forma_pago) {
        this.co_forma_pago = co_forma_pago;
    }

    public boolean isIn_referencia() {
        return in_referencia;
    }

    public void setIn_referencia(boolean in_referencia) {
        this.in_referencia = in_referencia;
    }
}