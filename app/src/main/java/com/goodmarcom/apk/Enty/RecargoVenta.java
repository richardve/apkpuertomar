package com.goodmarcom.apk.Enty;


public class RecargoVenta {

    private Integer co_config;
    private String co_forma_pago;
    private String co_tipo_pago;
    private String co_pais;
    private String nu_porcentaje;


    public Integer getCo_config() {
        return co_config;
    }

    public void setCo_config(Integer co_config) {
        this.co_config = co_config;
    }

    public String getCo_forma_pago() {
        return co_forma_pago;
    }

    public void setCo_forma_pago(String co_forma_pago) {
        this.co_forma_pago = co_forma_pago;
    }

    public String getCo_tipo_pago() {
        return co_tipo_pago;
    }

    public void setCo_tipo_pago(String co_tipo_pago) {
        this.co_tipo_pago = co_tipo_pago;
    }

    public String getCo_pais() {
        return co_pais;
    }

    public void setCo_pais(String co_pais) {
        this.co_pais = co_pais;
    }

    public String getNu_porcentaje() {
        return nu_porcentaje;
    }

    public void setNu_porcentaje(String nu_porcentaje) {
        this.nu_porcentaje = nu_porcentaje;
    }

}
