package com.goodmarcom.apk.Enty;

import com.google.gson.annotations.SerializedName;

public class Recetas {

    private Integer co_receta;
    @SerializedName("tx_tiulo")
    private String titulo;
    @SerializedName("tx_descripcion")
    private String descripcion;
    private String tx_imagen;
    @SerializedName("url_imagen")
    private String imagen;
    private String tx_ext_imagen;
    private String preparacion;

    public Recetas() {
    }

    public Recetas(String imagen, String titulo, String ingredientes, String preparacion) {
        this.setImagen(imagen);
        this.setTitulo(titulo);
        this.setDescripcion(ingredientes);
        this.setPreparacion(preparacion);
    }

    public String getImagen() {
        return imagen;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getPreparacion() {
        return preparacion;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPreparacion(String preparacion) {
        this.preparacion = preparacion;
    }

    public Integer getCo_receta() { return co_receta;   }

    public void setCo_receta(Integer co_receta) {  this.co_receta = co_receta;  }

    public String getTx_imagen() { return tx_imagen; }

    public void setTx_imagen(String tx_imagen) { this.tx_imagen = tx_imagen;  }

    public String getTx_ext_imagen() { return tx_ext_imagen; }

    public void setTx_ext_imagen(String tx_ext_imagen) { this.tx_ext_imagen = tx_ext_imagen; }




}
