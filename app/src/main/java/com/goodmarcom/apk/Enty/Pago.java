package com.goodmarcom.apk.Enty;

public class Pago {

    private Integer co_forma_pago;
    private String tx_forma_pago;
    private String nu_dias;
    private Boolean in_credito;
    private Boolean in_contado;


    public Integer getCo_forma_pago() {
        return co_forma_pago;
    }

    public void setCo_forma_pago(Integer co_forma_pago) {
        this.co_forma_pago = co_forma_pago;
    }

    public String getTx_forma_pago() {
        return tx_forma_pago;
    }

    public void setTx_forma_pago(String tx_forma_pago) {
        this.tx_forma_pago = tx_forma_pago;
    }

    public String getNu_dias() {
        return nu_dias;
    }

    public void setNu_dias(String nu_dias) {
        this.nu_dias = nu_dias;
    }

    public Boolean getIn_credito() {
        return in_credito;
    }

    public void setIn_credito(Boolean in_credito) {
        this.in_credito = in_credito;
    }

    public Boolean getIn_contado() {
        return in_contado;
    }

    public void setIn_contado(Boolean in_contado) {
        this.in_contado = in_contado;
    }
}


