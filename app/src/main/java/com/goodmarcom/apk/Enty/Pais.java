package com.goodmarcom.apk.Enty;

import com.google.gson.Gson;

import java.util.ArrayList;

public class Pais {

  private Integer  co_pais;

  private String tx_pais;

  private String tx_moneda;

  private String tx_codiso;

  private String tx_codmoneda;

  private String tx_simbolo_moneda;

  private String co_documento;

  private String tx_documento;

  private  ArrayList<TipoDocumento> documento;

  private ArrayList<Ciudad> ciudades;


    public Integer getCo_pais() {
        return co_pais;
    }

    public void setCo_pais(Integer co_pais) {
        this.co_pais = co_pais;
    }

    public String getTx_pais() {
        return tx_pais;
    }

    public void setTx_pais(String tx_pais) {
        this.tx_pais = tx_pais;
    }

    public String getTx_codiso() {
        return tx_codiso;
    }

    public void setTx_codiso(String tx_codiso) {
        this.tx_codiso = tx_codiso;
    }

    public String getTx_codmoneda() {
        return tx_codmoneda;
    }

    public void setTx_codmoneda(String tx_codmoneda) {
        this.tx_codmoneda = tx_codmoneda;
    }

    public String getTx_moneda() {
        return tx_moneda;
    }

    public void setTx_moneda(String tx_moneda) {
        this.tx_moneda = tx_moneda;
    }

    public String getTx_simbolo_moneda() {
        return tx_simbolo_moneda;
    }

    public void setTx_simbolo_moneda(String tx_simbolo_moneda) {
        this.tx_simbolo_moneda = tx_simbolo_moneda;
    }

    public ArrayList<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(ArrayList<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }

    public ArrayList<TipoDocumento> getDocumento() {
        return documento;
    }

    public void setDocumento(ArrayList<TipoDocumento> documento) {
        this.documento = documento;
    }


    public String getJsonTipoDocumento(){

        Gson gson = new Gson();

        return gson.toJson(getDocumento());

    }


}
