package com.goodmarcom.apk.Enty;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Pedidos {

    @SerializedName("co_pedido")
    private String codigo;
    private String co_empresa;
    private String co_estatus;
    @SerializedName("fe_registro")
    private String fecha;
    @SerializedName("tx_estatus")
    private String estatus;
    private PagoPedidos pago_pedido;
    private ArrayList<DetallePedidos> detalle;
    private Deuda deuda;
    private Double total;
    private Double cantidad;


    public Pedidos() {

        this.cantidad = 0.0;
        this.total = 0.0;

    }

    public Pedidos(String codigo, String fecha, String estatus, Double total, Double cantidad) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.estatus = estatus;
        this.total = total;
        this.cantidad = cantidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public String getEstatus() {
        return estatus;
    }

    public Double getTotal() {
        this.total = 0.0;
        for(int x=0;x<detalle.size();x++) {
            total += detalle.get(x).getNu_cantidad()*detalle.get(x).getNu_precio();
        }
        return total;
    }

    public Double getCantidad() {
        this.cantidad = 0.0;
        for(int x=0;x<detalle.size();x++) {
            cantidad += detalle.get(x).getNu_cantidad();
        }
        return cantidad;
    }

    public String getCo_empresa() {
        return co_empresa;
    }

    public String getCo_forma_pago() {
        return  getPago_pedido().getCo_forma_pago();
    }

    public String getTx_forma_pago() {
        return  getPago_pedido().getTx_forma_pago();
    }

    public String getTx_tipo_pago() {
        return  getPago_pedido().getTx_tipo_pago();
    }



    public String getCo_estatus() {
        return co_estatus;
    }

    public ArrayList<DetallePedidos> getDetalle() {
        return detalle;
    }

    public Deuda getDeuda() {
        return deuda;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public void setCo_empresa(String co_empresa) {
        this.co_empresa = co_empresa;
    }

  /*  public void setCo_forma_pago(String co_forma_pago) {
        this.co_forma_pago = co_forma_pago;
    }

    public void setIn_credito(boolean in_credito) {
        this.in_credito = in_credito;
    }

    public void setTx_forma_pago(String tx_forma_pago) {
        this.tx_forma_pago = tx_forma_pago;
    }*/

    public void setCo_estatus(String co_estatus) {
        this.co_estatus = co_estatus;
    }

    public void setDetalle(ArrayList<DetallePedidos> detalle) {
        this.detalle = detalle;
    }

    public void setDeuda(Deuda deuda) {
        this.deuda = deuda;
    }

    public PagoPedidos getPago_pedido() {
        return pago_pedido;
    }

    public void setPago_pedido(PagoPedidos pago_pedido) {
        this.pago_pedido = pago_pedido;
    }

    public class DetallePedidos {

        private String co_pedido;
        private String co_producto;
        private double nu_cantidad;
        private double nu_precio;
        private Productos producto;

        public double getNu_cantidad() {
            return nu_cantidad;
        }

        public double getNu_precio() {
            return nu_precio;
        }

        public String getNombre(){

            return producto.getNombre();
        }

        public String getImagen(){
            return producto.getImagen();
        }

        public String getUnidad() {return  producto.getUnidad();}

        public String getValor() {return  producto.getValor();}

    }

    public class Deuda {

        private String co_pedido;
        private String co_cuenta;
        private String fe_registro;
        private double nu_monto;
        private double nu_saldo;
        private String fe_vencimiento;

        public String getCo_pedido() {
            return co_pedido;
        }

        public String getCo_cuenta() {
            return co_cuenta;
        }

        public String getFe_registro() {
            return fe_registro;
        }

        public double getNu_monto() {
            return nu_monto;
        }

        public double getNu_saldo() {
            return nu_saldo;
        }

        public String getFe_vencimiento() {
            return fe_vencimiento;
        }
    }
}

