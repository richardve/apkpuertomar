package com.goodmarcom.apk.Enty;

public class Activo {

   private Integer co_activo;
   private String co_clase;
   private String co_tipo_activo;
   private String co_marca;
   private String co_unidad;
   private String tx_descripcion;
   private Unidad unidad;


    public Integer getCo_activo() {
        return co_activo;
    }

    public void setCo_activo(Integer co_activo) {
        this.co_activo = co_activo;
    }

    public String getCo_clase() {
        return co_clase;
    }

    public void setCo_clase(String co_clase) {
        this.co_clase = co_clase;
    }

    public String getCo_tipo_activo() {
        return co_tipo_activo;
    }

    public void setCo_tipo_activo(String co_tipo_activo) {
        this.co_tipo_activo = co_tipo_activo;
    }

    public String getCo_marca() {
        return co_marca;
    }

    public void setCo_marca(String co_marca) {
        this.co_marca = co_marca;
    }

    public String getCo_unidad() {
        return co_unidad;
    }

    public void setCo_unidad(String co_unidad) {
        this.co_unidad = co_unidad;
    }

    public String getTx_descripcion() {
        return tx_descripcion;
    }

    public void setTx_descripcion(String tx_descripcion) {
        this.tx_descripcion = tx_descripcion;
    }

    public Unidad getUnidad() {
        return unidad;
    }

    public void setUnidad(Unidad unidad) {
        this.unidad = unidad;
    }
}

class Unidad{

    private Integer co_unidad;
    private String tx_unidad;
    private Boolean in_precision;


    public Integer getCo_unidad() {
        return co_unidad;
    }

    public void setCo_unidad(Integer co_unidad) {
        this.co_unidad = co_unidad;
    }

    public String getTx_unidad() {
        return tx_unidad;
    }

    public void setTx_unidad(String tx_unidad) {
        this.tx_unidad = tx_unidad;
    }

    public Boolean getIn_precision() {
        return in_precision;
    }

    public void setIn_precision(Boolean in_precision) {
        this.in_precision = in_precision;
    }
}