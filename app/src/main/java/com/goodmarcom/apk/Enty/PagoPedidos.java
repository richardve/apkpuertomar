package com.goodmarcom.apk.Enty;

public class PagoPedidos {

    private Integer co_pago;
    private String co_pedido;
    private String co_cuenta;
    private String co_forma_pago;
    private String co_tipo_pago;
    private String co_direccion;
    private String tx_referencia;
    private String nu_monto;
    private String nu_porc;
    private String tx_recibo;
    private String fe_registro;
    private String tx_ext_recibo;
    private String recibo;
    private CuentaEntidad cuenta_entidad;
    private FormaPago forma_pago;
    private TipoPago tipo_pago;
    private Direcciones cliente_direccion;


    public Integer getCo_pago() {
        return co_pago;
    }

    public void setCo_pago(Integer co_pago) {
        this.co_pago = co_pago;
    }

    public String getCo_pedido() {
        return co_pedido;
    }

    public void setCo_pedido(String co_pedido) {
        this.co_pedido = co_pedido;
    }

    public String getCo_cuenta() {
        return co_cuenta;
    }

    public void setCo_cuenta(String co_cuenta) {
        this.co_cuenta = co_cuenta;
    }

    public String getCo_forma_pago() {
        return co_forma_pago;
    }

    public void setCo_forma_pago(String co_forma_pago) {
        this.co_forma_pago = co_forma_pago;
    }

    public String getCo_tipo_pago() {
        return co_tipo_pago;
    }

    public void setCo_tipo_pago(String co_tipo_pago) {
        this.co_tipo_pago = co_tipo_pago;
    }

    public String getCo_direccion() {
        return co_direccion;
    }

    public void setCo_direccion(String co_direccion) {
        this.co_direccion = co_direccion;
    }

    public String getTx_referencia() {
        return tx_referencia;
    }

    public void setTx_referencia(String tx_referencia) {
        this.tx_referencia = tx_referencia;
    }

    public String getNu_monto() {
        return nu_monto;
    }

    public void setNu_monto(String nu_monto) {
        this.nu_monto = nu_monto;
    }

    public String getTx_recibo() {
        return tx_recibo;
    }

    public void setTx_recibo(String tx_recibo) {
        this.tx_recibo = tx_recibo;
    }

    public String getFe_registro() {
        return fe_registro;
    }

    public void setFe_registro(String fe_registro) {
        this.fe_registro = fe_registro;
    }

    public String getTx_ext_recibo() {
        return tx_ext_recibo;
    }

    public void setTx_ext_recibo(String tx_ext_recibo) {
        this.tx_ext_recibo = tx_ext_recibo;
    }

    public String getRecibo() {
        return recibo;
    }

    public void setRecibo(String recibo) {
        this.recibo = recibo;
    }

    public CuentaEntidad getCuenta_entidad() {
        return cuenta_entidad;
    }

    public void setCuenta_entidad(CuentaEntidad cuenta_entidad) {
        this.cuenta_entidad = cuenta_entidad;
    }

    public FormaPago getForma_pago() {
        return forma_pago;
    }

    public void setForma_pago(FormaPago forma_pago) {
        this.forma_pago = forma_pago;
    }

    public TipoPago getTipo_pago() {
        return tipo_pago;
    }

    public void setTipo_pago(TipoPago tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    public Direcciones getCliente_direccion() {
        return cliente_direccion;
    }

    public void setCliente_direccion(Direcciones cliente_direccion) {
        this.cliente_direccion = cliente_direccion;
    }

    public String getTx_forma_pago()
    {
        return getForma_pago().getTx_forma_pago();
    }

    public String getTx_tipo_pago()  {

        if(this.getTipo_pago() == null) {
            return "";
        }else {
            return this.getTipo_pago().getTx_tipo_pago();
        }

        //return getTipo_pago().getTx_tipo_pago();

    }

    public String getNu_porc() {
        return nu_porc;
    }

    public void setNu_porc(String nu_porc) {
        this.nu_porc = nu_porc;
    }
}
