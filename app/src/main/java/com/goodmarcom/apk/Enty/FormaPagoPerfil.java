package com.goodmarcom.apk.Enty;

public class FormaPagoPerfil {

    private String co_empresa;
    private String co_forma_pago;
    private Pago metodo;


    public String getCo_empresa() {
        return co_empresa;
    }

    public void setCo_empresa(String co_empresa) {
        this.co_empresa = co_empresa;
    }

    public String getCo_forma_pago() {
        return co_forma_pago;
    }

    public void setCo_forma_pago(String co_forma_pago) {
        this.co_forma_pago = co_forma_pago;
    }

    public Pago getMetodo() {
        return metodo;
    }

    public void setMetodo(Pago metodo) {
        this.metodo = metodo;
    }
}
