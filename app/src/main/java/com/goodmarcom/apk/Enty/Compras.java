package com.goodmarcom.apk.Enty;

public class Compras {

    private String fecha;
    private String estatus;
    private String total;
    private String producto1;
    private String producto2;
    private String precio1;
    private String precio2;
    private String codigo;

    public Compras() {
    }

    public Compras(String codigo, String fecha, String estatus, String total, String producto1, String producto2, String precio1, String precio2) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.estatus = estatus;
        this.total = total;
        this.producto1 = producto1;
        this.producto2 = producto2;
        this.precio1 = precio1;
        this.precio2 = precio2;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public String getEstatus() {
        return estatus;
    }

    public String getTotal() {
        return total;
    }

    public String getProducto1() {
        return producto1;
    }

    public String getProducto2() {
        return producto2;
    }

    public String getPrecio1() {
        return precio1;
    }

    public String getPrecio2() {
        return precio2;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public void setProducto1(String producto1) {
        this.producto1 = producto1;
    }

    public void setProducto2(String producto2) {
        this.producto2 = producto2;
    }

    public void setPrecio1(String precio1) {
        this.precio1 = precio1;
    }

    public void setPrecio2(String precio2) {
        this.precio2 = precio2;
    }
}
