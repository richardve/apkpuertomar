package com.goodmarcom.apk.Enty;

public class Ciudad {

    private Integer co_ciudad;

    private String co_pais;

    private String tx_ciudad;

    private String tx_zona_horaria;


    public Integer getCo_ciudad() {
        return co_ciudad;
    }

    public void setCo_ciudad(Integer co_ciudad) {
        this.co_ciudad = co_ciudad;
    }

    public String getCo_pais() {
        return co_pais;
    }

    public void setCo_pais(String co_pais) {
        this.co_pais = co_pais;
    }

    public String getTx_ciudad() {
        return tx_ciudad;
    }

    public void setTx_ciudad(String tx_ciudad) {
        this.tx_ciudad = tx_ciudad;
    }

    public String getTx_zona_horaria() {
        return tx_zona_horaria;
    }

    public void setTx_zona_horaria(String tx_zona_horaria) {
        this.tx_zona_horaria = tx_zona_horaria;
    }

}
